<?php 
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php');
	include('inc/agl_ct.php');
	include('inc/agl_fn.php');
	include('inc/agl_fn_add_items.php');


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}
		/**
	 * Adding of items Process Logic
	 	*1. Add the item details in the Items Table in Database that is the "all items table"
	  *2. Check the item status if status = 1 add the item details in the available_items database table
	  *2.1 if status = 0 add to lock_items database table
	 */

	

	
	if(isset($_POST['add_item_btn'])){ // isset start

		

		if(empty(trim($_POST['sloc'])) || empty(trim($_POST['batch_num'])) || empty(trim($_POST['item_name'])) || empty(trim($_POST['item_program'])) || empty(trim($_POST['expiry_date'])) || empty(trim($_POST['item_unit'])) || empty(trim($_POST['item_status'])) || empty(trim($_POST['unit_cost'])) ){

			$_SESSION['response'] = "Error: <span class=\"text-bold\">All fields are Required!</span>";
			$_SESSION['res_type'] = "danger";
			header("Location:add_item.php");


		}else{	// else start


			/**
			* [Get all Items from DB to determine if the item to be added
			*  already exists in the Database]
			* @var array
			*/
		
			$asar_db_items = get_db_items($conn,"items");
			$asar_db_lock_items = get_db_items($conn,"lock_items");

			//print_r_html($asar_db_items,"All Items");
			//print_r_html($asar_db_lock_items,"Lock Items");

			$property_num = remove_junk(generate_property_num()); // SKU OR PROPERTY NUMBER
			$batch_no = remove_junk($_POST['batch_num']);
			$name = remove_junk($_POST['item_name']);
			$expiry = remove_junk($_POST['expiry_date']);
			$unit = remove_junk($_POST['item_unit']);
			$status = remove_junk($_POST['item_status']);
			$unit_cost = remove_junk($_POST['unit_cost']);
			$sloc = remove_junk($_POST['sloc']);
			$program = remove_junk($_POST['item_program']);

			if(empty($asar_db_items)){
				/**
				 * DB has no item so we just insert it right away
				 */
				//Insert Query
				$ins_query  = "INSERT INTO items";
				$ins_query .= " (item_batch_num,item_program,property_num,s_loc,item_name,item_expiry,item_unit,item_unit_cost,item_status)";
				$ins_query .= "VALUES (";
				$ins_query .= "'{$batch_no}','{$program}','{$property_num}','{$sloc}','{$name}','{$expiry}','{$unit}','{$unit_cost}','{$status}'";
				$ins_query .= ")";

				if($conn->query($ins_query) === True){
					if($status == 301){
						$av_query  = "INSERT INTO available_items";
						$av_query .= " (item_batch_num,item_program,property_num,s_loc,item_name,item_expiry,item_unit,item_unit_cost,item_status)";
						$av_query .= "VALUES (";
						$av_query .= "'{$batch_no}','{$program}','{$property_num}','{$sloc}','{$name}','{$expiry}','{$unit}','{$unit_cost}','{$status}'";
						$av_query .= ")";
	
						if($conn->query($av_query) === True){
							$_SESSION['response'] = "Success: <span class=\"text-bold\">{$property_num} - {$name}</span> has been Added Succesfully!";
							$_SESSION['res_type'] = "success";
		
							header("Location:add_item.php");
						}else{
							$_SESSION['response'] = "Error: <span class=\"text-bold\">Failed To Add Item! {$conn->error}</span>";
							$_SESSION['res_type'] = "danger";
							header("Location:add_item.php");
						}
					}
	
					if($status == 300){
						$av_query  = "INSERT INTO lock_items";
						$av_query .= " (item_batch_num,item_program,property_num,s_loc,item_name,item_expiry,item_unit,item_unit_cost,item_status)";
						$av_query .= "VALUES (";
						$av_query .= "'{$batch_no}','{$program}','{$property_num}','{$sloc}','{$name}','{$expiry}','{$unit}','{$unit_cost}','{$status}'";
						$av_query .= ")";
	
						if($conn->query($av_query) === TRUE){
							$_SESSION['response'] = "Success: <span class=\"text-bold\">{$property_num} - {$name}</span> has been Added Succesfully!";
							$_SESSION['res_type'] = "success";
		
							header("Location:add_item.php");
						}else{
							$_SESSION['response'] = "Error: <span class=\"text-bold\">Failed To Add Item! {$conn->error}</span>";
							$_SESSION['res_type'] = "danger";
							header("Location:add_item.php");
						}
					}
				}
			}else{ // 2nd else start
				
				if(!isset($asar_db_items[$property_num]) && $status == 301){
					/**
					 * Item does not exist yet so we are going to add it
					 */

					//Insert Query
					$ins_query  = "INSERT INTO items";
					$ins_query .= " (item_batch_num,item_program,property_num,s_loc,item_name,item_expiry,item_unit,item_unit_cost,item_status)";
					$ins_query .= "VALUES (";
					$ins_query .= "'{$batch_no}','{$program}','{$property_num}','{$sloc}','{$name}','{$expiry}','{$unit}','{$unit_cost}','{$status}'";
					$ins_query .= ")";

					if ($conn->query($ins_query) === True ) {

						$av_query  = "INSERT INTO available_items";
						$av_query .= " (item_batch_num,item_program,property_num,s_loc,item_name,item_expiry,item_unit,item_unit_cost,item_status)";
						$av_query .= "VALUES (";
						$av_query .= "'{$batch_no}','{$program}','{$property_num}','{$sloc}','{$name}','{$expiry}','{$unit}','{$unit_cost}','{$status}'";
						$av_query .= ")";

						if($conn->query($av_query) === True){
							$_SESSION['response'] = "Success: <span class=\"text-bold\">{$property_num} - {$name}</span> has been Added Succesfully!";
							$_SESSION['res_type'] = "success";
		
							header("Location:add_item.php");
						}else{
							$_SESSION['response'] = "Error: <span class=\"text-bold\">Failed To Add Item! {$conn->error}</span>";
							$_SESSION['res_type'] = "danger";
							header("Location:add_item.php");
						}
						
							
					}else{
						$_SESSION['response'] = "Error: <span class=\"text-bold\">Failed To Add Item! {$conn->error}</span>";
						$_SESSION['res_type'] = "danger";
						header("Location:add_item.php");
					}


				}

				if(!isset($asar_db_items[$property_num]) && $status == 300){
					/**
					 * Item does not exist yet so we are going to add it
					 */

					//Insert Query
					$ins_query  = "INSERT INTO items";
					$ins_query .= " (item_batch_num,item_program,property_num,s_loc,item_name,item_expiry,item_unit,item_unit_cost,item_status)";
					$ins_query .= "VALUES (";
					$ins_query .= "'{$batch_no}','{$program}','{$property_num}','{$sloc}','{$name}','{$expiry}','{$unit}','{$unit_cost}','{$status}'";
					$ins_query .= ")";

					if ($conn->query($ins_query) === True ) {

						$av_query  = "INSERT INTO lock_items";
						$av_query .= " (item_batch_num,item_program,property_num,s_loc,item_name,item_expiry,item_unit,item_unit_cost,item_status)";
						$av_query .= "VALUES (";
						$av_query .= "'{$batch_no}','{$program}','{$property_num}','{$sloc}','{$name}','{$expiry}','{$unit}','{$unit_cost}','{$status}'";
						$av_query .= ")";

						if($conn->query($av_query) === TRUE){
							$_SESSION['response'] = "Success: <span class=\"text-bold\">{$property_num} - {$name}</span> has been Added Succesfully!";
							$_SESSION['res_type'] = "success";
		
							header("Location:add_item.php");
						}else{
							$_SESSION['response'] = "Error: <span class=\"text-bold\">Failed To Add Item! {$conn->error}</span>";
							$_SESSION['res_type'] = "danger";
							header("Location:add_item.php");
						}
						
							
					}else{
						$_SESSION['response'] = "Error: <span class=\"text-bold\">Failed To Add Item! {$conn->error}</span>";
						$_SESSION['res_type'] = "danger";
						header("Location:add_item.php");
					}


				}

			}// 2nd else end
			

		}//else end

	}// isset end


 ?>