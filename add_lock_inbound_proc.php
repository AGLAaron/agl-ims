<?php 

	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	// if(isset($_SESSION['last_activity'])){

	// 	$last_activity = $_SESSION['last_activity'];
	// 	$timeout = 1800; // 30 mins

	// 	$time_now = time();

	// 	$duration = $time_now-$last_activity;
		
	// 	if($duration > $timeout){
	// 		session_start();

	// 		session_destroy();

	// 		header("location:login.php");
	// 	}
	// }

			

	if(isset($_POST['add_inbound'])){ // isset Start

		/**
		 * First Field Verification
		 */
		if (empty(trim($_POST['f_shipper'])) || empty(trim($_POST['f_dest'])) || empty(trim($_POST['f_in_date'])) || empty(trim($_POST['f_remarks'])) || !array_filter($_POST['f_property_no']) || !array_filter($_POST['f_qty'])) { //First Field Verification Start

			$_SESSION['submit_response'] = "<b>Error:</b> Please Fill All Fields!";
			$_SESSION['submit_res_type'] = "danger";
			header("Location:add_lock_inbound.php");

		}else{ // Field Verification Else 
			/**
			 * Second Verification of Fields
			 */
			
			 if(is_array_has_empty_input($_POST['f_property_no']) || is_array_has_empty_input($_POST['f_qty'])){
				$_SESSION['submit_response'] = "<b>Error:</b> Please Fill All Fields!";
				$_SESSION['submit_res_type'] = "danger";
				header("Location:add_lock_inbound.php");
			 }else{


				/**
				* Get All Items
				* It will be used in the Form That the User Will Select
				*/
				$all_items = get_locked_items($conn);

				/**
				* Check first Each Property Number if it does exist in All Items
				*/

				$are_all_items_exist = are_all_items_exist($_POST['f_property_no'], $all_items);

				if($are_all_items_exist){
					/**
					 * Associative Array that will contain all info from the submitted Form
					 */
					 $asar_form_info = array();
					 /**
					  * Associative Array that will contain the stock receive information
					  * Array contents will be pushed in the Database
					  */
					 $arr_inbound = array();


					 $asar_form_info['property_num'] = $_POST['f_property_no'];
					 $asar_form_info['qty'] = $_POST['f_qty'];
					
					
					 /**
					  * Complete Item Details 
					  1. Item name
					  2. Item Batch Number
					  3. Item Expiration Date
					  4. Item Unit
					  5. Unit Cost
					  6. Running Balance
					  */

					  foreach ($asar_form_info['property_num'] as $arr_key => $aux){
						  if(isset($all_items[$aux])){
							  $asar_form_info['program'][$arr_key] = $all_items[$aux]['item_program'];
							  $asar_form_info['name'][$arr_key] = $all_items[$aux]['item_name'];
							  $asar_form_info['batch_num'][$arr_key] = $all_items[$aux]['item_batch_num'];
							  $asar_form_info['expiry'][$arr_key] = $all_items[$aux]['item_expiry'];
							  $asar_form_info['unit'][$arr_key] = $all_items[$aux]['item_unit'];
							  $asar_form_info['unit_cost'][$arr_key] = $all_items[$aux]['item_unit_cost'];
							  $asar_form_info['running_balance'][$arr_key] = $all_items[$aux]['qty'];
							  $asar_form_info['amount'][$arr_key] = $all_items[$aux]['item_unit_cost'] * $asar_form_info['qty'][$arr_key];
						  }
					  }

					  $arr_count = count($asar_form_info['property_num']);
					  $start_index = 0;

					  while($start_index < $arr_count){
						  foreach($asar_form_info as $asar_key => $asar_val){
							  $arr_inbound[$start_index][$asar_key] = $asar_val[$start_index]; 
						  }
						  $start_index++;
					  }
					
					  $ref_num  = generate_ref_num(3);
					  $shipper = remove_junk(esc_str($conn,$_POST['f_shipper']));
					  $consignee = remove_junk(esc_str($conn,$_POST['f_dest']));
					  $inbound_date = remove_junk(esc_str($conn,$_POST['f_in_date']));
					  $remarks  = remove_junk(esc_str($conn,$_POST['f_remarks']));
					  $date_created = date('Y-m-d');
					  $created_by = remove_junk(esc_str($conn,$_SESSION['name']));
					

					  /**
					   * Insert into tb_inbound Database Table
					   */

					   $query = "INSERT INTO tb_inbound";
					   $query .= " (in_ref_num, in_program, in_shipper, in_consignee, in_date, in_property_num, in_item_name, in_batch_num, in_expiry, in_qty, in_unit, in_unit_cost, in_amount, in_remarks, prev_bal, in_created, in_created_by, in_type";
					   $query .= ") VALUES ";

					   foreach($arr_inbound as $arr_key => $arr_det){
							$property_num = remove_junk(esc_str($conn,$arr_det['property_num']));
							$item_name = remove_junk(esc_str($conn,$arr_det['name']));
							$batch_num = remove_junk(esc_str($conn,$arr_det['batch_num']));
							$expiry = remove_junk(esc_str($conn,$arr_det['expiry']));
							$qty = remove_junk(esc_str($conn,$arr_det['qty']));
							$unit = remove_junk(esc_str($conn,$arr_det['unit']));
							$unit_cost = remove_junk(esc_str($conn,$arr_det['unit_cost']));
							$amount = remove_junk(esc_str($conn,$arr_det['amount']));
							$prev_bal = remove_junk(esc_str($conn,$arr_det['running_balance']));
							$program = remove_junk(esc_str($conn,$arr_det['program']));

							$query .= "(";
							$query .= "'{$ref_num}', '{$program}', '{$shipper}', '{$consignee}', '{$inbound_date}', '{$property_num}', '{$item_name}', '{$batch_num}', '{$expiry}', '{$qty}', '{$unit}', '{$unit_cost}', '{$amount}', '{$remarks}', '{$prev_bal}', '{$date_created}', '{$created_by}','200'";
							$query .= "),";

					   }

					   $query = trim($query,",");
					   
					   if($conn->query($query) === TRUE){
							$_SESSION['insert_res'] = "<b>Success:</b> Inbound Transaction Successful! Reference No: <b>{$ref_num}</b>";
							$_SESSION['insert_res_type'] = "success";

							/**
							* Update Running Balance
							*/
							foreach($arr_inbound as $inbound_key => $inbound_det){
								$property_num = remove_junk(esc_str($conn,$inbound_det['property_num']));
								$qty = remove_junk(esc_str($conn,$inbound_det['qty']));
								$running_balance = remove_junk(esc_str($conn,$inbound_det['running_balance']));
								$balance = $running_balance + $qty; // Balance as Of and also be used in the update on the running balance

								$upd_query = "UPDATE lock_items";
								$upd_query .= " SET qty = {$balance}";
								$upd_query .= " WHERE property_num  = '{$property_num}'";
								
								if ($conn->query($upd_query) === TRUE) {
									$_SESSION['update_response'] =  "<b>Success:</b> All Balance Updated Successfully!";
									$_SESSION['update_res_type'] = "success";
									$_SESSION['last_activity'] = time(); //Update Last Activity
								}else{
										
									//Failed Update
									$_SESSION['update_response'] = "<b>Error:</b> Running Balance Update Failed for item <b>{$property_num}</b>";
									$_SESSION['update_res_type'] = "danger";
									$_SESSION['last_activity'] = time(); //Update Last Activity
									header("Location:add_lock_inbound.php");
								}
							}

					    }else{	
							// $_SESSION['insert_res'] = "<b>Error:</b> Inbound Transaction Failed! Reference No: <b>{$ref_num}</b>";
							$_SESSION['insert_res'] = "<b>Error:</b> $conn->error </b>";
							$_SESSION['insert_res_type'] = "danger";
							header("Location:add_lock_inbound.php");
						}

						//Update Last Activity
						$_SESSION['last_activity'] = time();
						header("Location:add_lock_inbound.php");
				}else{
						$_SESSION['submit_response'] = "<b>Error:</b> Please Make Sure to Enter the Correct Property No.!";
						$_SESSION['submit_res_type'] = "danger";
						header("Location:add_lock_inbound.php");
				}

				
			}
		}
	}//isset End

 ?>