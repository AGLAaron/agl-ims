<?php 
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	header("refresh: 600"); 
?>

<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Order</a></li>
	        <li class="breadcrumb-item active">Create Order</li>
	      </ul>
	    </div>
	  </div>
	<?php 

		/**
		 * Get All Items
		 * It will be used in the Form That the User Will Select
		 */
		$all_items = get_available_items($conn,"items");

		/**
		 * Get All Batch Numbers
		 */
		$all_batch_no = get_all_batch_no($conn,"items");

	  	/**
		 * Get all unit so it will be displayed in the form
		 * @var [type]
		 */
		
		$all_unit = get_all_unit($conn,"unit");
		/**
		 * All Consignee/Receiver
		 * @var [type]
		 */
		$all_consignee = get_all_out_cons($conn,"outbound_destination");



		//SUBMIT PROMPT
		if (isset($_SESSION['submit_res_type'])) {
			echo "<div class = \"container-fluid\">";
				echo "<div class=\"alert alert-{$_SESSION['submit_res_type']}\">";
					echo "{$_SESSION['submit_response']}";
				echo "</div>";
			echo "</div>";	

			unset($_SESSION['submit_res_type']);
			unset($_SESSION['submit_response']);
				
		}

		//SUCCESS PROMPT
		if (isset($_SESSION['success_res_type'])) {
			echo "<div class = \"container-fluid\">";
				echo "<div class=\"alert alert-{$_SESSION['success_res_type']}\">";
					echo "{$_SESSION['success_response']}";
				echo "</div>";
			echo "</div>";	

			unset($_SESSION['success_res_type']);
			unset($_SESSION['success_response']);
				
		}




	 ?>

	<div class="container-fluid">
	 	<form action="add_order_proc.php" method="post">
	 		<div class="row">
		 		<div class="col-lg-12 col-sm-12">
		 			<div class="card">
		 				<div class="card-header align-items-center">
							<h4>Order Information</h4>
						</div>
		 				<div class="card-body">
		 					<div class="form-group">
		 						<div class="row">
		 							<div class="col-lg-6">
				 						<select name="f_order_type" id="" class="form-control">
				 							<option value="">Select Type of Order</option>
				 							<option value="p">Pick-Up</option>
				 							<option value="d">Delivery</option>
				 						</select>
			 						</div>
			 						<div class="col-lg-6">
		 								<input  placeholder="Enter/Select Date" name="f_pick_date" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="Select Date">
		 							</div>
		 						</div>
		 						<div class="row mt-3">
		 							<div class="col-lg-6">
				 						<select name="f_shipper" id="" class="form-control">
				 							<option value="">Select Shipper</option>
				 							<option value="Department of Health Regional Office - XI">Department of Health Regional Office - XI</option>
				 						</select>
			 						</div>
			 						<div class="col-lg-6">
			 							<input list="dest_list" name="f_dest" placeholder="Select/Enter Consignee" class="form-control">
			 							<datalist id="dest_list">
			 								<?php foreach($all_consignee as $consignee_key => $consignee_val){ ?>
			 									<option value="<?php echo $consignee_key.",".$consignee_val; ; ?>"><?php echo "{$consignee_val}"; ?></option>
			 								<?php } ?>
			 							</datalist>
			 						</div>
		 						</div>
		 						<div class="row mt-3">
		 							<div class="col-lg-6">
		 								<input type="text" name="f_ptr" placeholder="Enter PTR No." class="form-control">
		 							</div>
		 							<div class="col-lg-6">
		 								<input list="trans_type_list" name="f_trans_type" placeholder="Select/Enter Transfer Type" class="form-control">
			 							<datalist id="trans_type_list">
			 								<option value="Donation"></option>
			 								<option value="Reassignment"></option>
			 							</datalist>
		 							</div>
		 						</div>
		 						<div class="row mt-3">
		 							<div class="col">
		 								<input type="text" placeholder="Remarks" name="f_remarks" class="form-control">
		 							</div>
		 						</div>
		 					</div>
		 				</div>
		 			</div>
		 		</div>

		 		<div class="col-lg-12">
		 			<div class="card">
		 				<div class="card-header align-items-center">
		 					<div class="row">
		 						<div class="col-lg-6">
		 							<h4>Order Item Information</h4>
		 						</div>
		 					</div>
						</div>
						<div class="card-body">
							<div class="form-group item_info_field">
								<div class="row mb-3">
									<div class="col-lg-3 col-md-3 col-sm-12">
										<input list="property_datalist" name="f_property_no[]" placeholder="Enter Property No." class="form-control">
										<datalist id="property_datalist">
											<?php foreach($all_items as $item_key => $arr_val){ ?>
													<option value="<?php echo $item_key; ?>"><span class="text-justify"><?php echo $arr_val['item_batch_num']."-".$arr_val['item_name'];?></span class="text-justify"></option>
											<?php } ?>
										</datalist>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12">
										<input type="number" name="f_qty[]" placeholder="Enter Quantity" class="form-control">
									</div>
									<div class="col-lg-1 col-md-1 col-sm-12 text-left">
										<a href="javascript:void(0);" class="add_item_button btn btn-primary form-control" title="Add Item">+</a>
									</div>	
								</div>
							</div>
						</div>
		 			</div>
		 			<div class="row">
						<div class="col-lg-12 text-center mb-2">
							<button type="submit" name="confirm_order" class="btn btn-primary btn-lg" >Confirm Order</button>
						</div>							
					</div>
		 		</div>
		 	</div>
	 	</form>	 		
	 </div>


<?php
	include("layout/footer.php"); 
?>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 1000; //Input fields increment limitation
    var addButton = $('.add_item_button'); //Add button selector
    var wrapper = $('.item_info_field'); //Input field wrapper	

    var x = 1; //Initial field counter is 1
	var form_id_count = 0;
    //Once add button is clicked
    $(addButton).click(function(){

    	var form_id = "add-form-"+form_id_count;
    	console.log(form_id);
    	 var fieldHTML = '<div class="form-group" id="'+form_id+'"><div class="row mb-3"><div class="col-lg-3 col-md-3 col-sm-12"><input list="property_datalist" name="f_property_no[]" placeholder="Enter Property No." class="form-control"></div><div class="col-lg-3 col-md-3 col-sm-12"><input type="number" name="f_qty[]" placeholder="Enter Quantity" class="form-control"></div><div class="col-lg-1 col-md-1 col-sm-12 text-left"><a href="javascript:void(0);" class="remove_item_button btn btn-danger form-control" onclick=remove_item("'+form_id+'") title="Remove Item">-</a></div></div>';
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            form_id_count++;
            $(wrapper).append(fieldHTML); //Add field html  
        }
    });
});

 function remove_item($id){
	document.getElementById($id).remove(); //Remove field html
	
 }

</script>
