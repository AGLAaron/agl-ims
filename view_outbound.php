<?php 
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	header("refresh: 600"); 
	
 ?>

 <?php
 	include("layout/head.php");
 	include("layout/main_nav.php"); 
 	include("layout/sidebar.php");
  ?>
	<!-- Breadcrumb-->
	  <div class="breadcrumb-holder">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Outbound</a></li>
	        <li class="breadcrumb-item active">View Outbound</li>
	      </ul>
	    </div>
	  </div>

	<?php 
		$query = "SELECT * FROM tb_outbound ORDER BY id DESC";
		$result = mysqli_query($conn,$query);

		$asar_db_out = array();

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_db_out[$db_rows['out_ref_num']] = $db_rows;
		}

		
		if(isset($_SESSION['submit_res'])){
			echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-{$_SESSION['submit_res_type']}\">";
							echo "{$_SESSION['submit_res']}";
					echo "</div>";
			echo "</div>";

			unset($_SESSION['submit_res']);
			unset($_SESSION['submit_res_type']);
		}

		
		if(isset($_SESSION['insert_res'])){
			echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-{$_SESSION['insert_res_type']}\">";
							echo "{$_SESSION['insert_res']}";
					echo "</div>";
			echo "</div>";

			unset($_SESSION['insert_res']);
			unset($_SESSION['insert_res_type']);
		}

		if(isset($_SESSION['update_res'])){
			echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-{$_SESSION['update_res_type']}\">";
							echo "{$_SESSION['update_res']}";
					echo "</div>";
			echo "</div>";

			unset($_SESSION['update_res']);
			unset($_SESSION['update_res_type']);
		}	

	 ?>

	<div class="container-fluid mt-3">
		<div class="card">
			<div class="card-header align-items-center">
				<h4>Outbound Transactions</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-md" id="view_outbound_tbl">
					  <thead>
					    <tr class="bg-primary">
					      <th class="small text-center font-weight-bold text-light px-3 py-2">Outbound Ref. #</th>
					      <th class="small text-center font-weight-bold text-light px-3 py-2">Order Ref. #</th>
					      <th class="small text-center font-weight-bold text-light px-3 py-2">Outbound Date</th>
					      <th class="small text-center font-weight-bold text-light px-3 py-2">Shipper</th>
					      <th class="small text-center pb-2 font-weight-bold text-light px-3 py-2">Consignee</th>
					      <th class="small text-center pb-2 font-weight-bold text-light px-3 py-2">PTR #</th>
					      <th class="small text-center pb-2 font-weight-bold text-light px-3 py-2">Outbound Created By</th>
					      <th class="small text-center pb-2 font-weight-bold text-light px-3 py-2">Date Created</th>
					      <th class="small text-center pb-2 font-weight-bold text-light px-3 py-2">Remarks</th>
								<th class="small text-center pb-2 font-weight-bold text-light px-3 py-2"></th>
								<th class="small text-center pb-2 font-weight-bold text-light px-3 py-2"></th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php
					  	
						  	foreach ($asar_db_out as $out_key => $arr_out_val) {
					  		 ?>
						    <tr>
						      <th scope="row" class="small text-center font-weight-bold <?php echo $child; ?>"><?php echo $out_key; ?></th>
						      <td class="small text-center"><?php echo $arr_out_val['order_ref_num']; ?></td>
						      <td class="small text-center"><?php echo $arr_out_val['out_date']; ?></td>
						      <td class="small text-center"><?php echo $arr_out_val['out_shipper']; ?></td>
						      <td class="small text-center"><?php echo $arr_out_val['out_consignee']; ?></td>
						      <td class="small text-center"><?php echo $arr_out_val['out_ptr_num']; ?></td>
						      <td class="small text-center"><?php echo $arr_out_val['out_created_by']; ?></td>
						      <td class="small text-center"><?php echo $arr_out_val['out_created']; ?></td>
 							  <td class="small text-center"><?php echo $arr_out_val['out_remarks']; ?></td>
						      <?php 
						      if ($_SESSION['type'] == "admin") {?> 
										<td class="small text-center"><a target="_blank" href="<?php echo "print_withdrawal.php?ref_no={$out_key}"; ?>" class="btn btn-primary btn-sm">Print WDO</a></td>
										
										<!-- Return Stock -->
										<td class="small text-center">
											<a href="<?php echo "stock_return.php?ref_no={$out_key}"; ?>" class="btn btn-primary btn-sm">Delivery Return</a>
										</td>
										<!-- Return Stock -->
						      <?php } ?>
						    </tr>
						<?php 
							}
							
						 ?> 
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


  <?php
 	include("layout/footer.php"); 
  ?>
  <script>
  	$(document).ready(function() {
	    $('#view_outbound_tbl').DataTable({
	    	"ordering":false
	    });
	} );
  </script>