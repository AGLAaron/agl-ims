<?php
session_start();
include('inc/db/bd_connect.php'); // Db Connection
include('inc/agl_ct.php'); // Constant
include('inc/agl_fn.php'); // Functions
?>

<?php
include("layout/head.php");
include("layout/main_nav.php");
include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">View</a></li>
            <li class="breadcrumb-item active">View Returned Items</li>
        </ul>
    </div>
</div>

<?php
    //DELETE PROMPT
        if (isset($_SESSION['delete_res_type'])) {
            echo "<div class = \"container-fluid\">";
            echo "<div class=\"alert alert-{$_SESSION['delete_res_type']}\">";
            echo "{$_SESSION['delete_response']}";
            echo "</div>";
            echo "</div>";

            unset($_SESSION['delete_res_type']);
            unset($_SESSION['delete_response']);
        }

?>



<?php

$query = "SELECT * FROM return_items ORDER by return_id DESC";
$result = mysqli_query($conn, $query);
$asar_return = array();

while ($db_rows = mysqli_fetch_assoc($result)) {
    $asar_return[] = $db_rows;
}

?>

<div class="container-fluid mt-3">
    <div class="card">
        <div class="card-header align-items-center">
            <h4>Returned Items</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-md" id="view_return_table">
                    <thead>
                        <tr class="bg-primary">
                            <td class="small text-center font-weight-bold text-light px-3 py-2">WDO Ref. #</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">PTR #</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Return Date</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Delivery Date</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Item Name</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Batch No.</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Returned Qty</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Created By</td>
                            <?php if($_SESSION['type'] == "admin"){?>
                                <td class="small text-center font-weight-bold text-light px-3 py-2"></td>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($asar_return as $return_id => $return_val) { ?>
                            <tr>
                                <th sorted="row" class="small text-center font-weight-bold"><?php echo "{$return_val['out_ref_num']}"; ?></th>
                                <td class="small text-center"><?php echo "{$return_val['ptr_num']}"; ?></td>
                                <td class="small text-center"><?php echo "{$return_val['return_date']}"; ?></td>
                                <td class="small text-center"><?php echo "{$return_val['out_date']}"; ?></td>
                                <td class="small text-center"><?php echo "{$return_val['item_name']}"; ?></td>
                                <td class="small text-center"><?php echo "{$return_val['batch_num']}"; ?></td>
                                <td class="small text-center"><?php echo "{$return_val['return_qty']}"; ?></td>
                                <td class="small text-center"><?php echo "{$return_val['created_by']}"; ?></td>
                                <?php if($_SESSION['type'] == "admin"){?>
                                    <td class="small text-center">
                                        <a href="<?php echo "remove_return.php?return_id={$return_val['return_id']}"; ?>" class="btn btn-primary btn-sm">Remove</a>
                                    </td>  
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
include("layout/footer.php");
?>

<!-- Datatables -->

<script>
    $(document).ready(function() {
        $('#view_return_table').DataTable({
            "order": [
				[3, "asc"]
			]
        });
    });
</script>