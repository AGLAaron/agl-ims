<?php
session_start();
include('inc/db/bd_connect.php'); // Db Connection
include('inc/agl_ct.php'); // Constant
include('inc/agl_fn.php'); // Functions
?>

<?php
include("layout/head.php");
include("layout/main_nav.php");
include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">View</a></li>
            <li class="breadcrumb-item active">View Locked Items</li>
        </ul>
    </div>
</div>
<?php
         //INSERT PROMPT
		if (isset($_SESSION['insert_res_type'])) {
			echo "<div class = \"container-fluid\">";
				echo "<div class=\"alert alert-{$_SESSION['insert_res_type']}\">";
					echo "{$_SESSION['insert_res']}";
				echo "</div>";
			echo "</div>";	

			unset($_SESSION['insert_res_type']);
			unset($_SESSION['insert_res']);
				
		}



?>
<?php

$query = "SELECT * FROM lock_items ORDER by lock_id ASC";
$result = mysqli_query($conn, $query);
$asar_lock = array();

while ($db_rows = mysqli_fetch_assoc($result)) {
    $asar_lock[] = $db_rows;
}

?>

<div class="container-fluid mt-3">
    <div class="card">
        <div class="card-header align-items-center">
            <h4>Locked Items</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-md" id="view_locked_items">
                    <thead>
                        <tr class="bg-primary">
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Property #</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Location</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Program</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Item Name</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Batch No.</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Expiration Date</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Unit</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Unit Cost</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Qty</td>
                            <?php if($_SESSION['type'] == "admin"){?>
                                <td class="small text-center font-weight-bold text-light px-3 py-2"></td>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($asar_lock as $lock_id => $lock_item_det ) { ?>
                            <tr>
                                <th sorted="row" class="small text-center font-weight-bold"><?php echo "{$lock_item_det ['property_num']}"; ?></th>
                                <td class="small text-center text-orange"><?php echo "{$lock_item_det['s_loc']}"; ?></td>
                                <td class="small text-center"><?php echo "{$lock_item_det['item_program']}"; ?></td>
                                <td class="small text-center"><?php echo "{$lock_item_det['item_name']}"; ?></td>
                                <td class="small text-center"><?php echo "{$lock_item_det['item_batch_num']}"; ?></td>
                                <td class="small text-center"><?php echo "{$lock_item_det['item_expiry']}"; ?></td>
                                <td class="small text-center"><?php echo "{$lock_item_det['item_unit']}"; ?></td>
                                <td class="small text-center"><?php echo "{$lock_item_det['item_unit_cost']}"; ?></td>
                                <td class="small text-center"><?php echo "{$lock_item_det['qty']}"; ?></td>
                                <?php if($_SESSION['type'] == "admin"){?>
                                    <td class="small text-center">
                                        <a href="<?php echo "unlock_item.php?lock_id={$lock_item_det['lock_id']}"; ?>" class="btn bg-green btn-sm">Unlock</a>
                                    </td>  
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
include("layout/footer.php");
?>

<!-- Datatables -->

<script>
    $(document).ready(function() {
        $('#view_locked_items').DataTable({
            "order": [
				[3, "asc"]
			]
        });
    });
</script>