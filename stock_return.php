<?php
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

?>

<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>  

<?php

    $all_out = get_db_out($conn);
    $all_items = get_available_items($conn);
    $all_consignee = get_all_out_cons($conn,"outbound_destination")
    //print_r_html($all_order);

?>
    
    <!-- Breadcrumb-->
    <div class="breadcrumb-holder mb-2">
    <div class="container-fluid">
        <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">Misc</a></li>
        <li class="breadcrumb-item active">Stock Return</li>
        </ul>
    </div>
    </div>
    <?php
        //Messages

        if(isset($_SESSION['submit_res'])){
            echo "<div class = \"container-fluid\">";
                echo "<div class=\"alert alert-{$_SESSION['submit_res_type']}\">";
                    echo "{$_SESSION['submit_res']}";
                echo "</div>";
            echo "</div>";

            unset($_SESSION['submit_res']);
            unset($_SESSION['submit_res_type']);
        }
        

        if(isset($_GET['ref_no'])){
            if(empty(trim($_GET['ref_no']))){
                $_SESSION['submit_res'] = "<b>Error:</b> Please Fill all Fields!";
                $_SESSION['submit_res_type'] = "danger";
            }else{
                /**
                Verify Field if Input it does exist
                 */
                $all_out = get_db_out($conn);
    
                if(!is_out_exist($_GET['ref_no'],$all_out)){
                    $_SESSION['submit_res'] = "<b>Error:</b> Make sure to Enter the Correct Outbound Reference Number!";
                    $_SESSION['submit_res_type'] = "danger";
                }else{
                    /**
                    Get the Details
                     */

                        /**
                        * Get all from tb_order where out_ref_num = $_GET['ref_no']
                        */

                        $query = "SELECT * FROM tb_outbound";
                        $query .= " WHERE out_ref_num = '{$_GET['ref_no']}'";

                        $result = mysqli_query($conn,$query);

                        $out_array_details = array();
                        $out_item_details = array();

                        while($db_rows = mysqli_fetch_assoc($result)){
                            /**
                             * Outbound Ref. No
                               Shipper
                               Consignee
                               PTR No.
                               Transfer type
                               Outbound Date
                               Remarks
                             */
                            $out_array_details ['out_ref_num']= $db_rows['out_ref_num'];
                            $out_array_details ['out_shipper']= $db_rows['out_shipper'];
                            $out_array_details ['out_consignee']= $db_rows['out_consignee'];
                            $out_array_details ['out_ptr_num']= $db_rows['out_ptr_num'];
                            $out_array_details ['out_date']= $db_rows['out_date'];
                            $out_array_details ['out_transfer_type']= $db_rows['out_trans_type'];
                            $out_array_details ['out_remarks']= $db_rows['out_remarks'];


                            $out_item_details[$db_rows['out_ref_num']][$db_rows['id']]['property_num'] = $db_rows['out_property_num'];
                            $out_item_details[$db_rows['out_ref_num']][$db_rows['id']]['item_name'] = $db_rows['out_item_name'];
                            $out_item_details[$db_rows['out_ref_num']][$db_rows['id']]['qty'] = $db_rows['out_qty'];
                            ['out_unit_cost'];

                        }
                    
                     
                }
    
            }
            $form_id_count = 0;
        }

    ?>
    <div class="container-fluid">
        <form action="stock_return_proc.php" method="get">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="card">
                        <div class="card-header align-items-center">
                            <h4>Outbound Information</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" name="f_out_ref" placeholder="Reference No." class="form-control" data-toggle="tooltip" data-placement="top" title="Outbound Reference No." value="<?php echo "{$out_array_details['out_ref_num']}";?>">
                                    </div>
                                    <div class="col-lg-6">
                                        <input  placeholder="Enter/Select Date" name="f_pick_date" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="Select Date" value="<?php echo"{$out_array_details['out_date']}";?>">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-lg-6">
                                        <select name="f_shipper" id="" class="form-control">
                                            <option value="<?php echo "{$out_array_details['out_shipper']}";?>"><?php echo "{$out_array_details['out_shipper']}";?></option>
                                            <option value="">Select Shipper</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <input list="dest_list" name="f_dest" placeholder="Select/Enter Consignee" class="form-control" value="<?php echo "{$out_array_details['out_consignee']}";?>">
                                        <datalist id="dest_list">
                                            <?php foreach($all_consignee as $consignee_key => $consignee_val){ ?>
                                                <option value="<?php echo $consignee_key.",".$consignee_val; ; ?>"><?php echo "{$consignee_val}"; ?></option>
                                            <?php } ?>
                                        </datalist>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-lg-6">
                                        <input type="text" name="f_ptr" placeholder="Enter PTR No." class="form-control" value="<?php echo "{$out_array_details['out_ptr_num']}";?>">
                                    </div>
                                    <div class="col-lg-6">
                                        <input list="trans_type_list" name="f_trans_type" placeholder="Select/Enter Transfer Type" class="form-control" value="<?php echo "{$out_array_details['out_transfer_type']}";?>">
                                        <datalist id="trans_type_list">
                                            <option value="Donation"></option>
                                            <option value="Reassignment"></option>
                                        </datalist>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col">
                                        <input type="text" placeholder="Remarks" name="f_remarks" class="form-control" value="<?php echo "{$out_array_details['out_remarks']}";?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header align-items-center">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4>Outbound Item Information</h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                        <div class="form-group item_info_field">
                                <?php foreach($out_item_details as $out_ref_num => $out_arr_items){?>
                                    <?php foreach($out_arr_items as $out_id => $out_det){?>
                                            <div class="row mb-3"  id="<?php echo "{$form_id_count}"; ?>">
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                            <input list="property_datalist" name="f_property_no[]" placeholder="Enter Property No." class="form-control" value="<?php echo"{$out_det['property_num']}"; ?>">
                                                            <datalist id="property_datalist">
                                                            <?php foreach($all_items as $item_key => $arr_val){ ?>
                                                                    <option value="<?php echo $item_key; ?>"><span class="text-justify"><?php echo $arr_val['item_batch_num']."-".$arr_val['item_name'];?></span class="text-justify"></option>
                                                            <?php } ?>
                                                    </datalist>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <input type="number" name="f_qty[]" placeholder="Enter Quantity" class="form-control" value="<?php echo"{$out_det['qty']}"; ?>">
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-12 text-left">
                                                    <a href="javascript:void(0);" class="remove_item_button btn btn-danger form-control" onclick=remove_item("<?php echo"{$form_id_count}"; ?>") title="Remove Item">-</a>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-12 text-left">
                                                    <a href="javascript:void(0);" class="add_item_button btn btn-primary form-control" title="Add Item">+</a>
                                                </div>	
                                            </div>	
                                    <?php $form_id_count++;} ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 justify-content-center pb-3">
                            <div>
                                <button type="submit" name="return_delivery" class="form-control btn btn-primary" >Return Delivery</button>
                            </div>
                        </div>		
                        <div class="col-lg-12 justify-content-center pb-3">
                            <div>
                                <a href="view_order.php" class="form-control btn btn-secondary">Cancel</a>
                            </div>
                        </div>							
                    </div>
                </div>
            </div>
        </form>	 		
    </div>
    

<?php
    include("layout/footer.php"); 
?>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 1000; //Input fields increment limitation
		var addButton = $('.add_item_button'); //Add button selector
		var removeButton = $('.remove_item_button'); // Remove Button Selector
    var wrapper = $('.item_info_field'); //Input field wrapper	

    var x = 1; //Initial field counter is 1
	  var form_id_count = 0;
    //Once add button is clicked
    $(addButton).click(function(){

    	var form_id = "add-form-"+form_id_count;
    	console.log(form_id);
    	 var fieldHTML = '<div class="form-group" id="'+form_id+'"><div class="row mb-3"><div class="col-lg-3 col-md-3 col-sm-12"><input list="property_datalist" name="f_property_no[]" placeholder="Enter Property No." class="form-control"></div><div class="col-lg-3 col-md-3 col-sm-12"><input type="number" name="f_qty[]" placeholder="Enter Quantity" class="form-control"></div><div class="col-lg-1 col-md-1 col-sm-12 text-left"><a href="javascript:void(0);" class="remove_item_button btn btn-danger form-control" onclick=remove_item("'+form_id+'") title="Remove Item">-</a></div></div>';
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            form_id_count++;
            $(wrapper).append(fieldHTML); //Add field html  
        }
		});
		

	
});

 function remove_item($id){
	document.getElementById($id).remove(); //Remove field html
	
 }

</script>