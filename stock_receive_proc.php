<?php
    ob_start();
    session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php');
	include('inc/agl_ct.php');
	include('inc/agl_fn.php');
?>

<?php 

    if(isset($_POST['add_stock_receive'])){ // isset start
        /**
         * First Field Verification
         */

        if(empty(trim($_POST['shipper'])) || empty(trim($_POST['consignee'])) || empty(trim($_POST['date_receive'])) || empty(trim($_POST['ptr_no'])) || empty(trim($_POST['remarks'])) || !array_filter($_POST['item_name']) || !array_filter($_POST['batch_num']) || !array_filter($_POST['expiry']) || !array_filter($_POST['unit']) || !array_filter($_POST['unit_cost']) || !array_filter($_POST['amount']) || !array_filter($_POST['qty'])  ){
            
            $_SESSION['fill_res'] = "<b>Error:</b> Please Fill All Fields!";
            $_SESSION['fill_res_type'] = "danger";
            header("Location: stock_receive.php");
        }else{
            /**
             * Second Verification of Fields
             * This step checks all array for EmptyString or Space Inputs
             */

            $arr_item_name = $_POST['item_name'];
            $arr_batch_num = $_POST['batch_num'];
            $arr_expiry = $_POST['expiry'];
            $arr_unit = $_POST['unit'];
            $arr_unit_cost = $_POST['unit_cost'];
            $arr_amount = $_POST['amount'];
            $arr_qty = $_POST['qty'];
            
            if(is_array_has_empty_input($arr_item_name) || is_array_has_empty_input($arr_batch_num) || is_array_has_empty_input($arr_expiry) || is_array_has_empty_input($arr_unit) || is_array_has_empty_input($arr_unit_cost)|| is_array_has_empty_input($arr_amount) || is_array_has_empty_input($arr_amount) || is_array_has_empty_input($arr_qty)){

                $_SESSION['fill_res'] = "<b>Error:</b> Item Information are not Filled. Please Fill All Fields!";
                $_SESSION['fill_res_type'] = "danger";
                header("Location: stock_receive.php");

            }else{
                /**
                 * Associative Array that will contain all info from the submitted Form
                 */
                $asar_form_info = array();
                /**
                 * Associative Array that will contain the stock receive information
                 * Array contents will be pushed in the Database
                 */
                $arr_stock_receive = array();

                $asar_form_info['item_name'] = $arr_item_name;
                $asar_form_info['batch_num'] = $arr_batch_num;
                $asar_form_info['expiry'] = $arr_expiry;
                $asar_form_info['unit'] = $arr_unit;
                $asar_form_info['unit_cost'] = $arr_unit_cost;
                $asar_form_info['amount'] = $arr_amount;
                $asar_form_info['qty'] = $arr_qty;

                $arr_count = count($asar_form_info['item_name']); // Determines the number of items
                $start_index = 0;
                
                while($start_index < $arr_count){
                    foreach($asar_form_info as $asar_item_key => $asar_item_val){
                        $arr_stock_receive[$start_index][$asar_item_key] = $asar_item_val[$start_index];
                    }
                    $start_index++;
                }

                /**
                 * Stock Receive Information
                 */
                $ref_num = generate_ref_num(4);
                $ptr_no = remove_junk(esc_str($conn,$_POST['ptr_no']));
                $shipper = remove_junk(esc_str($conn,$_POST['shipper']));
                $consignee = remove_junk(esc_str($conn,$_POST['consignee']));
                $date_receive = remove_junk(esc_str($conn,$_POST['date_receive']));
                $remarks  = remove_junk(esc_str($conn,$_POST['remarks']));
                $date_created = date('Y-m-d');
                $created_by = remove_junk(esc_str($conn,$_SESSION['name']));

                /**
                 * Insert To Database
                 * Clean first each data using remove_junk() and esc_str()
                 * Before Inserting to Database
                 */

                 $query = "INSERT INTO tb_receive";
                 $query .= " (ptr_no,ref_num, shipper, consignee, receive_date, item_name, batch_num, expiry, qty, unit, unit_cost, amount, remarks, date_created, created_by) VALUES";

                 foreach($arr_stock_receive as $stock_key => $arr_stock_details){
                     /**
                      * Stock Receive Item Information
                      */
                     $item_name = remove_junk(esc_str($conn,$arr_stock_details['item_name']));
                     $batch_num = remove_junk(esc_str($conn,$arr_stock_details['batch_num']));
                     $expiry = remove_junk(esc_str($conn,$arr_stock_details['expiry']));
                     $unit = remove_junk(esc_str($conn,$arr_stock_details['unit']));
                     $unit_cost = remove_junk(esc_str($conn,$arr_stock_details['unit_cost']));
                     $amount = remove_junk(esc_str($conn,$arr_stock_details['amount']));
                     $qty = remove_junk(esc_str($conn,$arr_stock_details['qty']));

                    
                     $query .= "  (";
                     $query .= "'{$ptr_no}','{$ref_num}', '{$shipper}', '{$consignee}', '{$date_receive}', '{$item_name}', '{$batch_num}', '{$expiry}', '{$qty}', '{$unit}', '{$unit_cost}', '{$amount}', '{$remarks}', '{$date_created}', '{$created_by}'";
                     $query .= "),";  
                 }

                 $query = trim($query,","); // Remove comma and the last part of the query

                 if($conn->query($query) === TRUE){
                    $_SESSION['insert_res'] = "<b>Success:</b> Stock Receive Successfully Created! Reference No: <b>{$ref_num}</b>";
                    $_SESSION['insert_res_type'] = "success";
                    header("Location:stock_receive.php");
                 }else{
                    $_SESSION['insert_res'] = "<b>Error:</b> Stock Receive Failed! Reference No: <b>{$ref_num}</b>";
                    $_SESSION['insert_res_type'] = "danger";
                    header("Location:stock_receive.php");
                 }
            }

        }//else end

    }//isset end
?>