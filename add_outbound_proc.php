<?php 
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions



	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

		
		/**
		* Add Outbound Form is submitted
		*/
		if(isset($_POST['add_outbound'])){ // isset Start

			if (empty(trim($_POST['order_ref_num'])) || empty(trim($_POST['f_shipper'])) || empty(trim($_POST['f_dest'])) || empty(trim($_POST['f_ptr'])) || empty(trim($_POST['f_trans_type'])) || empty(trim($_POST['f_pick_date'])) || empty(trim($_POST['f_remarks'])) || !array_filter($_POST['f_property_no']) || !array_filter($_POST['f_qty']) ) {

				$_SESSION['submit_response'] = "<b>Error:</b> Please Fill All Fields!";
				$_SESSION['submit_res_type'] = "danger";
				header("Location:add_outbound.php");

			}else{ //Else Start
				

				if(is_array_has_empty_input($_POST['f_property_no']) || is_array_has_empty_input($_POST['f_qty'])){
					$_SESSION['submit_response'] = "<b>Error:</b> Please Fill All Fields!";
					$_SESSION['submit_res_type'] = "danger";
					header("Location:add_outbound.php");
				}else{
					/**
					* Get All Available Items
					*/
					$all_items = get_available_items($conn,"items");


					/**
					Check first the submitted items if it does exist in All Items
					 */

					 if(!are_all_items_exist($_POST['f_property_no'],$all_items)){
						$_SESSION['submit_response'] = "<b>Error:</b> Please Make Sure to Enter the Correct Property No.!";
						$_SESSION['submit_res_type'] = "danger";
						header("Location:add_inbound.php");
					 }else{
						/**
						* Before Any Calculations and Recording of Outbound Transactions
						* Check first the Quantity of Each Stock 
						* If There will be a negative result or
						* if Outbound Qty is > than the Running Balance
						* Throw an Error Message saying: "{property_no}-{item name} current balance is {running balance}. Outbound Quantity Exceeds the Current Balance: {outbound__qty} "
						*/

						/**
						* Outbound item infromation 
						* @var [Array]
						*/
						$arr_property_num = $_POST['f_property_no'];
						$arr_qty = $_POST['f_qty'];

						$asar_items = array();
						$asar_items['property_num'] = $arr_property_num;
						$asar_items['qty'] = $arr_qty;


						$asar_items_count = count($asar_items['property_num']); //echobr("Array Count: ".$asar_items_count);
						$arr_start_index = 0;

						// Building the Orders Array Before Inserting In DB
						// First is Property Number and Quantity
						$asar_order_items = array();

						while($arr_start_index < $asar_items_count){
							foreach ($asar_items as $asar_items_key => $asar_items_arr_val) {
								$asar_order_items[$arr_start_index][$asar_items_key] = $asar_items_arr_val[$arr_start_index];
							}
							$arr_start_index++;
						}


						//Next we will go to all items to get the Details of the Ordered Item
						// $bal_err_count = 0;

						// foreach($asar_order_items as $arr_key => $arr_val){
						// 	$property_num = $arr_val['property_num'];

						// 	//Check to all Items it this Exist
						// 	if($all_items[$property_num]){
						// 		//Check the Outbound Qty vs Current Balance
						// 		if((int)$arr_val['qty']>(int)$all_items[$property_num]['qty']){
						// 			$_SESSION['balance_response'] = "Error: <span class=\"text-bold\">{$arr_val['property_num']}</span> outbound of <span class=\"text-bold\">{$arr_val['qty']}</span> exceeds the Current Balance of <span class=\"text-bold\">{$all_items[$property_num]['qty']}</span>";
						// 			$_SESSION['balance_res_type'] = "danger";
						// 			header("Location:add_outbound.php");
						// 			$bal_err_count++;
						// 		}
						// 	}
						// }

						//No Error on Balance
						if(is_balance_ok($all_items,$asar_order_items)){ // No error on Balance Start

							/**
							* Generate Outbound Reference Number
							* 2 = outbound
							*/
						
							$ref_num = generate_ref_num(2);
							$outbound_created_by = remove_junk(esc_str($conn,$_SESSION['name']));
							$outbound_created = date('Y-m-d'); 
							

							$order_ref_num = remove_junk(esc_str($conn,$_POST['order_ref_num']));
							$shipper = remove_junk(esc_str($conn,$_POST['f_shipper']));
							$consignee = remove_junk(esc_str($conn,$_POST['f_dest']));
							$ptr_no = remove_junk(esc_str($conn,$_POST['f_ptr']));
							$transfer_type = remove_junk(esc_str($conn,$_POST['f_trans_type']));
							$pick_date = remove_junk(esc_str($conn,$_POST['f_pick_date']));
							$remarks = remove_junk(esc_str($conn,$_POST['f_remarks']));

							/**
							* Build the Order Array Start
							*/
							
							foreach($asar_order_items as $arr_key => $arr_val){
								$property_num = $arr_val['property_num'];

								//Check to all Items it this Exist
								if($all_items[$property_num]){
									//It exists so we are going to enter the details in the [asar_order_items] array
									$asar_order_items[$arr_key]['batch_num'] = $all_items[$property_num]['item_batch_num'];
									$asar_order_items[$arr_key]['name'] = $all_items[$property_num]['item_name'];
									$asar_order_items[$arr_key]['expiry'] = $all_items[$property_num]['item_expiry'];
									$asar_order_items[$arr_key]['unit'] = $all_items[$property_num]['item_unit'];
									$asar_order_items[$arr_key]['unit_cost'] = $all_items[$property_num]['item_unit_cost'];
									$asar_order_items[$arr_key]['amount'] = $all_items[$property_num]['item_unit_cost'] * $asar_order_items[$arr_key]['qty'];
									$asar_order_items[$arr_key]['running_bal'] = $all_items[$property_num]['qty'];	
								}
							}

						

							// Build Order Array End

							/**
							* INSERT OUTBOUND RECORD TO DATABASE
							*/

							$query = "INSERT INTO tb_outbound";
							$query.=" (order_ref_num,out_ref_num,out_shipper,out_consignee,out_ptr_num,out_date,out_trans_type,out_property_num,out_item_name,out_batch_num,out_expiry,out_qty,out_unit,out_unit_cost,out_amount,out_remarks,balance,out_created,out_created_by";
							$query .=") VALUES";

							foreach($asar_order_items as $order_items_key => $order_items_val){//Foreach Start

								$property_num = remove_junk(esc_str($conn,$order_items_val['property_num']));
								$item_name = remove_junk(esc_str($conn,$order_items_val['name']));
								$batch_num = remove_junk(esc_str($conn,$order_items_val['batch_num']));
								$expiry = remove_junk(esc_str($conn,$order_items_val['expiry']));
								$qty = remove_junk(esc_str($conn,$order_items_val['qty']));
								$unit = remove_junk(esc_str($conn,$order_items_val ['unit']));
								$unit_cost = remove_junk(esc_str($conn,$order_items_val['unit_cost']));
								$amount = remove_junk(esc_str($conn,$order_items_val['amount']));
								$running_balance = remove_junk(esc_str($conn,$order_items_val['running_bal']));
								
								$balance = $running_balance - $qty; // Balance as Of and also be used in the update on the running balance

								$query .= " (";
								$query .= "'{$order_ref_num}','{$ref_num}','{$shipper}','{$consignee}','{$ptr_no}','{$pick_date}','{$transfer_type}','{$property_num}','{$item_name}','{$batch_num}','{$expiry}','{$qty}','{$unit}','{$unit_cost}','{$amount}','{$remarks}','{$balance}','{$outbound_created}','{$outbound_created_by}'";
								$query .= "),";
							}//Foreach End

							$query = trim($query,",");

							if($conn->query($query) === TRUE){

								$_SESSION['insert_response'] = "<b>Success:</b> Outbound Transaction Successfully Created - Reference No: <b>{$ref_num}</b>";
								$_SESSION['insert_res_type'] = "success";
								$_SESSION['last_activity'] = time(); // Update Last Activity

								/**
								* Update Running Balance
								*/
								foreach($asar_order_items as $order_item_key => $order_items_val){
									$property_num = remove_junk(esc_str($conn,$order_items_val['property_num']));
									$qty = remove_junk(esc_str($conn,$order_items_val['qty']));
									$running_balance = remove_junk(esc_str($conn,$order_items_val['running_bal']));
									$balance = $running_balance - $qty; // Balance as Of and also be used in the update on the running balance

									$upd_query = "UPDATE available_items";
									$upd_query .= " SET qty = {$balance}";
									$upd_query .= " WHERE property_num  = '{$property_num}'";
									
									if ($conn->query($upd_query) === TRUE) {
										$_SESSION['update_response'] =  "<b>Success:</b> All Balance Updated Successfully!";
										$_SESSION['update_res_type'] = "success";
										$_SESSION['last_activity'] = time(); //Update Last Activity
									}else{
											
										//Failed Update
										$_SESSION['update_response'] = "<b>Error:</b> Running Balance Update Failed for item <b>{$property_num}</b>";
										$_SESSION['update_res_type'] = "danger";
										$_SESSION['last_activity'] = time(); //Update Last Activity
										header("Location:add_outbound.php");
									}
								}

							}else{

								//Failed Insert
								$_SESSION['insert_response'] = "<b>Error:</b> Outbound Transaction Failed";
								$_SESSION['insert_res_type'] = "danger";
								$_SESSION['last_activity'] = time(); // Update Last Activity
								header("Location:add_outbound.php");

							}
							//Update Last Activity
							$_SESSION['last_activity'] = time();
							header("Location:add_outbound.php");

						}// No Error On balance
						else{
							$_SESSION['balance_response'] = "Error: <span class=\"text-bold\"> Outbound Quantity exceeds the Current Balance!</span> Make Sure to Check the Current Balance.";
							$_SESSION['balance_res_type'] = "danger";
							header("Location:add_outbound.php");
						}

					 }//Third ELse End (Verification if all submitted items exist)
				}// second else verification End
			}//Else End

		}//isset End
 ?>
