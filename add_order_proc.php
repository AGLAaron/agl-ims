<?php 
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	/**
	 * Get All Items
	 * It will be used in the Form That the User Will Select
	 */
	$all_items = get_available_items($conn,"items");

	/**
	 * Get All Batch Numbers
	 */
	$all_batch_no = get_all_batch_no($conn,"items");

  	/**
	 * Get all unit so it will be displayed in the form
	 * @var [type]
	 */
	
	$all_unit = get_all_unit($conn,"unit");
	/**
	 * All Consignee/Receiver
	 * @var [type]
	 */
	$all_consignee = get_all_out_cons($conn,"outbound_destination");
		

	if(isset($_POST['confirm_order'])){//isset Start


	 		// Validate all Fields if Filled
	 		if(empty(trim($_POST['f_order_type'])) || empty(trim($_POST['f_shipper'])) || empty(trim($_POST['f_dest'])) || empty(trim($_POST['f_ptr'])) || empty(trim($_POST['f_trans_type'])) || empty(trim($_POST['f_pick_date'])) || empty(trim($_POST['f_remarks'])) || !array_filter($_POST['f_property_no']) || !array_filter($_POST['f_qty'])){

	 				$_SESSION['submit_response'] = "<b>Error:</b> Please Fill All Fields!";
					$_SESSION['submit_res_type'] = "danger";
					header("Location:add_order.php");
	 			

	 		}else{ // validate else start

	 			$ref_num = generate_ref_num(1); // Reference Number
	 			$order_created_by = remove_junk(esc_str($conn,$_SESSION['name']));
	 			$date_order_created = date('Y-m-d');

	 			// echobr("Name:".$order_created_by);
	 			// echobr("Reference Number:".$ref_num);

	 			$order_type = remove_junk(esc_str($conn,$_POST['f_order_type']));
	 			$shipper = remove_junk(esc_str($conn,$_POST['f_shipper']));
	 			$consignee = remove_junk(esc_str($conn,$_POST['f_dest']));
	 			$ptr_no = remove_junk(esc_str($conn,$_POST['f_ptr']));
	 			$transfer_type = remove_junk(esc_str($conn,$_POST['f_trans_type']));
	 			$pick_date = remove_junk(esc_str($conn,$_POST['f_pick_date']));
	 			$remarks = remove_junk(esc_str($conn,$_POST['f_remarks']));

	 			//Item Info fields
	 			$arr_property_num = $_POST['f_property_no'];
		 		$arr_qty = $_POST['f_qty'];


		 		$asar_items = array();
		 		$asar_items['property_num'] = $arr_property_num;
		 		$asar_items['qty'] = $arr_qty;

		 		$asar_items_count = count($asar_items['property_num']); //echobr("Array Count: ".$asar_items_count);
		 		$arr_start_index = 0;

		 		// Building the Orders Array Before Inserting In DB
		 		// First is Property Number and Quantity
		 		$asar_order_items = array();

		 		while($arr_start_index < $asar_items_count){
		 			foreach ($asar_items as $asar_items_key => $asar_items_arr_val) {
		 				$asar_order_items[$arr_start_index][$asar_items_key] = $asar_items_arr_val[$arr_start_index];
		 			}
		 			$arr_start_index++;
		 		}
		 		
		 		//Next we will go to all items to get the Details of the Ordered Item
		 		foreach($asar_order_items as $arr_key => $arr_val){
		 			$property_num = $arr_val['property_num'];

		 			//Check to all Items it this Exist
		 			if($all_items[$property_num]){
		 				//It exists so we are going to enter the details in the [asar_order_items] array
		 				$asar_order_items[$arr_key]['batch_num'] = $all_items[$property_num]['item_batch_num'];
		 				$asar_order_items[$arr_key]['name'] = $all_items[$property_num]['item_name'];
		 				$asar_order_items[$arr_key]['expiry'] = $all_items[$property_num]['item_expiry'];
		 				$asar_order_items[$arr_key]['unit'] = $all_items[$property_num]['item_unit'];
		 				$asar_order_items[$arr_key]['unit_cost'] = $all_items[$property_num]['item_unit_cost'];
						 $asar_order_items[$arr_key]['amount'] = $all_items[$property_num]['item_unit_cost'] * $asar_order_items[$arr_key]['qty'];
						 $asar_order_items[$arr_key]['sloc'] = $all_items[$property_num]['s_loc'];
		 			}
		 		}

		 		$arr_insert_err = array();
		 		
		 		// Once we build the Array we will now insert it to the database
		 		foreach ($asar_order_items as $order_items_key => $order_items_val) {
		 			$property_num = remove_junk(esc_str($conn,$order_items_val['property_num']));
		 			$item_name = remove_junk(esc_str($conn,$order_items_val['name']));
		 			$batch_num = remove_junk(esc_str($conn,$order_items_val['batch_num']));
		 			$expiry = remove_junk(esc_str($conn,$order_items_val['expiry']));
		 			$qty = remove_junk(esc_str($conn,$order_items_val['qty']));
		 			$unit = remove_junk(esc_str($conn,$order_items_val ['unit']));
		 			$unit_cost = remove_junk(esc_str($conn,$order_items_val['unit_cost']));
					$amount = remove_junk(esc_str($conn,$order_items_val['amount']));
					$sloc = remove_junk(esc_str($conn,$order_items_val['sloc']));
		 			
		 			$query = "INSERT INTO tb_order";
		 			$query.= " (";
		 			$query .= "order_type,order_ref_num,order_shipper,order_consignee,order_ptr_num,order_release_date,order_transfer_type,order_property_num,order_item_name,order_batch_num,order_expiry,order_qty,order_unit,order_unit_cost,order_amount,order_remarks,order_created_by,order_date,sloc";
		 			$query .=") VALUES (";
		 			$query .="'{$order_type}','{$ref_num}','{$shipper}','{$consignee}','{$ptr_no}','{$pick_date}','{$transfer_type}','{$property_num}','{$item_name}','{$batch_num}','{$expiry}','{$qty}','{$unit}','{$unit_cost}','{$amount}','{$remarks}','{$order_created_by}','{$date_order_created}','{$sloc}'";
		 			$query .= ")";

		 			if ($conn->query($query) === TRUE) {
					   $arr_insert_err [] = 1;
					} else {
					    if($conn->error){
					    	$insert_err = $conn->error;

					    	//Failed Insert
		 					$_SESSION['success_response'] = "<b>Error:</b> Order Transaction Failed!";
							$_SESSION['success_res_type'] = "danger";
							$_SESSION['last_activity'] = time(); // Update Last activity
							header("Location:add_order.php");
					    }
					}

		 		}

		 		if(array_filter($arr_insert_err)){
		 			
				    $_SESSION['success_response'] =  "Success: Order Successful! Reference No.: <b>{$ref_num}</b>";
					$_SESSION['success_res_type'] = "success";
					$_SESSION['last_activity'] = time(); // Update Last Activity
					header("Location:add_order.php");
		 		}

	 				

	 		} //Validate else End

	 }//isset end



 ?>