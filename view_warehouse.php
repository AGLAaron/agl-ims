<?php 
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	header("refresh: 600"); 
	
?>
<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Master Data</a></li>
	        <li class="breadcrumb-item active">Warehouse</li>
	        <li class="breadcrumb-item active">View Warehouse</li>
	      </ul>
	    </div>
	  </div>
	<?php 
		$query = "SELECT * FROM warehouse_code";
		$result = mysqli_query($conn,$query);
	 ?>
	<div class="container-fluid mt-3">
		<div class="card">
			<div class="card-header align-items-center">
				<h4>Warehouses</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-md" id="view_user_tbl">
					  <thead>
					    <tr class="bg-primary">
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Id</th>
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Warehouse Code</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php while ($db_rows = mysqli_fetch_assoc($result)) {
					  		
					  	?>
						    <tr>
						      <th class="small text-center"><?php echo $db_rows['id']; ?></th>
						      <td class="small text-center"><?php echo $db_rows['Warehouse_Code']; ?></td>
						      
					    <?php 
						}
					     ?>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php
	include("layout/footer.php"); 
?>
 <script>
  	$(document).ready(function() {
	    $('#view_user_tbl').DataTable();
	} );
  </script>