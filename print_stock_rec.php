<?php   
    ob_start();
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions
	include('fpdf/fpdf.php'); // fpdf
	include('fpdf/easytable/exfpdf.php'); // exfpdf
    include('fpdf/easytable/easyTable.php'); // easytable
    

    if(isset($_GET['ref_no'])){

        //echobr($_GET['ref_no']);

        $query = "SELECT * FROM tb_receive WHERE ref_num = '{$_GET['ref_no']}'";

        $result = mysqli_query($conn,$query);
        $num_rows = mysqli_num_rows($result); //Number of rows
        $arr_stock_rec_details = array();
        $arr_stock_rec_item_details = array();

        while($db_rows = mysqli_fetch_assoc($result)){
            $arr_stock_rec_details[$db_rows['ref_num']]['ref_num'] = $db_rows['ref_num'];
            $arr_stock_rec_details[$db_rows['ref_num']]['shipper'] = $db_rows['shipper'];
            $arr_stock_rec_details[$db_rows['ref_num']]['consignee'] = $db_rows['consignee'];
            $arr_stock_rec_details[$db_rows['ref_num']]['ref_num'] = $db_rows['ref_num'];
            $arr_stock_rec_details[$db_rows['ref_num']]['date_receive'] = $db_rows['receive_date'];
            $arr_stock_rec_details[$db_rows['ref_num']]['ref_num'] = $db_rows['ref_num'];
            $arr_stock_rec_details[$db_rows['ref_num']]['remarks'] = $db_rows['remarks'];
            $arr_stock_rec_details[$db_rows['ref_num']]['date_created'] = $db_rows['date_created'];
            $arr_stock_rec_details[$db_rows['ref_num']]['created_by'] = $db_rows['created_by'];

            /**
             * Item Information
             */

             $arr_stock_rec_item_details[$db_rows['receive_id']]['item_name'] = $db_rows['item_name']; 
             $arr_stock_rec_item_details[$db_rows['receive_id']]['batch_num'] = $db_rows['batch_num']; 
             $arr_stock_rec_item_details[$db_rows['receive_id']]['expiry'] = $db_rows['expiry']; 
             $arr_stock_rec_item_details[$db_rows['receive_id']]['qty'] = $db_rows['qty']; 
             $arr_stock_rec_item_details[$db_rows['receive_id']]['unit'] = $db_rows['unit']; 
             $arr_stock_rec_item_details[$db_rows['receive_id']]['unit_cost'] = $db_rows['unit_cost']; 
             $arr_stock_rec_item_details[$db_rows['receive_id']]['amount'] = $db_rows['amount'];  
        }

        //print_r_html($arr_stock_rec_details);

        //print_r_html($arr_stock_rec_item_details);


        $pdf = new exFPDF('P','mm','Legal');

        /**
         * Legal is 216 x 356 mm
         */
        $pdf->AddPage();
        $pdf->SetFont('arial','',10);

        /**
         * Header
         */
        $tb_header=new easyTable($pdf, 2);
        $tb_header->easyCell('', 'img:img/invoice-logo.png, w50; align:L;');
        $tb_header->easyCell('RECEIVING RECEIPT', 'font-size:20; font-style:B; font-color:#424344; align:R');
        $tb_header->printRow();
        $tb_header->endTable(2);
        
        /**
         * Form Details
         */

        $table=new easyTable($pdf, '{130,30,50}');

        $table->rowStyle('font-size:8');
        $table->easyCell('Warehouse A and B, New Bypass Road'."\n".' Mamay Road,Buhangin'."\n".'Davao City,Philippines', 'rowspan:4; valign:T'); 
        $table->easyCell('<b>Receive Date:</b>', '');
        $table->easyCell($arr_stock_rec_details[$_GET['ref_no']]['date_receive'],'border:1; border-color:#afb5bf; align:R');
        $table->printRow();
        
        $table->rowStyle('font-size:8'); 
        $table->easyCell('<b>Ref. #:</b>', '');
        $table->easyCell($_GET['ref_no'],'border:1; border-color:#afb5bf; align:R');
        $table->printRow();

        $table->rowStyle('font-size:8'); 
        $table->easyCell('<b>Created By:</b>', '');
        $table->easyCell($arr_stock_rec_details[$_GET['ref_no']]['created_by'],'border:1; border-color:#afb5bf; align:R');
        $table->printRow();

        $table->rowStyle('font-size:8'); 
        $table->easyCell('<b>Date Created:</b>', '');
        $table->easyCell($arr_stock_rec_details[$_GET['ref_no']]['date_created'],'border:1; border-color:#afb5bf; align:R');
        $table->printRow();
        $table->endTable(5);

        /**
         * Shipper and Consignee 
         */
        
        $tableB = new easyTable($pdf,'{105,105}');

        $tableB->rowStyle('font-size:11'); 
        $tableB->easyCell('RECEIVING DETAILS','bgcolor:#424344; border-color:#424344;border:1; align:L; font-color:#ffffff; font-style:B; colspan:2');
        $tableB->printRow();

        $tableB->rowStyle('font-size:9; border:LRB; border-color:#afb5bf'); 
        $tableB->easyCell('<b>Shipper: </b>'.$arr_stock_rec_details[$_GET['ref_no']]['shipper']);
        $tableB->easyCell('<b>Consignee: </b>'.$arr_stock_rec_details[$_GET['ref_no']]['consignee']);
        $tableB->printRow();
        $tableB->endTable(5);

        /**
         * Table Headers 
         */

        $tableC = new easyTable($pdf,'{70,35,30,15,15,20,25}');
		
        $tableC->rowStyle('border:1; border-color:#afb5bf; bgcolor:#424344');
        $tableC->easyCell('Item Name','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
        $tableC->easyCell('Batch No.','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
        $tableC->easyCell('Expiry Date','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
        $tableC->easyCell('Qty','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
        $tableC->easyCell('Unit','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
        $tableC->easyCell('Unit Cost','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
        $tableC->easyCell('Amount','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
        $tableC->printRow();

        $maxRow = 15;
		  $initRow=0;
		  $i=0;
		  if($num_rows > $maxRow){
		  	
		  	 foreach($arr_stock_rec_item_details as $arr_key =>$arr_item_det){
			 	$bgcolor='';
				    if($i%2){
				       $bgcolor='bgcolor:#afb5bf;';
				    }

					$tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
				    $tableC->easyCell($arr_item_det['item_name']);
				    $tableC->easyCell($arr_item_det['batch_num']);
				    $tableC->easyCell($arr_item_det['expiry']);
				    $tableC->easyCell($arr_item_det['qty']);
				    $tableC->easyCell($arr_item_det['unit']);
				    $tableC->easyCell($arr_item_det['unit_cost']);
				    $tableC->easyCell($arr_item_det['amount']);
				    $tableC->printRow();
				    $i++;
				    $maxRow--;


			 }

		  }else{
		  	 

            foreach($arr_stock_rec_item_details as $arr_key =>$arr_item_det){
                $bgcolor='';
                   if($i%2){
                      $bgcolor='bgcolor:#afb5bf;';
                   }

                   $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
                   $tableC->easyCell($arr_item_det['item_name']);
                   $tableC->easyCell($arr_item_det['batch_num']);
                   $tableC->easyCell($arr_item_det['expiry']);
                   $tableC->easyCell($arr_item_det['qty']);
                   $tableC->easyCell($arr_item_det['unit']);
                   $tableC->easyCell($arr_item_det['unit_cost']);
                   $tableC->easyCell($arr_item_det['amount']);
                   $tableC->printRow();
                   $i++;
                   $maxRow--;


            }

			 while($maxRow!=0){
			 		if($i%2){
				       $bgcolor='bgcolor:#afb5bf;';
				    }
			 		 $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('-');
				    $tableC->printRow();
				    $i++;
				    $maxRow--;
			 }
		  }
		 
          $tableC->endTable(0);
          
          /**
           * Remarks
           */
          $tableD = new easyTable($pdf,'{70,35,30,15,15,20,5,20}','split-row:true');

		  $tableD->rowStyle('font-size:7; split-row:false');
		  $tableD->easyCell('','colspan:7; paddingY:2');
		  $tableD->printRow();

		  $tableD->rowStyle('font-size:7');
          $tableD->easyCell('SPECIAL NOTE(S) AND ACTION','font-style:B;colspan:4; bgcolor:#424344; border:1; border-color:#424344; font-color:#ffffff;');
          $tableD->easyCell('');
          $tableD->easyCell('<s "font-size:8">Sub-Total</s>','align:L;');
          $tableD->easyCell('<b><s "align:L">P</s></b>','font-size:8; border:TLB; border-color:#afb5bf');
          $tableD->easyCell('-','align:R; font-size:8;border: TRB;  border-color:#afb5bf');
          $tableD->printRow();

          $tableD->easyCell($arr_stock_rec_details[$_GET['ref_no']]['remarks'],'font-style:B;colspan:4;align:C; border:LRB; border-color:#afb5bf;rowspan:5');
		  $tableD->easyCell('');
		  $tableD->easyCell('<s "font-size:8">Total</s>','align:L;');
		  $tableD->easyCell('<b><s "align:L">P</s></b>','font-size:8; border:TLB; border-color:#afb5bf');
		  $tableD->easyCell('-','align:R; font-size:8;border: TRB;  border-color:#afb5bf');
		  $tableD->printRow();

          $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();

		  $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();
		  
		  $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();

		  $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();
		  $tableD->endTable(5);

        }

        $pdf->sign_field_pick_list($_SESSION['name']); // Sign Area
        $pdf->Output(); //To Print and to indicate the filename
    

?>