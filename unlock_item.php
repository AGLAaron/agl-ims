<?php   
    session_start();
    include('inc/db/bd_connect.php'); // Db Connection
    include('inc/agl_ct.php'); // Constant
    include('inc/agl_fn.php'); // Functions


    if(isset($_GET['lock_id'])){
        
        $aux = array();

        $lock_id = remove_junk(esc_str($conn,$_GET['lock_id']));
        
        $query = "SELECT * FROM lock_items";
        $query .= " WHERE lock_id = '{$lock_id}'";
        $result = mysqli_query($conn,$query);

        while($db_rows = mysqli_fetch_assoc($result)){
            $aux[] = $db_rows;
        }

        $delete_db_entry = delete_db_entry($conn,$lock_id,"lock_items");

        if($delete_db_entry){
            /**
            INSERT NOW TO available_items
             */
             $query = "INSERT INTO available_items";
             $query .= " (item_batch_num, item_program, property_num, s_loc, item_name, item_expiry, item_unit, item_unit_cost, item_status, qty)";

             foreach($aux as $db_id => $db_det){
                 $batch_num = remove_junk(esc_str($conn,$db_det['item_batch_num']));
                 $program = remove_junk(esc_str($conn,$db_det['item_program']));
                 $property_num = remove_junk(esc_str($conn,$db_det['property_num']));
                 $sloc = remove_junk(esc_str($conn,$db_det['s_loc']));
                 $name = remove_junk(esc_str($conn,$db_det['item_name']));
                 $expiry = remove_junk(esc_str($conn,$db_det['item_expiry']));
                 $unit = remove_junk(esc_str($conn,$db_det['item_unit']));
                 $unit_cost = remove_junk(esc_str($conn,$db_det['item_unit_cost']));
                 $qty = remove_junk(esc_str($conn,$db_det['qty']));

                 $query .= " VALUES (";
                 $query .= " '{$batch_num}', '{$program}', '{$property_num}', '{$sloc}', '{$name}', '{$expiry}', '{$unit}', '{$unit_cost}', '301', '{$qty}'";
                 $query .= "),";
             }

             $query = trim($query,",");

             if($conn->query($query) === TRUE){
                $_SESSION['insert_res'] = "<b>Unlocked:</b> {$batch_num} - {$name}";
				$_SESSION['insert_res_type'] = "success";
				header("Location:view_locked_items.php");
             }else{
                $_SESSION['insert_res'] = "<b>Error:</b> $conn->error </b>";
				$_SESSION['insert_res_type'] = "danger";
				header("Location:view_locked_items.php");
             }
        }
        
    }//isset end


?>
