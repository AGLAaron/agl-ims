<?php
session_start();
include('inc/db/bd_connect.php'); // Db Connection
include('inc/agl_ct.php'); // Constant
include('inc/agl_fn.php'); // Functions
?>

<?php
include("layout/head.php");
include("layout/main_nav.php");
include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
<div class="breadcrumb-holder">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">View</a></li>
            <li class="breadcrumb-item active">View Available Items</li>
        </ul>
    </div>
</div>
<?php
    //INSERT PROMPT
    if (isset($_SESSION['insert_res_type'])) {
        echo "<div class = \"container-fluid\">";
            echo "<div class=\"alert alert-{$_SESSION['insert_res_type']}\">";
                echo "{$_SESSION['insert_res']}";
            echo "</div>";
        echo "</div>";	

        unset($_SESSION['insert_res_type']);
        unset($_SESSION['insert_res']);
            
    }


$query = "SELECT * FROM available_items WHERE qty <> 0 ORDER by available_item_id ASC";
$result = mysqli_query($conn, $query);
$asar_avail = array();

while ($db_rows = mysqli_fetch_assoc($result)) {
    $asar_avail[] = $db_rows;
}

?>

<div class="container-fluid mt-3">
    <div class="card">
        <div class="card-header align-items-center">
            <h4>Available Items</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-hover table-md" id="view_available_items">
                    <thead>
                        <tr class="bg-primary">
                            
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Property #</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Location</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Program</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Item Name</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Batch No.</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Expiration Date</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Unit</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Unit Cost</td>
                            <td class="small text-center font-weight-bold text-light px-3 py-2">Qty</td>
                            <?php if($_SESSION['type'] == "admin"){?>
                                <td class="small text-center font-weight-bold text-light px-3 py-2"></td>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($asar_avail as $avail_id => $avail_item_det ) { ?>
                            <tr>
                                <th sorted="row" class="small text-center font-weight-bold"><?php echo "{$avail_item_det ['property_num']}"; ?></th>
                                <td class="small text-center text-orange"><?php echo "{$avail_item_det   ['s_loc']}"; ?></td>
                                <td class="small text-center"><?php echo "{$avail_item_det   ['item_program']}"; ?></td>
                                <td class="small text-center"><?php echo "{$avail_item_det   ['item_name']}"; ?></td>
                                <td class="small text-center"><?php echo "{$avail_item_det   ['item_batch_num']}"; ?></td>
                                <td class="small text-center"><?php echo "{$avail_item_det   ['item_expiry']}"; ?></td>
                                <td class="small text-center"><?php echo "{$avail_item_det   ['item_unit']}"; ?></td>
                                <td class="small text-center"><?php echo "{$avail_item_det   ['item_unit_cost']}"; ?></td>
                                <td class="small text-center"><?php echo "{$avail_item_det   ['qty']}"; ?></td>
                                <?php if($_SESSION['type'] == "admin"){?>
                                    <td class="small text-center">
                                        <a href="<?php echo "lock_item.php?id={$avail_item_det    ['available_item_id']}"; ?>" class="btn bg-red btn-sm">LOCK</a>
                                    </td>  
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
include("layout/footer.php");
?>

<!-- Datatables -->

<script>
   $(document).ready(function() {
		$('#view_available_items').DataTable({
			"order": [
				[3, "asc"]
			]
		});
	});
</script>