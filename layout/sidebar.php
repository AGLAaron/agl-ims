<?php 
	// Sidebar Menu
	// Contains All Page for the System
  
 ?>

 <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center justify-content-center">
            <div class="avatar"><img src="
              <?php 
              if($_SESSION['type'] == "admin"){
                echo "img/agl-logo.ico";
              }else{
                echo "img/default_avatar.jpg";
              }
              ?>" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4"><?php echo $_SESSION['name']; ?></h1>
              <p><?php echo $_SESSION['type']; ?></p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
            <?php 
              if($_SESSION['type'] == "admin" ||$_SESSION['type'] == "user" || $_SESSION['type'] == "view" ){
            ?>
                <li class="<?php if (basename($_SERVER['PHP_SELF'])=="index.php") {echo "active";} ?>"><a href="index.php"><i class="fas fa-home"></i>Home </a></li>
            <?php 
            }
            ?>
            <?php if($_SESSION['type'] == "admin"){?>
              <!-- Master Data -->
            <li class="
            <?php 
            if(basename($_SERVER['PHP_SELF']) == "add_user.php" || basename($_SERVER['PHP_SELF'])=="view_user.php" || basename($_SERVER['PHP_SELF'])=="manage_user.php" || basename($_SERVER['PHP_SELF'])=="add_item.php" || basename($_SERVER['PHP_SELF'])=="view_item.php" || basename($_SERVER['PHP_SELF'])=="manage_item.php"){ 
              echo "active";
               } ?>">
               <a href="#master_actions" aria-expanded="false" data-toggle="collapse"> <i class="fas fa-database"></i>Master Data </a>
              <ul id="master_actions" class="collapse list-unstyled ">
	                <li class="<?php 
                              if(basename($_SERVER['PHP_SELF'])=="add_item.php" || basename($_SERVER['PHP_SELF']) == "view_item.php" || basename($_SERVER['PHP_SELF']) == "manage_item.php"){
                                echo "active";
                              }
                             ?>"><a href="#item_actions" aria-expanded="false" data-toggle="collapse" class="text-small"><i class="fas fa-cubes"></i><strong>Items</strong></a>
	                	<ul id="item_actions" class="collapse list-unstyled">
		                	<li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_item.php"){echo "active";} ?>"><a href="add_item.php" class=""><i class="fas fa-plus-square"></i>Add Item</a></li>
		                	<li class="<?php if(basename($_SERVER['PHP_SELF']) == "view_item.php"){
                        echo "active";
                      } ?>"><a href="view_item.php"><i class="fas fa-eye"></i>View Items</a></li>
		                	<li class="<?php if(basename($_SERVER['PHP_SELF']) == "manage_item.php"){echo "active";} ?>"><a href="manage_item.php"><i class="fas fa-edit"></i>Manage Item(s)</a></li>
                      <li class="<?php if(basename($_SERVER['PHP_SELF']) == "manage_item.php"){echo "active";} ?>"><a href="manage_item.php"><i class="fas fa-edit"></i>Item(s) Descriptions</a></li>
                    </ul>
	                </li>
	                
	                <li><a href="#user_actions" aria-expanded="false" data-toggle="collapse"><i class="fas fa-users"></i>Users</a>
		                	<ul id="user_actions" class="collapse list-unstyled">
  		                	<li class="<?php if(basename($_SERVER['PHP_SELF'])=="add_user.php"){echo "active";} ?>"><a href="add_user.php"><i class="fas fa-user-plus"></i>Add User</a></li>
  		                	<li class="<?php if(basename($_SERVER['PHP_SELF'])=="view_user.php"){echo "active";} ?>"><a href="view_user.php"><i class="fas fa-eye"></i>View Users</a></li>
  		                	<li class="<?php if(basename($_SERVER['PHP_SELF'])=="manage_user.php"){echo "active";} ?>"><a href="manage_user.php"><i class="fas fa-edit"></i>Manage User(s)</a></li>
                        <li class="<?php if(basename($_SERVER['PHP_SELF'])=="manage_user.php"){echo "active";} ?>"><a href="manage_user.php"><i class="fas fa-edit"></i>Store</a></li>
		                </ul>
	                </li>
                  <li><a href="view_store_loc.php" aria-expanded="false"><i class="fas fa-map"></i>Store Location</a>	
	                </li>
                  <li><a href="view_warehouse.php" aria-expanded="false"><i class="fas fa-warehouse"></i>Warehouse Code</a>	
	                </li>
                  <li><a href="#" aria-expanded="false" data-toggle="collapse"><i class="fas fa-warehouse"></i>Vendor</a>	
	                </li>
                  <li><a href="#" aria-expanded="false" data-toggle="collapse"><i class="fas fa-list"></i>Item Category</a>	
	                </li>
                  
              </ul>
            </li>
            <li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_inbound.php" || basename($_SERVER['PHP_SELF']) == "view_inbound.php" || basename($_SERVER['PHP_SELF']) == "manage_inbound.php"){
                    echo "active";
               } ?>">
              <a href="#inbound_action" aria-expanded="false" data-toggle="collapse"><i class="fas fa-box"></i>Inbound</a>
              <ul id="inbound_action" class="collapse list-unstyled">
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_inbound.php"){echo "active";} ?>"><a href="add_inbound.php"><i class="fas fa-people-carry"></i>Create Inbound</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "view_inbound.php"){echo "active";} ?>"><a href="view_inbound.php"><i class="fas fa-eye"></i>View Inbounds</a></li>
                <!-- <li class="<?php if(basename($_SERVER['PHP_SELF']) == "manage_inbound.php"){echo "active";} ?>"><a href="manage_inbound.php"><i class="fas fa-edit"></i>Manage Inbound(s)</a></li> -->
              </ul>
            </li>


           
            
            <li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_outbound.php" || basename($_SERVER['PHP_SELF']) == "view_outbound.php" || basename($_SERVER['PHP_SELF']) == "manage_outbound.php"){ echo "active";} ?>">
              <a href="#outbound_action" aria-expanded="false" data-toggle="collapse"><i class="fas fa-truck-loading"></i>Outbound</a>
              <ul id="outbound_action" class="collapse list-unstyled">
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_outbound.php"){ echo "active";} ?>"><a href="add_outbound.php"><i class="fas fa-box-open"></i>Create Outbound</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "view_outbound.php"){echo "active";} ?>"><a href="view_outbound.php"><i class="fas fa-eye"></i>View Outbounds</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "manage_outobund.php"){echo "active";} ?>"><a href="manage_outbound.php"><i class="fas fa-edit"></i>Manage Outbound(s)</a></li>
              </ul>
            </li>
            
             
            <!-- MISCELLANOUS ACTIONS -->
            <li class="<?php if(basename($_SERVER['PHP_SELF']) == "stock_receive.php" || basename($_SERVER['PHP_SELF']) == "add_lock_inbound.php"){ echo "active";} ?>">
              <a href="#misc_action" aria-expanded="false" data-toggle="collapse"><i class="fas fa-tags"></i>Misc</a>
              <ul id="misc_action" class="collapse list-unstyled">
                <!-- Stock Receive -->
                <li><a href="#stock_receive_list" aria-expanded="false" data-toggle="collapse"><i class="fas fa-file-alt"></i>Receive</a>
                  <ul id="stock_receive_list" class="collapse list-unstyled">
                      <li class="<?php if(basename($_SERVER['PHP_SELF']) == "stock_receive.php"){ echo "active";} ?>">
                        <a href="stock_receive.php"><i class="fas fa-box-open"></i>Stock Receive</a>
                      </li>
                      <li class="<?php if(basename($_SERVER['PHP_SELF']) == "view_stock_receive.php"){ echo "active";} ?>">
                        <a href="view_stock_receive.php"><i class="fas fa-eye"></i>View Received Stock</a>
                      </li>
                  </ul>
                </li>
                <!-- Inbound Lock Items -->
                <li>
                  <a href="#lock_in_ac" aria-expanded="false" data-toggle="collapse"><i class="fas fa-lock"></i>Inbound (Lock)</a>
                  <ul id="lock_in_ac" class="collapse list-unstyled">
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_lock_inbound.php"){echo "active";}?>">
                      <a href="add_lock_inbound.php"><i class="fas fa-people-carry"></i>Create Inbound</a>
                    </li>
                    <li class="<?php ?>">
                      <a href=""><i class="fas fa-eye"></i>View Lock Inbound</a>
                    </li>
                  </ul>
                </li>
                
              </ul>
            </li>

          <?php 
          } 
          ?>

          <?php 
            if($_SESSION['type'] == "user" || $_SESSION['type'] == "admin" ){
          ?>
            <li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_order.php" || basename($_SERVER['PHP_SELF']) == "view_order.php" || basename($_SERVER['PHP_SELF']) == "manage_order.php"){ echo "active";} ?>">
              <a href="#order_action" class="" data-toggle="collapse" aria-expanded="false"><i class="fas fa-store-alt"></i>Order</a>
              <ul id="order_action" class="collapse list-unstyled">
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "add_order.php"){echo "active";} ?>"><a href="add_order.php"><i class="fas fa-shopping-cart"></i>Create Order</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "view_order.php"){echo "active";} ?>"><a href="view_order.php"><i class="fas fa-eye"></i>View Orders</a></li>
                <?php 
                  if ($_SESSION['type'] == "admin") {
                 ?>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "manage_order.php"){echo "active";} ?>"><a href="manage_order.php"><i class="fas fa-edit"></i>Manage Orders(s)</a></li>
                <?php 
                 }
                 ?>
              </ul>
            </li>
            <li class="<?php if(basename($_SERVER['PHP_SELF']) == "stock_report.php" || basename($_SERVER['PHP_SELF']) == "inbound_report.php" || basename($_SERVER['PHP_SELF']) == "outbound_report.php"){ echo "active";} ?>">
              <a href="#report_action" class="" data-toggle="collapse" aria-expanded="false"><i class="fas fa-file-alt"></i>Report</a>
               <ul id="report_action" class="collapse list-unstyled">
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "stock_report.php"){echo "active";} ?>"><a href="stock_report.php" class=""><i class="fas fa-shopping-cart"></i>Summary Report</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "item_summary_report.php"){echo "active";} ?>"><a href="item_summary_report.php" class=""><i class="fas fa-clipboard"></i></i>Item Summary Report</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "inbound_report.php"){echo "active";} ?>"><a href="inbound_report.php"><i class="fas fa-eye"></i>Inbound Report</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "outbound_report.php"){echo "active";} ?>"><a href="outbound_report.php"><i class="fas fa-edit"></i>Outbound Report</a></li>
                <li class="<?php if(basename($_SERVER['PHP_SELF']) == "stock_receive_report.php"){echo "active";} ?>"><a href="stock_receive_report.php"><i class="fas fa-edit"></i>Stock Receive Report</a></li>
              </ul>
            </li>
          
            <!-- Views -->
            <li>
                 <a href="#views_action" data-toggle="collapse" aria-expanded="false"><i class="fas fa-eye"></i>Views</a>
                 <ul id="views_action" class="collapse list-unstyled">
                    <li class="<?php if(basename($_SERVER['PHP_SELF']) == "view_stock_receive.php"){ echo "active";} ?>">
                      <a href="view_stock_receive.php"><i class="fas fa-eye"></i>View Received Stock</a>
                    </li>
                 </ul>
            </li>
          <?php
            }
          ?>
           
        </nav>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <?php if(basename($_SERVER['PHP_SELF']) == "add_user.php" || basename($_SERVER['PHP_SELF']) == "view_user.php" || basename($_SERVER['PHP_SELF']) == "manage_user.php"  ){  ?>
              <?php echo "<h2 class=\"no-margin-bottom\"> Users </h2>"; ?>
          	  <?php } ?>
              <?php if(basename($_SERVER['PHP_SELF']) == "add_item.php" || basename($_SERVER['PHP_SELF']) == "view_item.php" || basename($_SERVER['PHP_SELF']) == "manage_item.php"  ){  ?>
              <?php echo "<h2 class=\"no-margin-bottom\">Items </h2>"; ?>
              <?php } ?>
          	  <?php if(basename($_SERVER['PHP_SELF']) == "index.php"){  ?>
              <?php echo "<h2 class=\"no-margin-bottom\"> Dashboard </h2>"; ?>
          	  <?php } ?>
              <?php if(basename($_SERVER['PHP_SELF']) == "add_inbound.php" || basename($_SERVER['PHP_SELF']) == "view_inbound.php" || basename($_SERVER['PHP_SELF']) == "manage_inbound.php"){  ?>
              <?php echo "<h2 class=\"no-margin-bottom\"> Inbound </h2>"; ?>
              <?php } ?>
            </div>
          </header>