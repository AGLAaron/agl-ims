<?php
    session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
    include('inc/agl_fn.php'); // Functions
    

    if(isset($_GET['return_delivery'])){ // isset Start
        
        /**
        First Verification
        f_out_ref
        f_pick_date
        f_shipper
        f_dest
        f_ptr
        f_trans_type
        f_remarks
        f_property_no
        f_qty
         */
        if(empty(trim($_GET['f_out_ref'])) || empty(trim($_GET['f_pick_date'])) || empty(trim($_GET['f_shipper'])) || empty(trim($_GET['f_dest'])) || empty(trim($_GET['f_ptr'])) || empty(trim($_GET['f_trans_type'])) || empty(trim($_GET['f_remarks'])) || !array_filter($_GET['f_property_no']) || !array_filter($_GET['f_qty'])){

            $_SESSION['submit_response'] = "<b>Error:</b> Failed to Complete <b>Stock Return Transaction</b> Please Fill All Fields!";
            $_SESSION['submit_res_type'] = "danger";
            header("Location:view_outbound.php");
            

        }else{ // validate else start

            if(is_array_has_empty_input($_GET['f_property_no']) || is_array_has_empty_input($_GET['f_qty'])){
                $_SESSION['submit_response'] = "<b>Error:</b> Failed to Complete <b>Stock Return Transaction</b> Please Fill All Fields!";
                $_SESSION['submit_res_type'] = "danger";
                header("Location:view_outbound.php");
            }else{
                 /**
                * Get All Items
                * It will be used in the Form That the User Will Select
                */
                $all_items = get_available_items($conn,"items");
                /**
                * All Consignee/Receiver
                * @var [type]
                */
                $all_consignee = get_all_out_cons($conn,"outbound_destination");

                $are_all_items_exist = are_all_items_exist($_GET['f_property_no'], $all_items);

                if(!$are_all_items_exist){
                    $_SESSION['submit_response'] = "<b>Error:</b> Please Make Sure to Enter the Correct Property No.!";
                    $_SESSION['submit_res_type'] = "danger";
                    header("Location:view_outbound.php");
                }else{

                     //Item Info fields
                    $arr_property_num = $_GET['f_property_no'];
                    $arr_qty = $_GET['f_qty'];

                    $asar_items = array();
                    $asar_items['property_num'] = $arr_property_num;
                    $asar_items['qty'] = $arr_qty;

                    $asar_items_count = count($asar_items['property_num']); //echobr("Array Count: ".$asar_items_count);
                    $arr_start_index = 0;

                    // Building the Orders Array Before Inserting In DB
                    // First is Property Number and Quantity
                    $asar_out_items = array();

                    while($arr_start_index < $asar_items_count){
                        foreach ($asar_items as $asar_items_key => $asar_items_arr_val) {
                            $asar_out_items[$arr_start_index][$asar_items_key] = $asar_items_arr_val[$arr_start_index];
                        }
                        $arr_start_index++;
                    }


                    //Next we will go to all items to get the Details of the Ordered Item
                    foreach($asar_out_items as $arr_key => $arr_val){
                        $property_num = $arr_val['property_num'];

                        //Check to all Items it this Exist
                        if($all_items[$property_num]){
                            //It exists so we are going to enter the details in the [asar_out_items] array
                            $asar_out_items[$arr_key]['batch_num'] = $all_items[$property_num]['item_batch_num'];
                            $asar_out_items[$arr_key]['name'] = $all_items[$property_num]['item_name'];
                            $asar_out_items[$arr_key]['expiry'] = $all_items[$property_num]['item_expiry'];
                            $asar_out_items[$arr_key]['unit'] = $all_items[$property_num]['item_unit'];
                            $asar_out_items[$arr_key]['unit_cost'] = $all_items[$property_num]['item_unit_cost'];
                            $asar_out_items[$arr_key]['amount'] = $all_items[$property_num]['item_unit_cost'] * $asar_out_items[$arr_key]['qty'];
                        }
                    }
                        
                    

                    $return_ref_num = generate_ref_num(5);
                    $out_ref_num = remove_junk(esc_str($conn,$_GET['f_out_ref'])); // Reference Number
                    $shipper = remove_junk(esc_str($conn,$_GET['f_shipper']));
                    $consignee = remove_junk(esc_str($conn,$_GET['f_dest']));
                    $ptr_num = remove_junk(esc_str($conn,$_GET['f_ptr']));
                    $out_date = remove_junk(esc_str($conn,$_GET['f_pick_date']));
                    $return_date = date('Y-m-d');
                    $trans_type = remove_junk(esc_str($conn,$_GET['f_trans_type']));
                    $created_by = remove_junk(esc_str($conn,$_SESSION['name']));
                    $date_created = date('Y-m-d');
                    $remarks = remove_junk(esc_str($conn,$_GET['f_remarks']));


   
                    $query = "INSERT INTO return_items";
		 			$query.= " (";
                    $query .= "return_ref_num, out_ref_num, shipper, consignee, ptr_num, out_date, return_date, trans_type, property_num, item_name, batch_num, expiry, unit, unit_cost, amount, remarks, created_by, date_created, return_qty";
                    $query .= ") VALUES ";

                    foreach ($asar_out_items as $order_items_key => $order_items_val) {
                        $property_num = remove_junk(esc_str($conn,$order_items_val['property_num']));
                        $item_name = remove_junk(esc_str($conn,$order_items_val['name']));
                        $batch_num = remove_junk(esc_str($conn,$order_items_val['batch_num']));
                        $expiry = remove_junk(esc_str($conn,$order_items_val['expiry']));
                        $return_qty = remove_junk(esc_str($conn,$order_items_val['qty']));
                        $unit = remove_junk(esc_str($conn,$order_items_val ['unit']));
                        $unit_cost = remove_junk(esc_str($conn,$order_items_val['unit_cost']));
                        $amount = remove_junk(esc_str($conn,$order_items_val['amount']));
                        
                        $query .= " (";
                        $query .= "'{$return_ref_num}','{$out_ref_num}','{$shipper}','{$consignee}','{$ptr_num}','{$out_date}','{$return_date}','{$trans_type}','{$property_num}','{$item_name}','{$batch_num}','{$expiry}', '{$unit}','{$unit_cost}','{$amount}','{$remarks}','{$created_by}','{$date_created}','{$return_qty}'";
                        $query .= "),";
                    }
                     
                    $query = trim($query,",");

                    if($conn->query($query) === TRUE){
                        $_SESSION['insert_res'] = "<b>Success:</b> Stock Return Successfully Created! Reference No: <b>{$return_ref_num}</b>";
                        $_SESSION['insert_res_type'] = "success";

                        /**
                         * Update Outbound Status to 402 means Return
                         400 default
                         401 in transit
                         402 returned
                         */
                         foreach($asar_out_items as $out_key => $out_val){
                            $property_num = remove_junk(esc_str($conn,$out_val['property_num']));

                            $upd = "UPDATE tb_outbound";
                            $upd .= " SET status='402'";
                            $upd .= " WHERE out_ref_num = '{$out_ref_num}' AND out_property_num = '{$property_num}'";

                            if ($conn->query($upd) === TRUE) {
                                $_SESSION['update_res'] = "<b>Success:</b> Outbound Status Successfully Updated! Reference No: <b>{$return_ref_num}</b>";
                                $_SESSION['update_res_type'] = "success";
                                
                            }else{
                                    
                                //Failed Update
                                $_SESSION['update_res'] = "<b>Error:</b> Outbound Status Update Failed for item <b>{$property_num}</b>";
                                $_SESSION['update_res_type'] = "danger";
                                $_SESSION['last_activity'] = time(); //Update Last Activity
                                header("Location:view_outbound.php");
                            }
                         }
                        
                         //Update Last Activity
                        $_SESSION['last_activity'] = time();
                        header("Location:view_outbound.php");
                        
                    }else{
                        $_SESSION['insert_res'] = "<b>Error:</b> Stock Return Failed! Reference No: <b>{$return_ref_num}</b> $conn->error";
                        $_SESSION['insert_res_type'] = "danger";
                        header("Location:view_outbound.php");
                    }

                }

            }
        
        }

    }// Isset End
?>