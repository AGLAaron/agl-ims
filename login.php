<?php 
	//LOGIN
 	ob_start();
	session_start();
  	date_default_timezone_set("Asia/Manila");
	include("inc/db/bd_connect.php");
	include("inc/agl_fn.php");


	if(isset($_SESSION['type']))
	{
		header("location:index.php");
	}

 ?>

	<?php 
		if (isset($_POST['login_btn'])) {

			$user_name = $user_pass = "";
			$err_user_name = $err_user_pass = $err_user_inactive = "";

			if(empty(trim($_POST['user_name']))){
				$err_user_name = "Username Cannot be Blank";
			}else{
				$user_name = remove_junk($_POST['user_name']);
			}

			if(empty(trim($_POST['user_pass']))){
				$err_user_pass = "Password Cannot be Blank";
			}else{
				$user_pass = remove_junk($_POST['user_pass']);
			}

			if(!empty($user_name) && !empty($user_pass)){

				$user_name = remove_junk($conn->real_escape_string($user_name));
				$user_pass = remove_junk($conn->real_escape_string($user_pass));

				//Compare credentials from db
				$query = "SELECT * FROM";
				$query .= " users";
				$query .= " WHERE user_name = '{$user_name}'";

				$query = remove_junk($query);

				$result = mysqli_query($conn,$query);

				$num_rows = mysqli_num_rows($result);
				
				if($num_rows>0){

					foreach($result as $row){

						if($row['user_status'] == 1){

							if(password_verify($user_pass,$row['user_password'])){
								$_SESSION['type'] = $row['user_type'];
								$_SESSION['user_id'] = $row['user_id'];
								$_SESSION['user_name'] = $row['user_name'];
								$_SESSION['name'] = $row['Name'];
                				$_SESSION['last_activity'] = time();
								header("Location:index.php");
							}else{
								//echo "Wrong Password";
								$err_user_pass = "Wrong Password";
							}
						}else{
							//echo "User is Inactive";
							$err_user_inactive = "User is Inactive";
						}
					}
				}else{
					//echo "User does not Exist";
					$err_user_name = "User does not Exist";
				}
			}
		}
	 ?>

<!DOCTYPE html>
<html>
  <head>
   	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.green.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/agl-logo.ico">
    
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>A<span class="text-uppercase">Sys</span></h1>
                  </div>
                  <p>Warehouse Management System for DOH XI</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <form method="post" class="form-validate" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                  	<div class="form-group">
                  		<?php 
							if(!empty($err_user_name)){
								//echo "<div class = \"container-fluid\">";
									echo "<div class=\"alert alert-danger text-center\">";
										echo "<strong>{$err_user_name}</strong>";
									echo "</div>";
								//echo "</div>";
							}
							if(!empty($err_user_pass)){
								//echo "<div class = \"container-fluid\">";
									echo "<div class=\"alert alert-danger text-center\">";
										echo "<strong>{$err_user_pass}</strong>";
									echo "</div>";
								//echo "</div>";
							}
							if(!empty($err_user_inactive)){
								//echo "<div class = \"container-fluid\">";
									echo "<div class=\"alert alert-danger text-center\">";
										echo "<strong>{$err_user_inactive}</strong>";
									echo "</div>";
								//echo "</div>";
							}
						?>
                  	</div>
                    <div class="form-group">
                      <input id="login-username" type="text" name="user_name" required data-msg="Please enter your username" class="input-material">
                      <label for="login-username" class="label-material">User Name</label>
                    </div>
                    <div class="form-group">
                      <input id="login-password" type="password" name="user_pass" required data-msg="Please enter your password" class="input-material">
                      <label for="login-password" class="label-material">Password</label>
                    </div>
                    <div class="form-group">
                    	<button type="submit" name="login_btn" class="btn btn-primary btn-lg">Login</button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
  </body>
</html>