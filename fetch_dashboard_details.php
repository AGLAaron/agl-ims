<?php 
	
	/**
	 * Fetch Items
	 * @var string
	 */
	$item_query = "SELECT * FROM available_items";
	$item_result = mysqli_query($conn,$item_query);

	$asar_db_items = array();

	while($db_rows = mysqli_fetch_assoc($item_result)){
		$asar_db_items[$db_rows['available_item_id']] = $db_rows;
	}

	$asar_out_of_stock_items = array();

	foreach($asar_db_items as $arr_key => $arr_val){
		if($arr_val['qty'] == 0){
			$asar_out_of_stock_items[$arr_val['property_num']] = $arr_val['item_name'];
		}
	}

	$out_stock_count = count($asar_out_of_stock_items);
	if($out_stock_count !=0){
		 $out_stock_count = $out_stock_count;// Number of Stocks with 0 Balance
	}else{
		$out_stock_count = 0;
	}

	/**
	 * Fetch Orders
	 */
	
	$order_query = "SELECT * FROM tb_order";
	$order_result = mysqli_query($conn,$order_query);
	$date_today = date('Y-m-d');
	$asar_db_order = array();

	while($db_rows = mysqli_fetch_assoc($order_result)){
		$asar_db_order[$db_rows['order_ref_num']] = $db_rows['order_date'];
	}

	$new_order = 0;
	if(!empty($asar_db_order)){
		foreach($asar_db_order as $order_ref => $order_date){
			if(are_strings_equal($order_date,$date_today)){
				$new_order++;
			}
		}
	}else{
		$new_order = "-";
	}

	/**
	 * Fetch Inbound
	 */
	
	$in_query = "SELECT * FROM tb_inbound";
	$in_result = mysqli_query($conn,$in_query);
	$date_today = date('Y-m-d');
	$asar_db_in = array();

	while($db_rows = mysqli_fetch_assoc($in_result)){
		$asar_db_in[$db_rows['in_ref_num']] = $db_rows['in_created'];
	}

	$new_in = 0;
	if(!empty($asar_db_in)){
		foreach($asar_db_in as $in_ref => $in_date){
			if(are_strings_equal($in_date,$date_today)){
				$new_in++;
			}
		}
	}else{
		$new_in = "-";
	}

	/**
	 * Fetch Outbound
	 */
	
	$out_query = "SELECT * FROM tb_outbound";
	$out_result = mysqli_query($conn,$out_query);
	$date_today = date('Y-m-d');

	$asar_db_out = array();

	while($db_rows = mysqli_fetch_assoc($out_result)){
		$asar_db_out[$db_rows['out_ref_num']] = $db_rows['out_date'];
	}

	$new_out = 0;
	if(!empty($asar_db_out)){
		foreach($asar_db_out as $out_ref => $out_date){
			if(are_strings_equal($out_date,$date_today)){
				$new_out++;
			}
		}
	}else{
		$new_out = "-";
	}




	/**
	 * Fetch Orders
	 * @var [type]
	 */
	$all_allocate = get_all_order(1,"tb_order",$conn);

	$all_transit = get_all_order(2,"tb_order",$conn);

	$all_item_return = get_all_return($conn);
	

	$all_return = get_db_return($conn);

	$all_lock_items = get_db_lock_items($conn);

	$all_available_items = get_db_avail_items($conn);
	
 ?>