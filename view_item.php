<?php 
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	header("refresh: 600"); 
	
?>
<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>
	<!-- Breadcrumb-->
	  <div class="breadcrumb-holder">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Master Data</a></li>
	        <li class="breadcrumb-item active">Item</li>
	        <li class="breadcrumb-item active">View Items</li>
	      </ul>
	    </div>
	  </div>
	<?php 
		$query = "SELECT * FROM items";
		$result = mysqli_query($conn,$query);
	 ?>
	<div class="container-fluid mt-3">
		<div class="card">
			<div class="card-header align-items-center">
				<h4>Stock List</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-md" id="view_item_tbl">
					  <thead>
					    <tr class="bg-primary">
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Property No.</th>
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Item Name/Description</th>
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Batch No.</th>
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Expiration Date</th>
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Unit</th>
					      <th class="small text-center px-3 py-2 font-weight-bold text-light">Status</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php while ($db_rows = mysqli_fetch_assoc($result)) {
					  		
					  	?>
						    <tr>
						      <td class="small text-center font-weight-bold"><?php echo $db_rows['property_num']; ?></td>
						      <td class="small text-center font-weight-bold"><?php echo $db_rows['item_name']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['item_batch_num']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['item_expiry']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['item_unit']; ?></td>
						      <td class="small text-center">
						      	<?php if($db_rows['item_status'] == 301){
						      			echo "Active";
						      		}else{
								      	echo "Inactive";
								    } ;?></td>
						    </tr>
					    <?php 
						}
					     ?>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


  <?php
 	include("layout/footer.php"); 
  ?>
	<script>
	  	$(document).ready(function() {
		    $('#view_item_tbl').DataTable();
		} );
    </script>