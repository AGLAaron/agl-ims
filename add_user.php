<?php 
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // DB Connection
	include('inc/agl_fn.php'); //Functions
	include("layout/head.php");

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}
	
 ?>


 <?php
 	include("layout/main_nav.php"); 
 	include("layout/sidebar.php");
  ?>
	<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Users</a></li>
	        <li class="breadcrumb-item active">Add User</li>
	      </ul>
	    </div>
	  </div>
	
    <?php 
		//Required Fields
		$name = $user_name = $user_pass = $user_type = $user_status = "";
			
		//Error Fields
		$err_name = $err_user_name = $err_user_pass = $err_user_type = $err_user_type = "";

		if ($_SERVER['REQUEST_METHOD'] && isset($_POST['add_user'])) {

			//Verify name
			if(empty($_POST['name'])){
				$err_name="Name is Required";
			}else{
				$name=remove_junk($_POST['name']);
			}
			//Verify user_name
			if(empty($_POST['user_name'])){
				$err_user_name="Username is Required";
			}else{
				$user_name=remove_junk($_POST['user_name']);
			}

			//Verify user_pass
			if (empty($_POST['user_pass'])) {
				$err_user_pass = "Password is Required";
			}else{
				$user_pass=remove_junk($_POST['user_pass']);
			}

			//Verify user_type
			if(empty($_POST['user_type'])){
				$err_user_type="User Type is Required";
			}else{	
				$user_type = remove_junk($_POST['user_type']);
			}

			//Verify user_status
			if(empty($_POST['user_status'])){
				$err_user_status = "User Status is Required";
			}else{
				$user_status = remove_junk($_POST['user_status']);
			}

			//IF All fields are filled we insert it to database
			if(!empty($name) && !empty($user_name) && !empty($user_pass) && !empty($user_type) && !empty($user_status)){
		
				$user_name = remove_junk($conn->real_escape_string($user_name));
				$user_pass = remove_junk($conn->real_escape_string($user_pass));
				$user_type = remove_junk($conn->real_escape_string($user_type));
				$user_status = (int) remove_junk($conn->real_escape_string($user_status));

				$user_pass = password_hash($user_pass, PASSWORD_BCRYPT);

			 	$query = "INSERT into users (";
			 	$query .= "name,user_name, user_password, user_type, user_status";
			 	$query .= ") VALUES (";
			 	$query .= "'{$name}','{$user_name}','{$user_pass}','{$user_type}','{$user_status}'";
			 	$query .= ")";


			 	if ($conn->query($query) === TRUE) {
			    //echo "New record created successfully";
			    echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-success\">";
						echo "<strong>Success!</strong> New User Successfully Added";
					echo "</div>";
					echo "</div>";
					
				} else {
				    if($conn->error){
				    	$insert_err = $conn->error;
				    	echo "<div class = \"container-fluid\">";
							echo "<div class=\"alert alert-danger\">";
								echo "<strong>Failed to Add: </strong> User Not Created!";
							echo "</div>";
						echo "</div>";
				    }
				}

				
			}

		}

	 ?>
	
	<div class="container-fluid">
		<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<h4>Add User</h4>
						</div>
						<div class="card-body">
							<div class="form-group">
								<input type="text" name="name" placeholder="Enter Name" class="form-control">
								<?php  if(!empty($err_name)){ ?>
									<span class="error-validate"><?php echo $err_name;?></span>
								<?php } ?>
							</div>
							<div class="form-group">
								<input type="text" name="user_name" placeholder="User Name" class="form-control">
								<?php  if(!empty($err_user_name)){ ?>
									<span class="error-validate"><?php echo $err_user_name;?></span>
								<?php } ?>
							</div>
							<div class="form-group">
								<input type="password" name="user_pass" placeholder="Password" class="form-control">
								<?php  if(!empty($err_user_pass)){ ?>
									<span class="error-validate"><?php echo $err_user_pass;?></span>
								<?php } ?>
							</div>
							<div class="form-group">
								<select name="user_type" id="" class="form-control">
									<option value="">Select User Type</option>
									<option value="admin">Admin</option>
									<option value="user">User</option>
								</select>
								<?php  if(!empty($err_user_type)){ ?>
									<span class="error-validate"><?php echo $err_user_type;?></span>
								<?php } ?>
							</div>
							<div class="form-group">
								<select name="user_status" id="" class="form-control">
									<option value="">Select Status</option>
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
								<?php  if(!empty($err_user_status)){ ?>
									<span class="error-validate"><?php echo $err_user_status;?></span>
								<?php } ?>
							</div>
							<div class="form-group text-center">
								<button type="submit" name="add_user" class="btn btn-primary btn-lg">Add User</button>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</form>
	</div>
	


  <?php
 	include("layout/footer.php"); 
  ?>


