<?php   
    session_start();
    include('inc/db/bd_connect.php'); // Db Connection
    include('inc/agl_ct.php'); // Constant
    include('inc/agl_fn.php'); // Functions


    if(isset($_GET['id'])){
        
        $aux = array();

        $available_item_id = remove_junk(esc_str($conn,$_GET['id']));
        
        $query = "SELECT * FROM available_items";
        $query .= " WHERE available_item_id = '{$available_item_id}'";
        $result = mysqli_query($conn,$query);

        while($db_rows = mysqli_fetch_assoc($result)){
            $aux[] = $db_rows;
        }

        $delete_db_entry = delete_db_entry($conn,$available_item_id,"available_items");

        if($delete_db_entry){
            /**
            INSERT NOW TO available_items
             */
             $query = "INSERT INTO lock_items";
             $query .= " (item_batch_num, item_program, property_num, s_loc, item_name, item_expiry, item_unit, item_unit_cost, item_status, qty)";

             foreach($aux as $db_id => $db_det){
                 $batch_num = remove_junk(esc_str($conn,$db_det['item_batch_num']));
                 $program = remove_junk(esc_str($conn,$db_det['item_program']));
                 $property_num = remove_junk(esc_str($conn,$db_det['property_num']));
                 $sloc = remove_junk(esc_str($conn,$db_det['s_loc']));
                 $name = remove_junk(esc_str($conn,$db_det['item_name']));
                 $expiry = remove_junk(esc_str($conn,$db_det['item_expiry']));
                 $unit = remove_junk(esc_str($conn,$db_det['item_unit']));
                 $unit_cost = remove_junk(esc_str($conn,$db_det['item_unit_cost']));
                 $qty = remove_junk(esc_str($conn,$db_det['qty']));

                 $query .= " VALUES (";
                 $query .= " '{$batch_num}', '{$program}', '{$property_num}', '{$sloc}', '{$name}', '{$expiry}', '{$unit}', '{$unit_cost}', '300', '{$qty}'";
                 $query .= "),";
             }

             $query = trim($query,",");

             if($conn->query($query) === TRUE){
                $_SESSION['insert_res'] = "<b>Locked:</b> {$batch_num} - {$name}";
				$_SESSION['insert_res_type'] = "success";
				header("Location:view_available_items.php");
             }else{
                $_SESSION['insert_res'] = "<b>Error:</b> $conn->error </b>";
				$_SESSION['insert_res_type'] = "danger";
				header("Location:view_available_items.php");
             }
        }
        
    }//isset end


?>
