<?php 
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php');
	include('inc/agl_ct.php');
	include('inc/agl_fn.php');


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}


	header("refresh: 600"); 
 ?>


 <?php
 	include("layout/head.php");
 	include("layout/main_nav.php"); 
 	include("layout/sidebar.php");
  ?>
	<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Item</a></li>
	        <li class="breadcrumb-item active">Add Item</li>
	      </ul>
	    </div>
	  </div>
	  

		<?php 

			$all_unit = get_all_unit($conn,"unit");

			$all_sloc = get_all_sloc($conn,"tb_store_loc");

			//print_r_html($_SESSION);

			if (isset($_SESSION['res_type'])) {
				echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-{$_SESSION['res_type']}\">";
						echo "{$_SESSION['response']}";
					echo "</div>";
				echo "</div>";	

				unset($_SESSION['res_type']);
				unset($_SESSION['response']);
				
			}


		 ?>
	  <div class="container-fluid mt-3">
		<form action="add_item_proc.php" method="post">
			<div class="card">
				<div class="card-header align-items-center">
					<h4>Item Details</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="form-group col-lg-3">
							<input list="sloc_list" name="sloc"  class="form-control" placeholder="Enter Storage Location">
							<datalist id="sloc_list">
								<?php foreach($all_sloc as $sloc_id => $sloc_name){ ?>
									<option value="<?php echo $sloc_name; ?>"><?php echo $sloc_name; ?></option>
								<?php }?>
							</datalist>
						</div>
						<div class="form-group col-lg-3">
							<input type="text" name="batch_num" placeholder="Enter Batch No." class="form-control" id="batch_no">
						</div>
						<div class="form-group col-lg-3">
							<input type="text" name="item_name" placeholder="Item Name" class="form-control">
						</div>
						<div class="form-group col-lg-3">
							<input type="text" name="item_program" placeholder="Enter Program" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="form-group col-lg-3">
							<input type="date" name="expiry_date" placeholder="Expiration Date" class="form-control" id="" title="Expiration Date" data-toggle="tooltip" data-placement="top">
						</div>
						<div class="form-group col-lg-3">
							 <select name="item_unit" id="" class="form-control">
							 	<option value="">Select Unit</option>
								<?php foreach($all_unit as $all_unit_key => $all_unit_val){ ?>
								<option value="<?php echo $all_unit_val; ?>"><?php echo $all_unit_val; ?></option>
								<?php } ?>
							 </select>
						</div>
						<div class="form-group col-lg-3">
							 <input type="number" name="unit_cost" placeholder="Enter Unit Cost" class="form-control" step="0.01">
						</div>
						<div class="form-group col-lg-3">
							 <select name="item_status" id="" class="form-control">
							 	<option value="">Status</option>
							 	<option value="301">Active</option>
							 	<option value="300">Inactive</option>
							 </select>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<button type="submit" name="add_item_btn" class="form-control btn btn-primary">Add Item</button>
						</div>
					</div>

				</div>
			</div>              	
		</form>
	 </div>
	



  <?php
 	include("layout/footer.php"); 
  ?>
	