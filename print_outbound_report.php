<?php 

	ob_start();
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions
	include('fpdf/fpdf.php'); // fpdf
	include('fpdf/easytable/exfpdf.php'); // exfpdf
	include('fpdf/easytable/easyTable.php'); // easytable


	if(isset($_GET['start_date']) && isset($_GET['end_date'])){ // START ISSET
		
		$start_date = $_GET['start_date']; 
		$end_date = $_GET['end_date'];


		
				
				/**
				 * OUTBOUND TRANSACTION BETWEEN START AND END DATE
				 * ===========================================================================================
				 * We will display the Qty of the Outbound Transaction
				 */
				$bet_outbound = get_bet_outbound($conn,"tb_outbound",$start_date,$end_date);
				
				if(!empty($bet_outbound)){
					foreach($bet_outbound as $out_key => $out_det){
						if(isset($stock_summary[$out_det['out_property_num']])){
							$stock_summary[$out_det['out_property_num']]['out'] = $stock_summary[$out_det['out_property_num']]['out'] + $out_det['out_qty'];
						}
					}
				}

				

				/**
				 * PRINTING START
				 */
				
				$pdf=new exFPDF('P','mm','A4');
				$pdf->AddPage(); 
				$pdf->SetFont('arial','',10);

				$tb_header=new easyTable($pdf, 4);
				$tb_header->easyCell('', 'img:img/doh-logo.png, w25; align:L;');
				$tb_header->easyCell("<s 'font-size:8'>Republic of the Philippines\n<b>Department of Health</b></s>\n <b>REGIONAL OFFICE XI</b>\n <s 'font-size:7'>DOH Compound, J.P. Laurel Ave., Bajada, Davao City\nTrunklines: +63 (82) 305-1903, 305-1904, 305-1906, 227-407, 227-2463\nFax: 221-6320\n<b>email: doh11davao@gmail.com website: www.ro11.doh.gov</b></s>",'align:C; font-size:9; colspan:2');
				$tb_header->easyCell('', 'img:img/invoice-logo.png, w50; align:R;');
				$tb_header->printRow();
				$tb_header->endTable(5);

				$tb_info = new easyTable($pdf,'{140,50}');

				$tb_info->easyCell("OUTBOUND SUMMARY REPORT",'colspan:2; align:C; font-size:11; font-style:B; font-color:#ffffff; bgcolor:#1654c9');
				$tb_info->printRow();

				$tb_info->easyCell("Company: Arrowgo Logistics",'align:L; font-size:8; font-style:B; border:LR; border-color:#afb5bf');
				$tb_info->easyCell("Start Date: ".$start_date,'align:L; font-size:8; font-style:B;border:LRB; border-color:#afb5bf');
				$tb_info->printRow();

				$tb_info->easyCell("Warehouse: Warehouse A and B, New Bypass Road, Mamay Road Buhangin, Davao City",'align:L; font-size:8; rowspan:2; font-style:B;border:LB; border-color:#afb5bf');
				$tb_info->easyCell("End Date: ".$end_date,'align:L; font-size:8; font-style:B;border:LRB; border-color:#afb5bf');
				$tb_info->printRow();

				$tb_info->easyCell("Date: ".date('M-d-Y'),'align:L; font-size:8; font-style:B;border:LRB; border-color:#afb5bf');
				$tb_info->printRow();
				$tb_info->endTable(5);


				/**
				 * Stock Inbound
				 */
					$tb_table2 = new easyTable($pdf,'{20,30,20,20,30,20,20,10,20}','paddingY:1;valign:M;split-row:true');
					$tb_table2 -> easyCell('OUTBOUND TRANSACTIONS','font-size:9; font-style:B;font-color:#ffffff;bgcolor:#1654c9; align:L; colspan:9');
					$tb_table2->printRow();
				if(!empty($bet_outbound)){
					$tb_table2->easyCell('PTR No.','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Consignee','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Date','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Property No.','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Name','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Batch No.','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Qty','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Unit','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Expiration','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->printRow();

					foreach($bet_outbound as $outbound_key => $outbound_det){
						$tb_table2->easyCell($outbound_det['out_ptr_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_consignee'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_date'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_property_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_item_name'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_batch_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_qty'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_unit'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($outbound_det['out_expiry'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell('','font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell('','font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->printRow();
					}
				}else{
					$tb_table2->easyCell('No Outbound Transactions','font-size:6; font-style:B; border:B;border-color:#afb5bf; align:C; colspan:9');
					$tb_table2->printRow();
				}
					


					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->easyCell('','font-size:6;align:C');
					$tb_table2->printRow();
					$tb_table2->endTable(5);


				$pdf->sign_field($_SESSION['name']); // Sign Area

				$pdf->Output(); //To Print and to indicate the filename

	}// END ISSET
 ?>