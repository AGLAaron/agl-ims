<?php 
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions
	
?>
<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Report</a></li>
	        <li class="breadcrumb-item active">Inbound Report</li>
	      </ul>
	    </div>
	  </div>
	<?php 

		if(isset($_POST['btn_report'])){

			if(empty(trim($_POST['start_date'])) || empty(trim($_POST['end_date']))){
				echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-danger\">";
						echo "<strong>Error!</strong> Please Fill all Fields";
					echo "</div>";
				echo "</div>";
			}else{

				/**
				 * Start and End Date
				 * @var [type]
				 */
				$start_date = $_POST['start_date'];
				$end_date = $_POST['end_date'];

				$inbound_trans = get_bet_inbound($conn,"tb_inbound",$start_date,$end_date);

			}
		}
		




	 ?>
	<div class="container-fluid">
	 	<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post">
	 		<div class="row">
		 		<div class="col-lg-12 col-sm-12">
		 			
		 			<div class="card">
		 				<div class="card-header align-items-center">
							<h4>Inbound Report Date Filter</h4>
						</div>
		 				<div class="card-body">
		 					<div class="form-group">
		 						<div class="row">
		 							<div class="col-lg-4">
		 								<input  placeholder="To" name="start_date" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="Start Date">
		 							</div>
		 							<div class="col-lg-4">
		 								<input  placeholder="From" name="end_date" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="End Date">
		 							</div>
		 							<div class="col-lg-4">
		 								<button type="submit" class="btn btn-primary" name="btn_report">Generate Summary Report</button>
		 							</div>
		 						</div>
		 					</div>
		 				</div>
		 			</div>
		 		</div>
		 	</div>
	 	</form>	 		
	 </div>

	 <?php if(!empty($inbound_trans)){ ?>
	 		<div class="container-fluid">
	 			<div class="card">
			 		<div class="card-header">
		 				<div class="row">
			 				<div class="col-lg-4 justify-content-center">
				 				<h4>Inbound Summary Report</h4>
				 			</div>
				 			<div class="col-lg-2">
				 				<span class="small"><strong>Start Period:</strong> <span class="text-primary"><?php echo $start_date; ?></span></span>
				 			</div>
				 			<div class="col-lg-2">
				 				<span class="small"><strong>End Period:</strong> <span class="text-primary"><?php echo $end_date; ?></span></span>
				 			</div>
				 			<div class="col-lg-4 text-right">
				 				<a target="_blank" href="<?php echo "print_inbound_report.php?start_date={$start_date}&end_date={$end_date}"; ?>" class="btn btn-primary">Print Report</a>
				 			</div>
			 			</div>	
			 		</div>
			 		<div class="card-body">
			 			<div class="table-responsive">
	 						<table class="table table-striped table-hover table-md" id="stock_in_tbl">
	 							<thead>
	 								<tr class="bg-primary">
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Ref. #</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Shipper</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Date</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Property No.</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Name</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Batch No.</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Qty</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Unit</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Expiration Date</th>
	 								</tr>
	 							</thead>
	 							<tbody>
	 								<?php foreach($inbound_trans as $inbound_key => $inbound_det){ ?>
										<tr>
											<th scope="row" class="small text-center font-weight-bold"><?php echo $inbound_det['in_ref_num']; ?></th>
				 							<td class="small text-center"><?php echo $inbound_det['in_shipper']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_date']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_property_num']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_item_name']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_batch_num']; ?></td>
									      	<td class="small text-center font-weight-bold"><?php echo $inbound_det['in_qty']; ?></td>
									      	<td class="small text-center "><?php echo $inbound_det['in_unit']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_expiry']; ?></td>
										</tr>
	 								<?php } ?>
	 							</tbody>
	 						</table>
	 					</div>
			 		</div>
			 	</div>
	 		</div>
	 <?php }else{?>
			<?php if(!empty($_POST['start_date']) && !empty($_POST['start_date']) && isset($_POST['btn_report']) && empty($inbound_trans)){?>
					<div class="container-fluid">
			 			<div class="card">
					 		<div class="card-header">
				 				<div class="row">
					 				<div class="col-lg-4 justify-content-center">
						 				<h4>Inbound Summary Report</h4>
						 			</div>
						 			<div class="col-lg-2">
						 				<span class="small"><strong>Start Period:</strong> <span class="text-primary"><?php echo $start_date; ?></span></span>
						 			</div>
						 			<div class="col-lg-2">
						 				<span class="small"><strong>End Period:</strong> <span class="text-primary"><?php echo $end_date; ?></span></span>
						 			</div>
						 			<div class="col-lg-4 text-right">
						 				<a target="_blank" href="<?php echo "print_inbound_report.php?start_date={$start_date}&end_date={$end_date}"; ?>" class="btn btn-primary">Print Report</a>
						 			</div>
					 			</div>	
					 		</div>
					 		<div class="card-body">
					 			<div class="table-responsive">
			 						<table class="table table-striped table-hover table-md" id="stock_in_tbl">
			 							<h4>No Inbound Transaction</h4>
			 						</table>
			 					</div>
					 		</div>
					 	</div>
			 		</div>
			<?php } ?>
	 <?php } ?>

	
<?php
	include("layout/footer.php"); 
?>
<script>
	$(document).ready(function() {
	    $('#stock_in_tbl').DataTable();
	} );
</script>