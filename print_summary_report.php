<?php 

	ob_start();
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions
	include('fpdf/fpdf.php'); // fpdf
	include('fpdf/easytable/exfpdf.php'); // exfpdf
	include('fpdf/easytable/easyTable.php'); // easytable


	if(isset($_GET['start_date']) && isset($_GET['end_date'])){ // START ISSET
		
		$start_date = $_GET['start_date']; 
		$end_date = $_GET['end_date'];


		$all_items = get_all_items_report($conn,"items");

		$stock_summary = array();

				/*
					First we need to put all Items that are inside the Warehouse
					This will serve as a reference.
					For Each Item in Warehouse Get All Inbound Transaction and Get All Outbound Transaction
					Between the Start Period and End Period Being Set by the User
				 */
				
				foreach($all_items as $item_property_num => $item_details){
					$stock_summary[$item_property_num]['property_num'] = $item_details['property_num'];
					$stock_summary[$item_property_num]['item_name'] = $item_details['item_name'];
					$stock_summary[$item_property_num]['item_batch_num'] = $item_details['item_batch_num'];
					$stock_summary[$item_property_num]['item_expiry'] = $item_details['item_expiry'];
					$stock_summary[$item_property_num]['item_unit'] = $item_details['item_unit'];
					$stock_summary[$item_property_num]['item_unit_cost'] = $item_details['item_unit_cost'];
					$stock_summary[$item_property_num]['opening_qty'] = 0;
					$stock_summary[$item_property_num]['in'] = 0;
					$stock_summary[$item_property_num]['out'] = 0;
					$stock_summary[$item_property_num]['ending_qty'] = 0;

				}

				/**
				 * OPENING BALANCE
				 * ============================================================
				 * Get All Inbound Transaction Before the "Start Date"
				 * This Will Be the Opening Balance
				 * Insert in the Stock Summary as 'opening balance'
				 * @var [type]
				 */
				
				$prev_inbound = get_prev_inbound($conn,"tb_inbound",$start_date);

				if(!empty($prev_inbound)){
					foreach($prev_inbound as $in_key => $in_det){
						if(isset($stock_summary[$in_det['in_property_num']])){
							$stock_summary[$in_det['in_property_num']]['opening_qty'] = $stock_summary[$in_det['in_property_num']]['opening_qty'] + $in_det['in_qty'];
						}
					}
				}

				/**
				 * INBOUND TRANSACTIONS BETWEEN START AND END DATE
				 * ===========================================================================================
				 * We Will Display the Qty of the Inbound Transaction
				 */
				
				$bet_inbound = get_bet_inbound($conn,"tb_inbound",$start_date,$end_date);

				if(!empty($bet_inbound)){
					foreach($bet_inbound as $in_key => $in_det){
						$property_num = $in_det['in_property_num'];
						if(isset($stock_summary[$property_num])){
							$stock_summary[$property_num]['in'] = $stock_summary[$property_num]['in'] + $in_det['in_qty'];
						}
					}
				}

				
				/**
				 * OUTBOUND TRANSACTION BETWEEN START AND END DATE
				 * ===========================================================================================
				 * We will display the Qty of the Outbound Transaction
				 */
				$bet_outbound = get_bet_outbound($conn,"tb_outbound",$start_date,$end_date);
				
				if(!empty($bet_outbound)){
					foreach($bet_outbound as $out_key => $out_det){
						if(isset($stock_summary[$out_det['out_property_num']])){
							$stock_summary[$out_det['out_property_num']]['out'] = $stock_summary[$out_det['out_property_num']]['out'] + $out_det['out_qty'];
						}
					}
				}

				/**
				 * ENDING BALANCE/QTY CALCULATION
				 * ===========================================================================================
				 */
				
				foreach($stock_summary as $stock_sum_id => $stock_det){
					if(isset($stock_summary[$stock_det['property_num']])){
						$stock_summary[$stock_sum_id]['ending_qty'] = $stock_det['opening_qty'] + $stock_det['in'] - $stock_det['out'];
					}	
				}

				/**
				 * PRINTING START
				 */
				
				$pdf=new exFPDF('P','mm','A4');
				$pdf->AddPage(); 
				$pdf->SetFont('arial','',10);

				$tb_header=new easyTable($pdf, 4);
				$tb_header->easyCell('', 'img:img/doh-logo.png, w25; align:L;');
				$tb_header->easyCell("<s 'font-size:8'>Republic of the Philippines\n<b>Department of Health</b></s>\n <b>REGIONAL OFFICE XI</b>\n <s 'font-size:7'>DOH Compound, J.P. Laurel Ave., Bajada, Davao City\nTrunklines: +63 (82) 305-1903, 305-1904, 305-1906, 227-407, 227-2463\nFax: 221-6320\n<b>email: doh11davao@gmail.com website: www.ro11.doh.gov</b></s>",'align:C; font-size:9; colspan:2');
				$tb_header->easyCell('', 'img:img/invoice-logo.png, w50; align:R;');
				$tb_header->printRow();
				$tb_header->endTable(5);

				$tb_info = new easyTable($pdf,'{140,50}');

				$tb_info->easyCell("INVENTORY SUMMARY REPORT",'colspan:2; align:C; font-size:11; font-style:B; font-color:#ffffff; bgcolor:#1654c9');
				$tb_info->printRow();

				$tb_info->easyCell("Company: Arrowgo Logistics",'align:L; font-size:8; font-style:B; border:LR; border-color:#afb5bf');
				$tb_info->easyCell("Start Date: ".$start_date,'align:L; font-size:8; font-style:B;border:LRB; border-color:#afb5bf');
				$tb_info->printRow();

				$tb_info->easyCell("Warehouse: Warehouse A and B, New Bypass Road, Mamay Road Buhangin, Davao City",'align:L; font-size:8; rowspan:2; font-style:B;border:LB; border-color:#afb5bf');
				$tb_info->easyCell("End Date: ".$end_date,'align:L; font-size:8; font-style:B;border:LRB; border-color:#afb5bf');
				$tb_info->printRow();

				$tb_info->easyCell("Date: ".date('M-d-Y'),'align:L; font-size:8; font-style:B;border:LRB; border-color:#afb5bf');
				$tb_info->printRow();
				$tb_info->endTable(5);

				/**
				 * Table Fields
				 * @var easyTable
				 *
				 */
				$tb_table1 = new easyTable($pdf,'{20,55,20,20,20,10,10,20,15}','paddingY:1;valign:M; split-row:true');
				$tb_table1 -> easyCell('Property No.','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('Name','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('Batch No.','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('Expiration Date','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('Opening Qty','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('In','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('Out','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('Ending Qty','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1 -> easyCell('Unit','font-size:6; font-style:B; align:C; border:TB');
				$tb_table1->printRow();


					

				if(!empty($stock_summary)){
					foreach($stock_summary as $stock_sum_key => $stock_sum_details){
						$tb_table1 -> easyCell($stock_sum_details['property_num'],'font-size:6; align:C; border:B;border-color:#afb5bf');
						$tb_table1 -> easyCell($stock_sum_details['item_name'],'font-size:6;align:C; border:B;border-color:#afb5bf');
						$tb_table1 -> easyCell($stock_sum_details['item_batch_num'],'font-size:6;align:C; border:B;border-color:#afb5bf');
						/**
						 * Calculation of How close is the Expiration Date
						 * @var [type]
						 */
							$exp_date = $stock_sum_details['item_expiry'];
			      			$date_now = date('Y-m-d');

			      			$exp = strtotime($exp_date);
			      			$dt = strtotime($date_now);

			      			if($dt < $exp){
		 						$diff = $exp - $dt;
		 						$days_left = abs(floor($diff/(60*60*24)));
		 						if($days_left<365){
		 							$tb_table1 -> easyCell($stock_sum_details['item_expiry'],'font-color:#f4890e; font-size:6;align:C; border:B;border-color:#afb5bf');
		 						}else{
		 							$tb_table1 -> easyCell($stock_sum_details['item_expiry'],'font-size:6;align:C; border:B;border-color:#afb5bf');
		 						}
							}else{
									$tb_table1 -> easyCell($stock_sum_details['item_expiry'],'font-color:#f72000; font-size:6;align:C; border:B;border-color:#afb5bf');
							}

						$tb_table1 -> easyCell($stock_sum_details['opening_qty'],'font-size:6;align:C; border:B;border-color:#afb5bf');
						$tb_table1 -> easyCell($stock_sum_details['in'],'font-size:6;align:C; border:B;border-color:#afb5bf');
						$tb_table1 -> easyCell($stock_sum_details['out'],'font-size:6;align:C; border:B;border-color:#afb5bf');
						$tb_table1 -> easyCell($stock_sum_details['ending_qty'],'font-color:#1654c9 ; font-size:6;align:C; border:B;border-color:#afb5bf');
						$tb_table1 -> easyCell($stock_sum_details['item_unit'],'font-size:6;align:C; border:B;border-color:#afb5bf');
						$tb_table1->printRow();
					}
				}else{
					$tb_table1 -> easyCell('*****No Stock to Show****','colspan:9;font-size:9; align:C; border:B;border-color:#afb5bf');
					
					$tb_table1->printRow();
				}
				$tb_table1->endTable(8);

				/**
				 * Stock Inbound
				 */
					$tb_table2 = new easyTable($pdf,'{20,30,20,20,30,20,20,10,20}','paddingY:1;valign:M;split-row:true');
					$tb_table2 -> easyCell('INBOUND TRANSACTIONS','font-size:9; font-style:B;font-color:#ffffff;bgcolor:#1654c9; align:L; colspan:9');
					$tb_table2->printRow();
				if(!empty($bet_inbound)){
					$tb_table2->easyCell('Ref. No.','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Shipper','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Date','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Property No.','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Name','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Batch No.','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Qty','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Unit','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->easyCell('Expiration','font-size:6; font-style:B; border:B; align:C');
					$tb_table2->printRow();

					foreach($bet_inbound as $inbound_key => $inbound_det){
						$tb_table2->easyCell($inbound_det['in_ref_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_shipper'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_date'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_property_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_item_name'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_batch_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_qty'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_unit'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell($inbound_det['in_expiry'],'font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell('','font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->easyCell('','font-size:6;border:B;border-color:#afb5bf; align:C');
						$tb_table2->printRow();
					}
				}else{
					$tb_table2->easyCell('No Inbound Transactions','font-size:6; font-style:B; border:B;border-color:#afb5bf; align:C; colspan:9');
					$tb_table2->printRow();
				}
					$tb_table2->endTable(5);

				/**
				 * Stock Outbound
				 */
				
					$table3 = new easyTable($pdf,'{25,30,15,20,30,20,20,10,20}','paddingY:1;valign:M;split-row:true');
					$table3 -> easyCell('OUTBOUND TRANSACTIONS','font-size:9; font-style:B;font-color:#ffffff;bgcolor:#1654c9; align:L; colspan:9');
					$table3->printRow();

					if(!empty($bet_outbound)){
						$table3->easyCell('PTR No.','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Consignee','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Date','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Property No.','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Name','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Batch No.','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Qty','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Unit','font-size:6; font-style:B; border:B; align:C');
						$table3->easyCell('Expiration','font-size:6; font-style:B; border:B; align:C');
						$table3->printRow();

						foreach($bet_outbound as $outbound_key => $outbound_det){
							$table3->easyCell($outbound_det['out_ptr_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_consignee'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_date'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_property_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_item_name'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_batch_num'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_qty'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_unit'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->easyCell($outbound_det['out_expiry'],'font-size:6;border:B;border-color:#afb5bf; align:C');
							$table3->printRow();
						}
					}else{
						$table3->easyCell('No Outbound Transactions','font-size:6; font-style:B; border:B;border-color:#afb5bf; align:C; colspan:9');
						$table3->printRow();
					}

					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->easyCell('','font-size:6;align:C');
					$table3->printRow();
					$table3->endTable(5);


				$pdf->sign_field($_SESSION['name']); // Sign Area

				$pdf->Output(); //To Print and to indicate the filename

	}// END ISSET
 ?>