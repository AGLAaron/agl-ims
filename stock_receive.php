<?php 
	ob_start();
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}
	 
?>



<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Misc</a></li>
	        <li class="breadcrumb-item active">Stock Receive</li>
	      </ul>
	    </div>
	  </div>
	
   <?php 
		/**
		 * All Unit
		 */
		$all_unit = get_all_unit($conn,"unit");

    ?>
		
		<?php
			
			//Field Error
			if(isset($_SESSION['fill_res_type'])){
				echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-{$_SESSION['fill_res_type']}\">";
						echo "{$_SESSION['fill_res']}";
					echo "</div>";
				echo "</div>";

				unset($_SESSION['fill_res']);
				unset($_SESSION['fill_res_type']);
			}

			//Insert Message
			if(isset($_SESSION['insert_res_type'])){
				echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-{$_SESSION['insert_res_type']}\">";
						echo "{$_SESSION['insert_res']}";
					echo "</div>";
				echo "</div>";

				unset($_SESSION['insert_res']);
				unset($_SESSION['insert_res_type']);

			}
		?>


	<div class="container-fluid">
	 	<form action="stock_receive_proc.php" method="post">
	 		<div class="row">
		 		<div class="col-lg-4 col-sm-12">
		 			
		 			<div class="card">
		 				<div class="card-header align-items-center">
							<h4>Barcode Information</h4>
						</div>
		 				<div class="card-body">
		 					<div class="form-group">
		 						<div class="row">
		 							<div class="col-lg-12">
				 						<input type="text" name="shipper" class="form-control" placeholder="Barcode">
			 						</div>	
		 						</div>
		 					</div>
		 				</div>
		 			</div>
		 		</div>

		 		<div class="col-lg-12">
		 			<div class="card">
		 				<div class="card-header align-items-center">
							<h4>Item Information</h4>
						</div>
						<div class="card-body ">
							<div class="form-group item_info_field">
								<div class="row mb-3">
									<div class="col-lg-3 col-md-3 col-sm-12">
										<input type="text" name="transac[]" placeholder="PTR No./Transaction No." class="form-control" disabled>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12">
										<input type="text" name="item_name[]" placeholder="Item Name" class="form-control" disabled >
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12">
										<input type="text" name="item_code[]" placeholder="Item Code" class="form-control" disabled >
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12">
										<input type="date" name="expiry[]" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Expiration Date" disabled>
									</div>
								</div>
								<div class="row mb-3">
									<div class="col-lg-3">
										<input type="text" name="desc[]" placeholder="Item Description" class="form-control" disabled >
									</div>
									<div class="col-lg-3">
										<select name="unit[]" id="" class="form-control" disabled>
												<option value="">Select Unit</option>
												<?php foreach($all_unit as $unit_key => $unit_name){ ?>
														<option value="<?php echo $unit_name; ?>"><?php echo $unit_name; ?></option>
												<?php } ?>
										</select>
									</div>	
									<!-- <div class="col-lg-3">
											<input type="number" name="unit_cost[]" placeholder="Enter Unit Cost" class="form-control" step=".01" disabled> 
									</div> -->
									<div class="col-lg-3">
											<input type="number" name="amount[]" placeholder="Enter Amount" class="form-control" step=".01" disabled>
									</div>
								</div>
								<div class="row mb-3">
										<div class="col-lg-3 col-md-3 col-sm-12">
											<input type="number" name="qty[]" placeholder="Enter Quantity" class="form-control" disabled>
										</div>
									</div>
							</div>
						</div>
		 			</div>
		 			<div class="row">
						<div class="col-lg-12 text-center mb-2">
							<button type="submit" name="add_stock_receive" class="btn btn-primary btn-lg">Okay</button>
						</div>							
					</div>
		 		</div>
		 	</div>
	 	</form>	 		
	 </div>

  <?php
 	include("layout/footer.php"); 
  ?>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 1000; //Input fields increment limitation
    var addButton = $('.add_item_button'); //Add button selector
    var wrapper = $('.item_info_field'); //Input field wrapper	

    var x = 1; //Initial field counter is 1
	var form_id_count = 0;
    //Once add button is clicked
    $(addButton).click(function(){

    	var form_id = "add-form-"+form_id_count;
			console.log(form_id);
			/*
			 var fieldHTML = '<div class="form-group" id="'+form_id+'"><div class="row mb-3"><div class="col-lg-3 col-md-3 col-sm-12"><input list="property_datalist" name="f_property_no[]" placeholder="Enter Property No." class="form-control"></div><div class="col-lg-3 col-md-3 col-sm-12"><input type="number" name="f_qty[]" placeholder="Enter Quantity" class="form-control"></div><div class="col-lg-1 col-md-1 col-sm-12 text-left"><a href="javascript:void(0);" class="remove_item_button btn btn-danger form-control" onclick=remove_item("'+form_id+'") title="Remove Item">-</a></div></div>';
			*/
			 var fieldHTML = '<div class="form-group" id="'+form_id+'"><hr><hr><div class="row mb-3"><div class="col-lg-3 col-md-3 col-sm-12">	<input type="text" name="item_name[]" placeholder="Enter Item Name" class="form-control"></div><div class="col-lg-3 col-md-3 col-sm-12"><input type="text" name="batch_num[]" placeholder="Enter Batch No." class="form-control"></div><div class="col-lg-3 col-md-3 col-sm-12"><input type="date" name="expiry[]" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Expiration Date"></div><div class="col-lg-3 col-md-3 col-sm-12"><select name="unit[]" id="" class="form-control"><option value="">Select Unit</option><?php foreach($all_unit as $unit_key => $unit_name){ ?><option value="<?php echo $unit_name; ?>"><?php echo $unit_name; ?></option><?php } ?></select></div></div><div class="row"><div class="col-lg-3"><input type="number" name="unit_cost[]" placeholder="Enter Unit Cost" class="form-control" step=".01"></div><div class="col-lg-3"><input type="number" name="amount[]" placeholder="Enter Amount" class="form-control" step=".01"></div><div class="col-lg-3"><input type="number" name="qty[]" placeholder="Enter Quantity" class="form-control"></div><div class="col-lg-3 col-md-1 col-sm-12 text-left"><a href="javascript:void(0);" class="remove_item_button btn btn-danger form-control" onclick=remove_item("'+form_id+'") title="Remove Item">-</a></div></div></div>';
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            form_id_count++;
            $(wrapper).append(fieldHTML); //Add field html  
        }
    });
});

 function remove_item($id){
	document.getElementById($id).remove(); //Remove field html
	
 }

</script>


