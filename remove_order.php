<?php 
	ob_start();
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	
	if (isset($_GET['ref_no'])) {


		$query = "DELETE FROM tb_order";
		$query .= " WHERE order_ref_num = '{$_GET['ref_no']}'";

		if ($conn->query($query) === TRUE) {
			$_SESSION['delete_response'] =  "<b>Success:</b> Order Deleted Successfully! Order: <b>{$_GET['ref_no']}</b> - <b>Removed</b>";
			$_SESSION['delete_res_type'] = "success";
			header("Location:view_order.php");
		}else{
			//Failed delete
			$_SESSION['delete_response'] = "<b>Error:</b> Failed to Deleted Order: <b>{$_GET['ref_no']}</b>";
			$_SESSION['delete_res_type'] = "danger";
			header("Location:view_order.php");
		}

	}

 ?>