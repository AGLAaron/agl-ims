<?php 
	ob_start();
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	/**
	 * 0 - No Status
	 * 1 - Allocated
	 * 2 - In Transit
	 * 3 - Return
	 */
	
	if (isset($_GET['ref_no'])) {


		$upd_query = "UPDATE  tb_order";
		$upd_query .= " SET order_status = '1'";
		$upd_query .= " WHERE order_ref_num  = '{$_GET['ref_no']}'";

		if ($conn->query($upd_query) === TRUE) {
			$_SESSION['update_response'] =  "<b>Success:</b> Order Status Updated Successfully! Order: <b>{$_GET['ref_no']}</b> - <b>Allocated</b>";
			$_SESSION['update_res_type'] = "success";
			header("Location:view_order.php");
		}else{
			//Failed Update
			$_SESSION['update_response'] = "<b>Error:</b> Failed to updated Order Status: <b>{$_GET['ref_no']}</b>";
			$_SESSION['update_res_type'] = "danger";
			header("Location:view_order.php");
		}
	

	}
	


?>