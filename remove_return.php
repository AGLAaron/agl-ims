<?php 
	ob_start();
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	
	if (isset($_GET['return_id'])) {


		$query = "DELETE FROM return_items";
		$query .= " WHERE return_id = '{$_GET['return_id']}'";

		if ($conn->query($query) === TRUE) {
			$_SESSION['delete_response'] =  "<b>Success:</b> Order Deleted Successfully! Order: <b>{$_GET['return_id']}</b> - <b>Removed</b>";
			$_SESSION['delete_res_type'] = "success";
			header("Location:view_return.php");
		}else{
			//Failed delete
			$_SESSION['delete_response'] = "<b>Error:</b> Failed to Deleted Return: <b>{$_GET['return_id']}</b>";
			$_SESSION['delete_res_type'] = "danger";
			header("Location:view_return.php");
		}

	}

 ?>