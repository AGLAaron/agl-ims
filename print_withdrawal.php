<?php 
	ob_start();
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions
	include('fpdf/fpdf.php'); // fpdf
	include('fpdf/easytable/exfpdf.php'); // exfpdf
	include('fpdf/easytable/easyTable.php'); // easytable
	

	if (isset($_GET['ref_no'])) {
			

		$query = "SELECT * FROM tb_outbound WHERE out_ref_num = '{$_GET['ref_no']}'";
		
		$result = mysqli_query($conn,$query);
		$num_rows = mysqli_num_rows($result); //Number of rows
		
		$asar_outbound = array();
		while($db_row = mysqli_fetch_assoc($result)){
			
			$asar_outbound[]= $db_row;

		}
		
		//print_r_html($asar_outbound);
		$total = 0;
		foreach($asar_outbound as $asar_outbound_key =>$asar_arr_val){
				$order_ref_num  = $asar_arr_val['order_ref_num'];
				$outbound_date = $asar_arr_val['out_date'];
				$out_created_by = $asar_arr_val['out_created_by'];
				$shipper = $asar_arr_val['out_shipper'];
				$consignee = $asar_arr_val['out_consignee'];
				$trans_type = $asar_arr_val['out_trans_type'];
				$ptr_no = $asar_arr_val['out_ptr_num'];
				$outbound_created = $asar_arr_val['out_created'];
				$remarks = $asar_arr_val['out_remarks'];

				$total += floatval($asar_arr_val['out_amount']);	
				
		}

		$total = number_format($total,2,".",",");
		
		$pdf=new exFPDF('P','mm','A4');
		$pdf->AddPage(); 
		$pdf->SetFont('arial','',10);
		

		$tb_header=new easyTable($pdf, 2);
		$tb_header->easyCell('', 'img:img/invoice-logo.png, w50; align:L;');
		$tb_header->easyCell('WAREHOUSE DELIVERY ORDER', 'font-size:15; font-style:B; font-color:#1654c9; align:R');
		$tb_header->printRow();
		$tb_header->endTable(2);

		 $table=new easyTable($pdf, '{110,40,40}');

 		 $table->rowStyle('font-size:9');
		 $table->easyCell('Warehouse A and B, New Bypass Road'."\n".' Mamay Road,Buhangin'."\n".'Davao City,Philippines', 'rowspan:4; valign:T'); 
		 $table->easyCell('<b>Date Outbound-Created:</b>', 'font-size:8');
		 $table->easyCell($outbound_date,'border:1; border-color:#afb5bf; align:R; font-size:7');
		 $table->printRow();
		 
		 $table->rowStyle('font-size:8'); 
		 $table->easyCell('<b>Outbound #:</b>', '');
		 $table->easyCell($_GET['ref_no'],'border:1; border-color:#afb5bf; align:R;font-size:7');
		 $table->printRow();

		 $table->rowStyle('font-size:8'); 
		 $table->easyCell('<b>Outbound-Created By:</b>', '');
		 $table->easyCell($out_created_by,'border:1; border-color:#afb5bf; align:R;font-size:7');
		 $table->printRow();

		 $table->rowStyle('font-size:8'); 
		 $table->easyCell('<b>Order #:</b>', '');
		 $table->easyCell($order_ref_num,'border:1; border-color:#afb5bf; align:R;font-size:7');
		 $table->printRow();
		 $table->endTable(5);

		 $tableB = new easyTable($pdf,'{130,60}');

		 $tableB->rowStyle('font-size:11'); 
		  $tableB->easyCell('OUTBOUND DETAILS','bgcolor:#1654c9; border-color:#1654c9;border:1; align:L; font-color:#ffffff; font-style:B; colspan:2');
		 $tableB->printRow();

		  $tableB->rowStyle('font-size:9; border:LR; border-color:#afb5bf'); 
		  $tableB->easyCell('<b>Shipper:</b> '.$shipper);
		  $tableB->easyCell('<b>PTR #:</b> '.$ptr_no);
		  $tableB->printRow();
		  
		  $tableB->rowStyle('font-size:9; border:LBR; border-color:#afb5bf');
		  $tableB->easyCell('<b>Consignee:</b> '.$consignee);
		  $tableB->easyCell('<b>Pull-Out Date:</b> '.$outbound_date);
		  $tableB->printRow();

		  $tableB->rowStyle('font-size:9; border:LBR; border-color:#1654c9; bgcolor:#1654c9');
		  $tableB->easyCell('','colspan:2');
		  $tableB->printRow();

		  $tableB->rowStyle('font-size:9; border:LR; border-color:#afb5bf');
		  $tableB->easyCell('<b>Transfer Type:</b> '.$trans_type,'colspan:2');
		  $tableB->printRow();

		  $tableB->rowStyle('font-size:9; border:LBR; border-color:#afb5bf');
		  $tableB->easyCell('<b>Remarks/Reason For Transfer:</b> '.$remarks,'colspan:2');
		  $tableB->printRow();
		  $tableB->endTable(6);

		  $tableC = new easyTable($pdf,'{20,60,20,20,10,10,25,25}');

		  $tableC->rowStyle('border:1; border-color:#afb5bf; bgcolor:#1654c9');
		  $tableC->easyCell('Property No.','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->easyCell('Item Name','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->easyCell('Batch No.','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->easyCell('Expiry Date','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->easyCell('Qty','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->easyCell('Unit','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->easyCell('Unit Cost','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->easyCell('Amount','align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:8');
		  $tableC->printRow();

		  $maxRow = 10;
		  $initRow=0;
		  $i=0;
		  if($num_rows > $maxRow){
		  	
		  	 foreach($asar_outbound as $asar_outbound_key =>$asar_arr_val){
			 	$bgcolor='';
				    if($i%2){
				       $bgcolor='bgcolor:#afb5bf;';
				    }

					 $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
				    $tableC->easyCell($asar_arr_val['out_property_num']);
				    $tableC->easyCell($asar_arr_val['out_item_name']);
				    $tableC->easyCell($asar_arr_val['out_batch_num']);
				    $tableC->easyCell($asar_arr_val['out_expiry']);
				    $tableC->easyCell($asar_arr_val['out_qty']);
				    $tableC->easyCell($asar_arr_val['out_unit']);
				    $tableC->easyCell($asar_arr_val['out_unit_cost']);
				    $tableC->easyCell($asar_arr_val['out_amount']);
				    $tableC->printRow();
				    $i++;
				    $maxRow--;


			 }

		  }else{
		  	 

			 foreach($asar_outbound as $asar_outbound_key =>$asar_arr_val){
			 	$bgcolor='';
				    if($i%2){
				       $bgcolor='bgcolor:#afb5bf;';
				    }

					 $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
				    $tableC->easyCell($asar_arr_val['out_property_num']);
				    $tableC->easyCell($asar_arr_val['out_item_name']);
				    $tableC->easyCell($asar_arr_val['out_batch_num']);
				    $tableC->easyCell($asar_arr_val['out_expiry']);
				    $tableC->easyCell($asar_arr_val['out_qty']);
				    $tableC->easyCell($asar_arr_val['out_unit']);
				    $tableC->easyCell($asar_arr_val['out_unit_cost']);
				    $tableC->easyCell($asar_arr_val['out_amount']);
				    $tableC->printRow();
				    $i++;
				    $maxRow--;

			 }

			 while($maxRow!=0){
			 		if($i%2){
				       $bgcolor='bgcolor:#afb5bf;';
				    }
			 		 $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('');
				    $tableC->easyCell('-');
				    $tableC->printRow();
				    $i++;
				    $maxRow--;
			 }
		  }
		 
		  $tableC->endTable(0);

		  $tableD = new easyTable($pdf,'{20,60,20,20,10,10,25,5,20}','split-row:true');

		  $tableD->rowStyle('font-size:7; split-row:false');
		  $tableD->easyCell('','colspan:8; paddingY:2');
		  $tableD->printRow();

		  $tableD->rowStyle('font-size:7');
		  $tableD->easyCell('SPECIAL NOTE(S) AND ACTION','font-style:B;colspan:5; bgcolor:#1654c9; border:1; border-color:#1654c9; font-color:#ffffff;');
		  $tableD->easyCell('');
		  $tableD->easyCell('<s "font-size:8">Sub-Total</s>','align:L;');
		  $tableD->easyCell('<b><s "align:L">P</s></b>','font-size:8; border:TLB; border-color:#afb5bf');
		  $tableD->easyCell($total,'align:R; font-size:8;border: TRB;  border-color:#afb5bf');
		  $tableD->printRow();

		 
		  $tableD->easyCell('','font-style:B;colspan:5; border:LRB; border-color:#afb5bf; rowspan:5');
		  $tableD->easyCell('');
		  $tableD->easyCell('<s "font-size:8">Total</s>','align:L;');
		  $tableD->easyCell('<b><s "align:L">P</s></b>','font-size:8; border:TLB; border-color:#afb5bf');
		  $tableD->easyCell($total,'align:R; font-size:8;border: TRB;  border-color:#afb5bf');
		  $tableD->printRow();

		  $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();

		  $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();
		  
		  $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();

		  $tableD->rowStyle('paddingY:3');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->easyCell('');
		  $tableD->printRow();
		  $tableD->endTable(5);

		  $table3 = new easyTable($pdf,'{25,30,15,20,30,20,20,10,20}','paddingY:1;valign:M;split-row:true');

		  $table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->easyCell('','font-size:6;align:C');
			$table3->printRow();
			$table3->endTable(5);

		  $pdf->sign_field_outbound($_SESSION['name']);

		  // $tableE = new easyTable($pdf,'{30,40,40,40,40}');

		  // $tableE->easyCell('','colspan:5; bgcolor:#1654c9');
		  // $tableE->printRow();
		  
		  // $tableE->rowStyle('align:C{CCCCC}; font-size:8; font-style:B');
		  // $tableE->easyCell('');
		  // $tableE->easyCell('Approved By:');
		  // $tableE->easyCell('Issued By:');
		  // $tableE->easyCell('Prepared By:');
		  // $tableE->easyCell('Received By:');
		  // $tableE->printRow();

		  // $tableE->rowStyle('align:C{LCCCC}; font-size:7');
		  // $tableE->easyCell('Signature:');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->printRow();

		  //  $tableE->rowStyle('align:C{LCCCC}; font-size:7');
		  // $tableE->easyCell('Name:');
		  // //$tableE->easyCell('','border:B;border-color:#afb5bf');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;font-style:B;paddingY:2');
		  // $tableE->easyCell($out_created_by,'font-color:#1654c9;font-style:B;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->printRow();

		  // $tableE->rowStyle('align:C{LCCCC}; font-size:7');
		  // $tableE->easyCell('Designation:');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->printRow();

		  // $tableE->rowStyle('align:C{LCCCC}; font-size:7');
		  // $tableE->easyCell('Date:');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->easyCell('___________________','font-color:#afb5bf;paddingY:2');
		  // $tableE->printRow();
		  

		  // $tableE->easyCell('','colspan:5; bgcolor:#1654c9');
		  // $tableE->printRow();

		  //$tableE->endTable();

 	  	$pdf->Output(); //To Print and to indicate the filename
  }
?>