<?php
  session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	// if(isset($_SESSION['last_activity'])){

	// 	$last_activity = $_SESSION['last_activity'];
	// 	$timeout = 1800; // 30 mins

	// 	$time_now = time();

	// 	$duration = $time_now-$last_activity;
		
	// 	if($duration > $timeout){
	// 		session_start();

	// 		session_destroy();

	// 		header("location:login.php");
	// 	}
	// }

	// header("refresh: 600"); 
?>


<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>


<!-- Breadcrumb-->
	<div class="breadcrumb-holder mb-2">
		<div class="container-fluid">
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Order</a></li>
				<li class="breadcrumb-item active">Edit Order</li>
			</ul>
		</div>
	</div>

	<?php
			/**
		 * Get All Items
		 * It will be used in the Form That the User Will Select
		 */
		 $all_items = get_available_items($conn,"items");

		 /**
			* Get All Batch Numbers
			*/
		 $all_batch_no = get_all_batch_no($conn,"items");
 
			 /**
			* Get all unit so it will be displayed in the form
			* @var [type]
			*/
		 
		 $all_unit = get_all_unit($conn,"unit");
		 /**
			* All Consignee/Receiver
			* @var [type]
			*/
		 $all_consignee = get_all_out_cons($conn,"outbound_destination");
	?>

	<?php

		if(isset($_GET['ref_no'])){ // Isset start
			/**
			 * Get all from tb_order where order_ref_num = $_GET['ref_no']
			 */

			$query = "SELECT * FROM tb_order";
			$query .= " WHERE order_ref_num = '{$_GET['ref_no']}'";

			$result = mysqli_query($conn,$query);

			$order_array_details = array();
			$order_item_details = array();

			while($db_rows = mysqli_fetch_assoc($result)){
				$order_array_details ['order_ref_num']= $db_rows['order_ref_num'];
				$order_array_details ['order_type']= $db_rows['order_type'];
				$order_array_details ['order_shipper']= $db_rows['order_shipper'];
				$order_array_details ['order_consignee']= $db_rows['order_consignee'];
				$order_array_details ['order_ptr_num']= $db_rows['order_ptr_num'];
				$order_array_details ['order_release_date']= $db_rows['order_release_date'];
				$order_array_details ['order_transfer_type']= $db_rows['order_transfer_type'];
				$order_array_details ['order_remarks']= $db_rows['order_remarks'];
				$order_array_details ['order_date']= $db_rows['order_date'];


				$order_item_details[$db_rows['order_ref_num']][$db_rows['id']]['property_num'] = $db_rows['order_property_num'];
				$order_item_details[$db_rows['order_ref_num']][$db_rows['id']]['item_name'] = $db_rows['order_item_name'];
				$order_item_details[$db_rows['order_ref_num']][$db_rows['id']]['qty'] = $db_rows['order_qty'];
				['order_unit_cost'];

			}

			$form_id_count = 0;
		}//isset End
	?>

		<div class="container-fluid">
			<form action="edit_order_proc.php" method="get">
				<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="card">
							<div class="card-header align-items-center">
								<h4>Order Information</h4>
							</div>
							<div class="card-body">
								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<input type="text" name="f_order_ref" placeholder="Reference No." class="form-control" data-toggle="tooltip" data-placement="top" title="Order Reference No." value="<?php echo "{$order_array_details['order_ref_num']}";?>">
										</div>
									</div>
									<div class="row mt-3">
										<div class="col-lg-6">
											<select name="f_order_type" id="" class="form-control">
												<?php if(are_strings_equal(trim($order_array_details['order_type']),"p")){ ?>
													<option value="<?php echo "{$order_array_details['order_type']}";?>"> Pick-Up </option>
													<option value="d">Delivery</option>
												<?php }else{?>
													<option value="<?php echo "{$order_array_details['order_type']}";?>"> Delivery </option>
													<option value="p">Pick-Up</option>
												<?php }?>		
											</select>
										</div>
										<div class="col-lg-6">
											<input  placeholder="Enter/Select Date" name="f_pick_date" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="Select Date" value="<?php echo"{$order_array_details['order_date']}";?>">
										</div>
									</div>
									<div class="row mt-3">
										<div class="col-lg-6">
											<select name="f_shipper" id="" class="form-control">
												<option value="<?php echo "{$order_array_details['order_shipper']}";?>"><?php echo "{$order_array_details['order_shipper']}";?></option>
												<option value="">Select Shipper</option>
											</select>
										</div>
										<div class="col-lg-6">
											<input list="dest_list" name="f_dest" placeholder="Select/Enter Consignee" class="form-control" value="<?php echo "{$order_array_details['order_consignee']}";?>">
											<datalist id="dest_list">
												<?php foreach($all_consignee as $consignee_key => $consignee_val){ ?>
													<option value="<?php echo $consignee_key.",".$consignee_val; ; ?>"><?php echo "{$consignee_val}"; ?></option>
												<?php } ?>
											</datalist>
										</div>
									</div>
									<div class="row mt-3">
										<div class="col-lg-6">
											<input type="text" name="f_ptr" placeholder="Enter PTR No." class="form-control" value="<?php echo "{$order_array_details['order_ptr_num']}";?>">
										</div>
										<div class="col-lg-6">
											<input list="trans_type_list" name="f_trans_type" placeholder="Select/Enter Transfer Type" class="form-control" value="<?php echo "{$order_array_details['order_transfer_type']}";?>">
											<datalist id="trans_type_list">
												<option value="Donation"></option>
												<option value="Reassignment"></option>
											</datalist>
										</div>
									</div>
									<div class="row mt-3">
										<div class="col">
											<input type="text" placeholder="Remarks" name="f_remarks" class="form-control" value="<?php echo "{$order_array_details['order_remarks']}";?>">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-12">
						<div class="card">
							<div class="card-header align-items-center">
								<div class="row">
									<div class="col-lg-6">
										<h4>Order Item Information</h4>
									</div>
								</div>
							</div>
							<div class="card-body">
							<div class="form-group item_info_field">
									<?php foreach($order_item_details as $order_ref_num => $order_arr_items){?>
										<?php foreach($order_arr_items as $order_id => $order_det){?>
												<div class="row mb-3"  id="<?php echo "{$form_id_count}"; ?>">
													<div class="col-lg-3 col-md-3 col-sm-12">
																<input list="property_datalist" name="f_property_no[]" placeholder="Enter Property No." class="form-control" value="<?php echo"{$order_det['property_num']}"; ?>">
																<datalist id="property_datalist">
																<?php foreach($all_items as $item_key => $arr_val){ ?>
																		<option value="<?php echo $item_key; ?>"><span class="text-justify"><?php echo $arr_val['item_batch_num']."-".$arr_val['item_name'];?></span class="text-justify"></option>
																<?php } ?>
														</datalist>
													</div>
													<div class="col-lg-3 col-md-3 col-sm-12">
														<input type="number" name="f_qty[]" placeholder="Enter Quantity" class="form-control" value="<?php echo"{$order_det['qty']}"; ?>">
													</div>
													<div class="col-lg-1 col-md-1 col-sm-12 text-left">
														<a href="javascript:void(0);" class="remove_item_button btn btn-danger form-control" onclick=remove_item("<?php echo"{$form_id_count}"; ?>") title="Remove Item">-</a>
													</div>
													<div class="col-lg-1 col-md-1 col-sm-12 text-left">
														<a href="javascript:void(0);" class="add_item_button btn btn-primary form-control" title="Add Item">+</a>
													</div>	
												</div>	
										<?php $form_id_count++;} ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 justify-content-center pb-3">
								<div>
									<button type="submit" name="update_order" class="form-control btn btn-primary" >Update Order</button>
								</div>
							</div>		
							<div class="col-lg-12 justify-content-center pb-3">
								<div>
									<a href="view_order.php" class="form-control btn btn-secondary">Cancel</a>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</form>	 		
		</div>



<?php
	include("layout/footer.php"); 
?>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 1000; //Input fields increment limitation
		var addButton = $('.add_item_button'); //Add button selector
		var removeButton = $('.remove_item_button'); // Remove Button Selector
    var wrapper = $('.item_info_field'); //Input field wrapper	

    var x = 1; //Initial field counter is 1
	  var form_id_count = 0;
    //Once add button is clicked
    $(addButton).click(function(){

    	var form_id = "add-form-"+form_id_count;
    	console.log(form_id);
    	 var fieldHTML = '<div class="form-group" id="'+form_id+'"><div class="row mb-3"><div class="col-lg-3 col-md-3 col-sm-12"><input list="property_datalist" name="f_property_no[]" placeholder="Enter Property No." class="form-control"></div><div class="col-lg-3 col-md-3 col-sm-12"><input type="number" name="f_qty[]" placeholder="Enter Quantity" class="form-control"></div><div class="col-lg-1 col-md-1 col-sm-12 text-left"><a href="javascript:void(0);" class="remove_item_button btn btn-danger form-control" onclick=remove_item("'+form_id+'") title="Remove Item">-</a></div></div>';
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            form_id_count++;
            $(wrapper).append(fieldHTML); //Add field html  
        }
		});
		

	
});

 function remove_item($id){
	document.getElementById($id).remove(); //Remove field html
	
 }

</script>