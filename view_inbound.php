<?php 
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	header("refresh: 600"); 
	
 ?>

 <?php
 	include("layout/head.php");
 	include("layout/main_nav.php"); 
 	include("layout/sidebar.php");
  ?>
	<!-- Breadcrumb-->
	  <div class="breadcrumb-holder">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Inbound</a></li>
	        <li class="breadcrumb-item active">View Inbound</li>
	      </ul>
	    </div>
	  </div>

	<?php 
		$query = "SELECT * FROM tb_inbound ORDER BY id DESC";
		$result = mysqli_query($conn,$query);

		$asar_db_in = array();

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_db_in[$db_rows['in_ref_num']] = $db_rows;
		}
	 ?>
     
	<div class="container-fluid mt-3">
		<div class="card">
			<div class="card-header align-items-center">
				<h4>Inbound Transactions</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-md" id="view_inbound_tbl">
					  <thead>
					    <tr class="bg-primary">
					      <th class="small text-center font-weight-bold text-light">Inbound Ref. #</th>
					      <th class="small text-center font-weight-bold text-light">Arrival Date</th>
					      <th class="small text-center font-weight-bold text-light">Arrival Time</th>
					      <th class="small text-center font-weight-bold text-light">Item Name</th>
						  <th class="small text-center font-weight-bold text-light">Expiration Date</th>
					      <th class="small text-center pb-2 font-weight-bold text-light">Created By</th>
					      <th class="small text-center pb-2 font-weight-bold text-light">Date Created</th>
					      
					      <th class="small text-center pb-2 font-weight-bold text-light">Tools</th>
						  
					      
					    </tr>
					  </thead>
					  <tbody>
					  	<?php
					  	
						  	foreach ($asar_db_in as $in_key => $arr_in_val) {
					  		 ?>
						    <tr>
						      <th scope="row" class="small text-center font-weight-bold <?php echo $child; ?>"><?php echo $in_key; ?></th>
						      <td class="small text-center"><?php echo $arr_in_val['arrival_date']; ?></td>
						      <td class="small text-center"><?php echo $arr_in_val['arrival_time']; ?></td>
						      <td class="small text-center"><?php echo $arr_in_val['in_item_name']; ?></td>
							  <td class="small text-center"><?php echo $arr_in_val['in_expiry']; ?></td>
						      <td class="small text-center"><?php echo $arr_in_val['in_created_by']; ?></td>
						      <td class="small text-center"><?php echo $arr_in_val['in_created']; ?></td>
 							  
						      <?php 
						      if ($_SESSION['type'] == "admin") {?> 
						      	<td class="small text-center"><a target="_blank" href="<?php echo "print_inbound.php?ref_no={$in_key}"; ?>" class="btn btn-primary btn-sm">Inbound Receipt</a></td>
						      <?php } ?>
							  <!-- <td class="small text-center"><a target="_blank"   class="btn btn-primary btn-sm" data-toggle="modal" data-target="#barcodeModal">Generate Barcode</a></td> -->
						    </tr>
						<?php 
							}
							
						 ?> 
					  	<?php
					  	/*
					  		$current_ref = array( 0 =>""); 
					  		while ($db_rows = mysqli_fetch_assoc($result)) {
					  			$db_ref = $db_rows['in_ref_num'];
							 	 	$curr_num = $current_ref[0];
						 	 	

							 	 	if($curr_num == $db_ref){
							 	 		$child = "text-primary";
							 	 	}else{
							 	 		$child = "";
							 	 		$current_ref[0] = $db_ref;
							 	 	} 

					  		 ?>
						    <tr>
						      <th scope="row" class="small text-center font-weight-bold <?php echo $child; ?>"><?php echo $db_rows['in_ref_num']; ?></th>
						      <td class="small text-center"><?php echo $db_rows['in_date']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_shipper']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_consignee']; ?></td>
						      <td class="small text-center text-primary "><?php echo $db_rows['in_batch_num']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_item_name']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_expiry']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_qty']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_unit']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_unit_cost']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_amount']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_created_by']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['in_created']; ?></td>
						      <?php 
						      if ($_SESSION['type'] == "admin") {?> 
						      	<td class="small text-center"><a href="<?php echo "print_inbound.php?ref_no={$db_rows['in_ref_num']}"; ?>" class="btn btn-primary btn-sm">Inbound Receipt</a></td>
						      <?php } ?>
						    </tr>
						<?php 
							}
							*/
						 ?> 
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="barcodeModal" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="barcodeModalLabel">New barcode
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
            </button>
          </div>
          <div class="modal-body">
            <form action="generate_barcode.php" method="post">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label" required>Product name:
                </label>
                <input type="text" class="form-control" id="name" name="name" maxlength="20" required>
              </div>
              <hr>
              <button type="submit" class="btn btn-primary">Generate barcode
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>

  <?php
 	include("layout/footer.php"); 
  ?>
  <script>
  	$(document).ready(function() {
	    $('#view_inbound_tbl').DataTable({
	    	"ordering":false
	    });
	} );
  </script>