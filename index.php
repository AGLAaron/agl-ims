<?php 
	ob_start();
	session_start();
	date_default_timezone_set("Asia/Manila");
	include("inc/db/bd_connect.php");
	include('inc/agl_ct.php');
	include('inc/agl_fn.php');
  include('fetch_dashboard_details.php');
	
	if(!isset($_SESSION['type']))
	{
		header("location:login.php");
	}

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;

		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	/**
	 * Refresh This Page Every 10 Mins or 600 sec
	 * This is to check if there are new orders and check the movements of the goods
	 */
	header("refresh: 600"); 
 ?>


 <?php
 	include("layout/head.php");
 	include("layout/main_nav.php"); 
 	include("layout/sidebar.php");
  ?>
	<?php 
		//print_r_html($_SESSION);
		$query = "SELECT * FROM available_items";
		$result = mysqli_query($conn,$query);
		
	 ?>

	  <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
						<div class="row bg-white has-shadow">
								<!-- Available Items -->
								 <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-blue"><i class="fas fa-box"></i></div>
                    <div class="title"><a href="<?php echo "view_available_items.php"; ?>"><span>Available<br>Items</span></a>
                    </div>
                    <div class="number"><strong><?php echo $all_available_items; ?></strong></div>
                  </div>
								</div>  

                <!-- Locked Items -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-gray"><i class="fas fa-lock"></i></div>
                    	<div class="title"><a href="<?php echo "view_locked_items.php"; ?>"><span>Locked<br>Items</span></a>
                    </div>
                    <div class="number"><strong><?php echo $all_lock_items ; ?></strong></div>
                  </div>
                </div>
  			
								<!-- Returns -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="fas fa-undo"></i></div>
											<div class="title">
												<span><a href="<?php echo "view_return.php" ?>">Returned<br>Items</a></span> 
											</div>
												<div class="number"><strong><?php echo $all_return; ?></strong>
											</div>
                  </div>
                </div>
							</div>

              <div class="row bg-white has-shadow">
                <!-- Out of Stock -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="fas fa-pallet"></i></div>
                    <div class="title">
                      <a href="<?php echo "view_out_of_stock_items.php"; ?>"><span>Out of Stock<br>Items</span></a>
                    </div>
                    <div class="number"><strong><?php echo $out_stock_count; ?>/<?php echo mysqli_num_rows($result); ?></strong></div>
                  </div>
                </div>
                <!-- New Orders -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-orange"><i class="icon-padnote"></i></div>
                    <div class="title"><span>New<br>Orders</span>
                    </div>
                    <div class="number"><strong><?php echo $new_order; ?></strong></div>
                  </div>
                </div>
                <!-- New Inbound -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-green"><i class="icon-bill"></i></div>
                    <div class="title"><span>New<br>Inbound</span>
                    </div>
                    <div class="number"><strong><?php echo $new_in; ?></strong></div>
                  </div>
                </div>  
							</div>
									
						</div>
					</section>
			 <!-- Dashboard Counts Section-->

	<div class="container-fluid mt-3">
		<div class="card">
			<div class="card-header align-items-center">
				<h4 class="text-primary">Current Stock Inventory</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-hover table-striped table-md" id="curr_stock_tb">
					  <thead>
					    <tr>
						  <th class="small text-center font-weight-bold text-primary">Property No.</th>
						  <th class="small text-center font-weight-bold text-primary">Location</th>
					      <th class="small text-center font-weight-bold text-primary">Batch No.</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">Item Name</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">On-hand</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">Allocated</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">In-Transit</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">Returned</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">Available</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">Unit</th>
					      <th class="small text-center pb-2 font-weight-bold text-primary">Unit Cost</th>
			      		  <th class="small text-center pb-2 font-weight-bold text-primary">Expiration Date</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php while ($db_rows = mysqli_fetch_assoc($result)) { ?>
						    <tr>
							  <th scope="row" class="small text-center font-weight-bold"><?php echo $db_rows['property_num']; ?></th>
							  <th scope="row" class="small text-center text-orange"><?php echo $db_rows['s_loc']; ?></th>
						      <th scope="row" class="small text-center"><?php echo $db_rows['item_batch_num']; ?></th>
						      <td class="small text-center"><?php echo $db_rows['item_name']; ?></td>

						      <!--On Hand-->
						      <td class="small text-center font-weight-bold">

						      	<?php echo $db_rows['qty']; ?>
						      		
						      </td>
						      <!--On Hand-->

						      <!--Allocated-->
						      <td class="small text-center font-weight-bold" style="color: blue;">
						      	<?php 

						      		if(!empty($all_allocate)){
							      		if(isset($all_allocate[$db_rows['property_num']])){
							      			echo "{$all_allocate[$db_rows['property_num']]}";
							      		}else{
							      			echo "0.00";
							      		}
						      		}else{
						      			echo "0.00";
						      		}
						      	 ?>
						      </td>
						      <!--Allocated-->

							  <!--In-Transit -->
						      <td class="small text-center font-weight-bold" style="color: green;">
						      	<?php 

						      		if(!empty($all_transit)){
							      		if(isset($all_transit[$db_rows['property_num']])){
							      			echo "{$all_transit[$db_rows['property_num']]}";
							      		}else{
							      			echo "0.00";
							      		}
						      		}else{
						      			echo "0.00";
						      		}
						      	 ?>
						      </td>
						      <!--In-Transit -->

							  <!--Returned -->
						      <td class="small text-center font-weight-bold" style="color: orange;">
						      	<?php 

						      		if(!empty($all_item_return)){
							      		if(isset($all_item_return[$db_rows['property_num']])){
							      			echo "{$all_item_return[$db_rows['property_num']]}";
							      		}else{
							      			echo "0.00";
							      		}
						      		}else{
						      			echo "0.00";
						      		}
						      	 ?>
						      </td>
						      <!--Returned -->


						      <td class="small text-center font-weight-bold" style="color: red;">
						      	<?php 

						      		$running_bal = $db_rows['qty'];

						      		if(!empty($all_allocate)){
							      		if(isset($all_allocate[$db_rows['property_num']])){
							      			//echo "{$all_allocate[$db_rows['property_num']]}";
							      			$allocate = $all_allocate[$db_rows['property_num']];
							      		}else{
							      			$allocate = 0;
							      		}
						      		}else{
						      			$allocate = 0;
						      		}

						   

						      		$available_stocks  = $running_bal - $allocate ;

						      		echo $available_stocks;

						      	 ?>
						      </td><!-- Available-->
						      <td class="small text-center"><?php echo $db_rows['item_unit']; ?></td>
						      <td class="small text-center"><?php echo $db_rows['item_unit_cost']; ?></td>
						
						      	<?php 

						      			$exp_date =  $db_rows['item_expiry']; 
						      			$date_now = date('Y-m-d');

						      			$exp = strtotime($exp_date);
						      			$dt = strtotime($date_now);

						      			if($dt < $exp){
													$diff = $exp - $dt;
													$days_left = abs(floor($diff/(60*60*24)));
													if($days_left<365){
														echo "<td class=\"small text-center\"><span style=\"color:orange\">$exp_date</span></td>";
													}else{
														echo "<td class=\"small text-center\">$exp_date</td>";
													}
												}else{
														echo "<td class=\"small text-center\"><span style=\"color:red\">$exp_date</span></td>";
												}

						      	?>
						      		
						     
						    </tr>
						<?php 
							}
						 ?> 
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

  <?php
 	include("layout/footer.php"); 
  ?>
  <script>
  	$(document).ready(function() {
	    $('#curr_stock_tb').DataTable();
	} );
  </script>