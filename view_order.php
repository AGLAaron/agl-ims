<?php
session_start();
include('inc/db/bd_connect.php'); // Db Connection
include('inc/agl_ct.php'); // Constant
include('inc/agl_fn.php'); // Functions

if (isset($_SESSION['last_activity'])) {

	$last_activity = $_SESSION['last_activity'];
	$timeout = 1800; // 30 mins

	$time_now = time();

	$duration = $time_now - $last_activity;

	if ($duration > $timeout) {
		session_start();

		session_destroy();

		header("location:login.php");
	}
}

header("refresh: 600");
?>

<?php
include("layout/head.php");
include("layout/main_nav.php");
include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
<div class="breadcrumb-holder mb-2">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="index.php">Order</a></li>
			<li class="breadcrumb-item active">View Order</li>
		</ul>
	</div>
</div>
<?php
$query = "SELECT * FROM tb_order ORDER BY id DESC";
$result = mysqli_query($conn, $query);

$asar_db_order = array();
while ($db_rows = mysqli_fetch_assoc($result)) {
	$asar_db_order[$db_rows['order_ref_num']] = $db_rows;
}
?>

<?php

//Sumbit Prompt
if (isset($_SESSION['submit_response'])) {
	echo "<div class = \"container-fluid\">";
	echo "<div class=\"alert alert-{$_SESSION['submit_res_type']}\">";
	echo "{$_SESSION['submit_response']}";
	echo "</div>";
	echo "</div>";

	unset($_SESSION['submit_res_type']);
	unset($_SESSION['submit_response']);
}


//Success PROMPT
if (isset($_SESSION['success_res_type'])) {
	echo "<div class = \"container-fluid\">";
	echo "<div class=\"alert alert-{$_SESSION['success_res_type']}\">";
	echo "{$_SESSION['success_response']}";
	echo "</div>";
	echo "</div>";

	unset($_SESSION['success_res_type']);
	unset($_SESSION['success_response']);
}
//UPDATE PROMPT
if (isset($_SESSION['update_res_type'])) {
	echo "<div class = \"container-fluid\">";
	echo "<div class=\"alert alert-{$_SESSION['update_res_type']}\">";
	echo "{$_SESSION['update_response']}";
	echo "</div>";
	echo "</div>";

	unset($_SESSION['update_res_type']);
	unset($_SESSION['update_response']);
}

//DELETE PROMPT
if (isset($_SESSION['delete_res_type'])) {
	echo "<div class = \"container-fluid\">";
	echo "<div class=\"alert alert-{$_SESSION['delete_res_type']}\">";
	echo "{$_SESSION['delete_response']}";
	echo "</div>";
	echo "</div>";

	unset($_SESSION['delete_res_type']);
	unset($_SESSION['delete_response']);
}
?>

<div class="container-fluid mt-3">
	<div class="card">
		<div class="card-header align-items-center">
			<h4>Order Transactions</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-md" id="view_order_tbl">
					<thead>
						<tr class="bg-primary">
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Reference No.</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Order Type</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Order Date</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">PTR No.</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Transfer Type</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Pull-Out Date</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Shipper</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Consignee</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Created By</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Remarks</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light">Amount</th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light"></th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light"></th>
							<th class="small text-center px-3 py-2 font-weight-bold text-light"></th>

							<?php if($_SESSION['type'] == "admin"){?>
								<th class="small text-center px-3 py-2 font-weight-bold text-light"></th> <!-- Allocate -->

								<th class="small text-center px-3 py-2 font-weight-bold text-light"></th> <!-- Transit -->

								<th class="small text-center px-3 py-2 font-weight-bold text-light"></th> <!-- Pick List -->
							<?php } ?>

							

						</tr>
					</thead>
					<tbody>
						<?php foreach ($asar_db_order as $order_key => $arr_order_val) { ?> <!-- Foreach -->
							<tr>
								<th scope="row" class="small text-center font-weight-bold <?php echo $child; ?>">

									<?php echo $order_key; ?>

								</th>
								<td class="small text-center">
									<?php
									if (are_strings_equal($arr_order_val['order_type'], "d")) {
										echo "Delivery";
									} else {
										echo "Pick-Up";
									}
									?>
								</td>
								<td class="small text-center"><?php echo $arr_order_val['order_date']; ?></td>
								<td class="small text-center"><?php echo $arr_order_val['order_ptr_num']; ?></td>
								<td class="small text-center"><?php echo $arr_order_val['order_transfer_type']; ?></td>
								<td class="small text-center"><?php echo $arr_order_val['order_release_date']; ?></td>
								<td class="small text-center"><?php echo $arr_order_val['order_shipper']; ?></td>
								<td class="small text-center"><?php echo $arr_order_val['order_consignee']; ?></td>
								<td class="small text-center"><?php echo $arr_order_val['order_created_by']; ?></td>
								<td class="small text-center"><?php echo $arr_order_val['order_remarks']; ?></td>
								<td class="small text-center"><b><?php echo get_order_amount($conn, $order_key); ?></b></td>
								<!-- Edit Order -->
								<td class="small text-center"><a target="" href="<?php echo "edit_order.php?ref_no={$order_key}"; ?>" class="btn btn-primary btn-sm">Edit Order</a></td>
								<!-- Edit Order -->

								<!-- Print Order Receipt -->
								<td class="small text-center"><a target="_blank" href="<?php echo "print_order.php?ref_no={$order_key}"; ?>" class="btn btn-primary btn-sm">Print Order</a></td>
								<!-- Print Order Receipt -->

								<!-- Remove/Cancel Order -->
								<td class="small text-center">
									<a href="<?php echo "remove_order.php?ref_no={$order_key}"; ?>" class="btn btn-sm text-light" style="background-color: orange;">Cancel Order</a>
								</td>
								<!-- Remove/Cancel Order -->

								<?php if($_SESSION['type'] == "admin"){?>
									<!-- Print Pick-List -->
									<td class="small text-center">
										<a target="_blank	" href="<?php echo "print_pick_list.php?ref_no={$order_key}";?>" class="btn btn-primary btn-sm">Pick List</a>
									</td>
									<!-- Print Pick-List -->

									<!-- Allocate Order -->
									<?php if($arr_order_val['order_status'] == 1){ ?>
											<td class="small text-center">
												<a href="<?php echo "unallocate_order.php?ref_no={$order_key}";?>" class="btn btn-secondary btn-sm">Unallocate Order</a>
											</td>
									<!-- Allocate Order -->
									<?php } else{?>
											<td class="small text-center">
												<a href="<?php echo "allocate_order.php?ref_no={$order_key}";?>" class="btn btn-primary btn-sm">Allocate Order</a>
											</td>
									<?php } ?>

									<?php if($arr_order_val['order_status'] == 2){?>
											<!-- In Transit -->
												<td class="small text-center">
													<a href="<?php echo "unallocate_order.php?ref_no={$order_key}";?>" class="btn btn-secondary btn-sm">In-Transit</a>
												</td>
											<!-- In Transit -->
									<?php } else{?>
											<!-- In Transit -->
												<td class="small text-center">
													<a href="<?php echo "transit_order.php?ref_no={$order_key}";?>" class="btn btn-primary btn-sm">In-Transit</a>
												</td>
											<!-- In Transit -->
									<?php } ?>
									
								<?php } ?>
							</tr>
						<?php }	?> <!-- Foreach -->
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




<?php
include("layout/footer.php");
?>
<script>
	$(document).ready(function() {
		$('#view_order_tbl').DataTable({
			"order": [
				[2, "desc"]
			]
		});
	});
</script>