<?php 
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions

	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}

	header("refresh: 600"); 
	
 ?>

 <?php
 	include("layout/head.php");
 	include("layout/main_nav.php"); 
 	include("layout/sidebar.php");
  ?>
	<!-- Breadcrumb-->
	  <div class="breadcrumb-holder">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Inbound</a></li>
	        <li class="breadcrumb-item active">View Inbound</li>
	      </ul>
	    </div>
	  </div>

	<?php 
		$query = "SELECT * FROM tb_inbound ORDER BY id DESC";
		$result = mysqli_query($conn,$query);

		$asar_db_in = array();

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_db_in[$db_rows['in_ref_num']] = $db_rows;
		}
	 ?>
     
     <div class="container">
      <div class="col-md-6 mx-auto text-center">
        <div class="header-title">
          <h2 class="wv-heading--subtitle">
            Barcodes Generator
          </h2>
        </div>
      </div>
    </div>
    <br>
    <div class="cotainer">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#barcodeModal">New Barcode
              </button>
            </div>
            <div class="card-body">
              <?php
                //Connect to egm_barcodes database.
                $connection=mysqli_connect('localhost','root','','arrowgo_doh');
                
                $sql="SELECT * FROM tbl_products";
                $result=mysqli_query($connection,$sql);
                //data selected from the database.
                $arrayBarcodes=array();
              ?>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-sm">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Prodcut Name</th>
                      <th scope="col">Barcode</th>
                      <th scope="col">Created Date</th>
                    </tr>
                    <?php 
                      //get back rows of results
                      while($row=mysqli_fetch_row($result)):
                      $arrayBarcodes[]=(string)$row[2]; 
                    ?>
                    <tr>
                      <td><?php echo $row[0] ?></td>
                      <td><?php echo $row[1] ?></td>
                      <td>
                        <svg id='<?php echo "barcode".$row[2]; ?>'>
                      </td>
                      <td><?php echo $row[3] ?></td>
                    </tr>
                    <?php endwhile; ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!--Add new product Modal -->
    <div class="modal fade" id="barcodeModal" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="barcodeModalLabel">New barcode
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
            </button>
          </div>
          <div class="modal-body">
            <form action="php/insert.php" method="post">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label" required>Product name:
                </label>
                <input type="text" class="form-control" id="name" name="name" maxlength="20" required>
              </div>
              <hr>
              <button type="submit" class="btn btn-primary">Generate barcode
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
	<div class="modal fade" id="barcodeModal" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="barcodeModalLabel">New barcode
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
            </button>
          </div>
          <div class="modal-body">
            <form action="Barcode/index.php" method="post">
              <div class="form-group">
                <label for="recipient-name" class="col-form-label" required>Product name:
                </label>
                <input type="text" class="form-control" id="name" name="name" maxlength="20" required>
              </div>
              <hr>
              <button type="submit" class="btn btn-primary">Generate barcode
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>

  <?php
 	include("layout/footer.php"); 
  ?>
  <script type="text/javascript">
  //convert json to JS array data.
  function arrayjsonbarcode(j) {
    json = JSON.parse(j);
    arr = [];
    for (var x in json) {
      arr.push(json[x]);
    }
    return arr;
  }

  //convert PHP array to json data.
  jsonvalue = '<?php echo json_encode($arrayBarcodes) ?>';
  values = arrayjsonbarcode(jsonvalue);

  //generate barcodes using values data.
  for (var i = 0; i < values.length; i++) {
    JsBarcode("#barcode" + values[i], values[i].toString(), {
      format: "codabar",
      lineColor: "#000",
      width: 2,
      height: 30,
      displayValue: true
      }
    );
  }
</script>