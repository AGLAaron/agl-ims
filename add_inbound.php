<?php 
	session_start();
	date_default_timezone_set("Asia/Manila");
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions


	if(isset($_SESSION['last_activity'])){

		$last_activity = $_SESSION['last_activity'];
		$timeout = 1800; // 30 mins

		$time_now = time();

		$duration = $time_now-$last_activity;
		
		if($duration > $timeout){
			session_start();

			session_destroy();

			header("location:login.php");
		}
	}
	
?>



<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>
<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Inbound</a></li>
	        <li class="breadcrumb-item active">Create Inbound</li>
	      </ul>
	    </div>
	  </div>
	
   <?php 
		/**
		 * Get All Items
		 * It will be used in the Form That the User Will Select
		 * All Items including the Inactive and Active and as well as the Zero (0) Balance
		 */
		$all_items = get_available_items($conn);
		
		/**
		 * Get All Batch Numbers
		 */
		$all_batch_no = get_all_batch_no($conn,"items");

	  	/**
		 * Get all unit so it will be displayed in the form
		 * @var [type]
		 */
		
		$all_unit = get_all_unit($conn,"unit");
		/**
		 * All Consignee/Receiver
		 * @var [type]
		 */
		$all_consignee = get_all_out_cons($conn,"outbound_destination");
		
		
		
		//SUBMIT PROMPT
		if (isset($_SESSION['submit_res_type'])) {
			echo "<div class = \"container-fluid\">";
				echo "<div class=\"alert alert-{$_SESSION['submit_res_type']}\">";
					echo "{$_SESSION['submit_response']}";
				echo "</div>";
			echo "</div>";	

			unset($_SESSION['submit_res_type']);
			unset($_SESSION['submit_response']);
				
		}

		//INSERT PROMPT
		if (isset($_SESSION['insert_res_type'])) {
			echo "<div class = \"container-fluid\">";
				echo "<div class=\"alert alert-{$_SESSION['insert_res_type']}\">";
					echo "{$_SESSION['insert_res']}";
				echo "</div>";
			echo "</div>";	

			unset($_SESSION['insert_res_type']);
			unset($_SESSION['insert_res']);
				
		}

		//UPDATE PROMPT
		if (isset($_SESSION['update_res_type'])) {
			echo "<div class = \"container-fluid\">";
				echo "<div class=\"alert alert-{$_SESSION['update_res_type']}\">";
					echo "{$_SESSION['update_response']}";
				echo "</div>";
			echo "</div>";	

			unset($_SESSION['update_res_type']);
			unset($_SESSION['update_response']);
				
		}


    ?>
	
	<div class="container-fluid">
	 	<form action="add_inbound_proc.php" method="post">
	 		<div class="row">
		 		<div class="col-lg-12 col-sm-12">
		 			
		 			<div class="card">
		 				<div class="card-header align-items-center">
							<h4>Inbound Information and Fleet Details</h4>
						</div>
		 				<div class="card-body">
		 					<div class="form-group">
		 						<div class="row">
								    <div class="col-lg-3">
										 <label class="row col-sm-10 ft-style">Arrival Date</label>
										 <input type="date" placeholder="Enter Inbound Date" name="f_shipper" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="Inbound Date">
			 						</div>

	                                 <div class="col-lg-3">
									 	<label class="row col-sm-10 ft-style">Arrival Time</label>
			 							<input  placeholder="Enter Inbound Time" name="f_dest" class="form-control" onmouseover="(this.type='time')" data-toggle="tooltip" data-placement="top" title="Inbound Time">
			 						</div>
			 			             
									 <div class="col-lg-3">
									 	<label class="row col-sm-10 ft-style">Unloading Start</label>
									 	<input  placeholder="Enter Stripping Start" name="Strip_Start" class="form-control" onmouseover="(this.type='time')" data-toggle="tooltip" data-placement="top" title="Stripping Start">
			 						</div>

									 <div class="col-lg-3">
									 	<label class="row col-sm-10 ft-style">Unloading End</label>
										 <input  placeholder="Enter Stripping End" name="Strip_End" class="form-control" onmouseover="(this.type='time')" data-toggle="tooltip" data-placement="top" title="Stripping End">
			 						</div>
		 						</div><br/>

								 <div class="form-group">
		 							<div class="row">
		 								<div class="col-lg-6">
									 		<label class="row col-sm-10 ft-style">Documentation Ref. DR No.</label>
									   		<input  placeholder="Enter Ref. DR No." name="docu_Ref" class="form-control"  data-toggle="tooltip" data-placement="top" title="Documentation Ref. DR No.">
			 							</div>

									 	<div class="col-lg-6">
									 		<label class="row col-sm-10 ft-style">BI Ref No.</label>
									 		<input  placeholder="Enter Bl Ref No." name="BL_Ref" class="form-control"  data-toggle="tooltip" data-placement="top" title="Bl Ref No.">
			 							</div>
		 							</div>
		 						</div>	

								 <div class="form-group">
		 							<div class="row">
		 								<div class="col-lg-3">
									 		<label class="row col-sm-10 ft-style">Plate No.</label>
									 		<input  placeholder="Enter Plate No." name="plate_No" class="form-control"  data-toggle="tooltip" data-placement="top" title="Plate No.">
			 							</div>
									 <div class="col-lg-3">
									 	<label class="row col-sm-10 ft-style">Routing</label>
									 	<input  placeholder="Enter Routing" name="route" class="form-control"  data-toggle="tooltip" data-placement="top" title="Routing">
			 						</div>
									 <div class="col-lg-3">
									   <label class="row col-sm-10 ft-style">Driver</label>
									   <input  placeholder="Enter Diver" name="driver" class="form-control"  data-toggle="tooltip" data-placement="top" title="Driver">
			 						</div>
									 <div class="col-lg-3">
									 	<label class="row col-sm-10 ft-style">Courier</label>
										 <input  placeholder="Enter Courier" name="courier" class="form-control"  data-toggle="tooltip" data-placement="top" title="Courier">
	                                </div>
		 						</div>
		 					</div>	
		 				  </div>
		 				</div>
		 			</div>
		 		</div>

		 		<div class="col-lg-12">
		 			<div class="card">
		 				<div class="card-header align-items-center">
							<h4>Inbound Item Information</h4>
						</div>
						<div class="card-body ">
							<div class="form-group item_info_field">
								<div class="row mb-3">
									<div class="col-lg-2 col-md-3 col-sm-12">
									
										<input list="property_datalist" name="f_property_no[]" placeholder="Item Code" class="form-control" data-toggle="tooltip" data-placement="top" title="Item Code">
										<datalist id="property_datalist">
											<?php foreach($all_items as $item_key => $arr_val){ ?>
													<option value="<?php echo $item_key; ?>"><?php echo $arr_val['item_batch_num']."-".$arr_val['item_name'];?></option>
											<?php } ?>
										</datalist>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-12">
										<input list="property_datalist1" name="Batch[]" placeholder="Batch No." class="form-control" data-toggle="tooltip" data-placement="top" title="Batch Number">
										<datalist id="property_datalist1">
											<?php foreach($all_items as $item_key => $arr_val){ ?>
													<option value="<?php echo $item_key; ?>"><?php echo $arr_val['item_batch_num'].""?></option>
											<?php } ?>
										</datalist>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-12">
										<input type="text" name="desc[]" placeholder="Item description" class="form-control" data-toggle="tooltip" data-placement="top" title="Item Description">
									</div>
									
									<div class="col-lg-1 col-md-2 col-sm-12">
										<input type="number" name="BOX[]" placeholder="BOX" class="form-control" data-toggle="tooltip" data-placement="top" title="Qty Box">
									</div>

									<div class="col-lg-1 col-md-2 col-sm-12">
										<input type="number" name="f_qty[]" placeholder= "PCS" class="form-control" data-toggle="tooltip" data-placement="top" title="Qty PCS">
									</div>

									<div class="col-lg-1 col-md-3 col-sm-12">
									    <input type="text" name="size[]" placeholder="Pack" class="form-control" data-toggle="tooltip" data-placement="top" title="Packing">	
									</div>

									<div class="col-lg-2 col-md-2 col-sm-12">
										<input type="text" name="ttl[]" placeholder="Total" class="form-control" data-toggle="tooltip" data-placement="top" title="Total Weight">
									</div>

									<div class="col-lg-1 col-md-1 col-sm-12 text-left">
										<a href="javascript:void(0);" class="add_item_button btn btn-primary btn-primary1 form-control" title="Add Item">+</a>
									</div>	
								</div>
							</div>
						</div>
		 			</div>
		 			<div class="row">
						<div class="col-lg-12 text-center mb-2">
							<button type="submit" name="add_inbound" class="btn btn-primary btn-lg" >Create Inbound</button>
						</div>							
					</div>
		 		</div>
		 	</div>
	 	</form>	 		
	 </div>

  <?php
 	include("layout/footer.php"); 
  ?>
<script type="text/javascript">
	$(document).ready(function(){
		var maxField = 1000; //Input fields increment limitation
		var addButton = $('.add_item_button'); //Add button selector
		var wrapper = $('.item_info_field'); //Input field wrapper	

		var x = 1; //Initial field counter is 1
		var form_id_count = 0;
		//Once add button is clicked
		$(addButton).click(function(){

			var form_id = "add-form-"+form_id_count;
			console.log(form_id);
			var fieldHTML = '<div class="form-group" id="'+form_id+'"><div class="row mb-3"> <div class="col-lg-2 col-md-3 col-sm-12"><input list="property_datalist" name="f_property_no[]" placeholder="Select" class="form-control"></div> <div class="col-lg-2 col-md-3 col-sm-12"><input type="text" name="Batch[]" placeholder="Batch No." class="form-control" data-toggle="tooltip" data-placement="top" title="Batch Number"></div> <div class="col-lg-2 col-md-3 col-sm-12"><input type="text" name="desc[]" placeholder="Item description" class="form-control" data-toggle="tooltip" data-placement="top" title="Item Description"></div> <div class="col-lg-1 col-md-3 col-sm-12"><input type="number" name="BOX[]" placeholder="BOX" class="form-control" data-toggle="tooltip" data-placement="top" title="Qty Box"></div> <div class="col-lg-1 col-md-3 col-sm-12"><input type="number" name="f_qty[]" placeholder= "PCS" class="form-control" data-toggle="tooltip" data-placement="top" title="Qty PCS"></div> <div class="col-lg-1 col-md-3 col-sm-12"><input type="text" name="size[]" placeholder="Pack" class="form-control" data-toggle="tooltip" data-placement="top" title="Packing"></div> <div class="col-lg-2 col-md-3 col-sm-12"><input type="text" name="ttl[]" placeholder="Total" class="form-control" data-toggle="tooltip" data-placement="top" title="Total Weight"></div> <div class="col-lg-1 col-md-1 col-sm-12 text-left"><a href="javascript:void(0);" class="remove_item_button btn btn-danger btn-danger1 form-control" onclick=remove_item("'+form_id+'") title="Remove Item">-</a></div></div>';
			//Check maximum number of input fields
			if(x < maxField){ 
				x++; //Increment field counter
				form_id_count++;
				$(wrapper).append(fieldHTML); //Add field html  
			}
		});
	});

	function remove_item($id){
		document.getElementById($id).remove(); //Remove field html
		
	}

</script>


