<?php 
	session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions
	
?>
<?php
	include("layout/head.php");
	include("layout/main_nav.php"); 
	include("layout/sidebar.php");
?>

<!-- Breadcrumb-->
	  <div class="breadcrumb-holder mb-2">
	    <div class="container-fluid">
	      <ul class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.php">Report</a></li>
	        <li class="breadcrumb-item active">Summary Report</li>
	      </ul>
	    </div>
	  </div>

	  <?php 
		/**
		 * [All items from DB]
		 * Associative array the array key is the property/sku Number
		 * @var [type]
		 */
		$all_items = get_all_items_report($conn,"items");

		// /**
		//  * [All Inbound From DB]
		//  * Associative array, the arry key is the inbound reference number
		//  * @var [type]
		//  */
		// $all_inbound = get_all_inbound($conn,"tb_inbound");

		// /**
		//  * All Outbound from Db
		//  * Associative Array, the array key is the outbound reference number
		//  * @var  [type]
		//  */
		// $all_outbound = get_all_outbound($conn,"tb_outbound");


		/**
		 * Verify If btn_report is clicked
		 */

		if(isset($_POST['btn_report'])){

			/**
			 * Verify Date Fields
			 */

			if(empty(trim($_POST['start_date'])) || empty(trim($_POST['end_date']))){
				echo "<div class = \"container-fluid\">";
					echo "<div class=\"alert alert-danger\">";
						echo "<strong>Error!</strong> Please Fill all Fields";
					echo "</div>";
				echo "</div>";
			}else{
				/**
				 * Start and End Date
				 * @var [type]
				 */
				$start_date = $_POST['start_date'];
				$end_date = $_POST['end_date'];

				/**
				 * ====================================================================================
				 * First Table Displays All Stock Item with (Opening Balance, Total Inbound 
				 * 											 and Total Outbound, Ending Balance)
				 * ====================================================================================
				 */
				
				$stock_summary = array();

				/*
					First we need to put all Items that are inside the Warehouse
					This will serve as a reference.
					For Each Item in Warehouse Get All Inbound Transaction and Get All Outbound Transaction
					Between the Start Period and End Period Being Set by the User
				 */
				
				foreach($all_items as $item_property_num => $item_details){
					$stock_summary[$item_property_num]['property_num'] = $item_details['property_num'];
					$stock_summary[$item_property_num]['item_name'] = $item_details['item_name'];
					$stock_summary[$item_property_num]['item_batch_num'] = $item_details['item_batch_num'];
					$stock_summary[$item_property_num]['item_expiry'] = $item_details['item_expiry'];
					$stock_summary[$item_property_num]['item_unit'] = $item_details['item_unit'];
					$stock_summary[$item_property_num]['item_unit_cost'] = $item_details['item_unit_cost'];
					$stock_summary[$item_property_num]['opening_qty'] = 0;
					$stock_summary[$item_property_num]['in'] = 0;
					$stock_summary[$item_property_num]['out'] = 0;
					$stock_summary[$item_property_num]['ending_qty'] = 0;

				}

				/**
				 * OPENING BALANCE
				 * ============================================================
				 * Get All Inbound Transaction Before the "Start Date"
				 * This Will Be the Opening Balance
				 * Insert in the Stock Summary as 'opening balance'
				 * @var [type]
				 */
				
				$prev_inbound = get_prev_inbound($conn,"tb_inbound",$start_date);

				if(!empty($prev_inbound)){
					foreach($prev_inbound as $in_key => $in_det){
						if(isset($stock_summary[$in_det['in_property_num']])){
							$stock_summary[$in_det['in_property_num']]['opening_qty'] = $stock_summary[$in_det['in_property_num']]['opening_qty'] + $in_det['in_qty'];
						}
					}
				}

				/**
				 * INBOUND TRANSACTIONS BETWEEN START AND END DATE
				 * ===========================================================================================
				 * We Will Display the Qty of the Inbound Transaction
				 */
				
				$bet_inbound = get_bet_inbound($conn,"tb_inbound",$start_date,$end_date);

				if(!empty($bet_inbound)){
					foreach($bet_inbound as $in_key => $in_det){
						$property_num = $in_det['in_property_num'];
						if(isset($stock_summary[$property_num])){
							$stock_summary[$property_num]['in'] = $stock_summary[$property_num]['in'] + $in_det['in_qty'];
						}
					}
				}

				
				/**
				 * OUTBOUND TRANSACTION BETWEEN START AND END DATE
				 * ===========================================================================================
				 * We will display the Qty of the Outbound Transaction
				 */
				$bet_outbound = get_bet_outbound($conn,"tb_outbound",$start_date,$end_date);
				
				if(!empty($bet_outbound)){
					foreach($bet_outbound as $out_key => $out_det){
						if(isset($stock_summary[$out_det['out_property_num']])){
							$stock_summary[$out_det['out_property_num']]['out'] = $stock_summary[$out_det['out_property_num']]['out'] + $out_det['out_qty'];
						}
					}
				}

				/**
				 * ENDING BALANCE/QTY CALCULATION
				 * ===========================================================================================
				 */
				
				foreach($stock_summary as $stock_sum_id => $stock_det){
					if(isset($stock_summary[$stock_det['property_num']])){
						$stock_summary[$stock_sum_id]['ending_qty'] = $stock_det['opening_qty'] + $stock_det['in'] - $stock_det['out'];
					}	
				}

				
				//print_r_html($stock_summary);
				
			}


		}


	 ?>

	<div class="container-fluid">
	 	<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post">
	 		<div class="row">
		 		<div class="col-lg-12 col-sm-12">
		 			
		 			<div class="card">
		 				<div class="card-header align-items-center">
							<h4>Summary Report Date Filter</h4>
						</div>
		 				<div class="card-body">
		 					<div class="form-group">
		 						<div class="row">
		 							<div class="col-lg-4">
		 								<input  placeholder="To" name="start_date" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="Start Date">
		 							</div>
		 							<div class="col-lg-4">
		 								<input  placeholder="From" name="end_date" class="form-control" onmouseover="(this.type='date')" data-toggle="tooltip" data-placement="top" title="End Date">
		 							</div>
		 							<div class="col-lg-4">
		 								<button type="submit" class="btn btn-primary" name="btn_report">Generate Summary Report</button>
		 							</div>
		 						</div>
		 					</div>
		 				</div>
		 			</div>
		 		</div>

		 		
		 	</div>
	 	</form>	 		
	 </div>
		
	<?php if(!empty($stock_summary)){ ?>
	 <div class="container-fluid">
	 	<div class="card">
	 		<div class="card-header align-items-center">
	 			<div class="row">
	 				<div class="col-lg-4 justify-content-center">
		 				<h4>Stock Summary Report</h4>
		 			</div>
		 			<div class="col-lg-2">
		 				<span class="small"><strong>Start Period:</strong> <span class="text-primary"><?php echo $start_date; ?></span></span>
		 			</div>
		 			<div class="col-lg-2">
		 				<span class="small"><strong>End Period:</strong> <span class="text-primary"><?php echo $end_date; ?></span></span>
		 			</div>
		 			<div class="col-lg-4 text-right">
		 				<a target="_blank" href="<?php echo "print_summary_report.php?start_date={$start_date}&end_date={$end_date}"; ?>" class="btn btn-primary">Print Report</a>
		 			</div>
	 			</div>
	 		</div>
	 		<div class="card-body">
	 			<div class="table-responsive">
	 				<table class="table table-striped table-hover table-md" id="stock_sum_tbl">
	 					<thead>
	 						<tr class="bg-primary">
	 						  <th class="small text-center px-3 py-2 font-weight-bold text-light">Property No.</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Name</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Batch No.</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Expiration Date</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Opening Qty</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">In</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Out</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Ending Qty</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Unit</th>
						      <th class="small text-center px-3 py-2 font-weight-bold text-light">Unit Cost</th>
	 						</tr>
	 					</thead>
	 					<tbody>
	 						<?php foreach($stock_summary as $stock_property_num => $stock_det){ ?>
	 						<tr>
	 							<th scope="row" class="small text-center font-weight-bold"><?php echo $stock_det['property_num']; ?></th>
	 							<td class="small text-center"><?php echo $stock_det['item_name']; ?></td>
						      	<td class="small text-center"><?php echo $stock_det['item_batch_num']; ?></td>
						      	<td class="small text-center"><?php echo $stock_det['item_expiry']; ?></td>
						      	<td class="small text-center"><?php echo $stock_det['opening_qty']; ?></td>
						      	<td class="small text-center"><?php echo $stock_det['in']; ?></td>
						      	<td class="small text-center"><?php echo $stock_det['out']; ?></td>
						      	<td class="small text-center font-weight-bold"><?php echo $stock_det['ending_qty']; ?></td>
						      	<td class="small text-center"><?php echo $stock_det['item_unit']; ?></td>
						      	<td class="small text-center"><?php echo $stock_det['item_unit_cost']; ?></td>
						      <?php } ?>
	 						</tr>
	 					</tbody>
	 				</table>
	 			</div>
	 		</div>
	 	</div>

	 	<div class="card">
	 		<div class="card-header">
	 			<div>
	 				<h4>Stock Inbound</h4>
	 			</div>
	 		</div>
	 		<div class="card-body">
	 			<?php if(empty($bet_inbound)){ ?>
	 					<div class="table-responsive">
	 						<h6>No Inbound Transactions</h6>
	 					</div>
	 			<?php }else{?>
	 					<div class="table-responsive">
	 						<table class="table table-striped table-hover table-md" id="stock_in_tbl">
	 							<thead>
	 								<tr class="bg-primary">
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Ref. #</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Shipper</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Date</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Property No.</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Name</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Batch No.</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Qty</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Unit</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Expiration Date</th>
	 								</tr>
	 							</thead>
	 							<tbody>
	 								<?php foreach($bet_inbound as $inbound_key => $inbound_det){ ?>
										<tr>
											<th scope="row" class="small text-center font-weight-bold"><?php echo $inbound_det['in_ref_num']; ?></th>
				 							<td class="small text-center"><?php echo $inbound_det['in_shipper']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_date']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_property_num']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_item_name']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_batch_num']; ?></td>
									      	<td class="small text-center font-weight-bold"><?php echo $inbound_det['in_qty']; ?></td>
									      	<td class="small text-center "><?php echo $inbound_det['in_unit']; ?></td>
									      	<td class="small text-center"><?php echo $inbound_det['in_expiry']; ?></td>
										</tr>
	 								<?php } ?>
	 							</tbody>
	 						</table>
	 					</div>
	 			<?php } ?>
	 		</div>
	 	</div>

	 	<div class="card">
	 		<div class="card-header">
	 			<div>
	 				<h4>Stock Outbound</h4>
	 			</div>
	 		</div>
	 		<div class="card-body">
	 			<?php if(empty($bet_outbound)){ ?>
	 					<div class="table-responsive">
	 						<h6>No Outbound Transactions</h6>
	 					</div>
	 			<?php }else{?>
	 					<div class="table-responsive">
	 						<table class="table table-striped table-hover table-md" id="stock_out_tbl">
	 							<thead>
	 								<tr class="bg-primary">
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Ref. #</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">PTR No.</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Consignee</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Date</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Property No.</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Name</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Batch No.</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Qty</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Unit</th>
	 									 <th class="small text-center px-3 py-2 font-weight-bold text-light">Expiration Date</th>
	 								</tr>
	 							</thead>
	 							<tbody>
	 								<?php foreach($bet_outbound as $outbound_key => $outbound_det){ ?>
										<tr>
											<th scope="row" class="small text-center font-weight-bold"><?php echo $outbound_det['out_ref_num']; ?></th>
											<th scope="row" class="small text-center font-weight-bold"><?php echo $outbound_det['out_ptr_num']; ?></th>
				 							<td class="small text-center"><?php echo $outbound_det['out_consignee']; ?></td>
									      	<td class="small text-center"><?php echo $outbound_det['out_date']; ?></td>
									      	<td class="small text-center"><?php echo $outbound_det['out_property_num']; ?></td>
									      	<td class="small text-center"><?php echo $outbound_det['out_item_name']; ?></td>
									      	<td class="small text-center"><?php echo $outbound_det['out_batch_num']; ?></td>
									      	<td class="small text-center font-weight-bold"><?php echo $outbound_det['out_qty']; ?></td>
									      	<td class="small text-center "><?php echo $outbound_det['out_unit']; ?></td>
									      	<td class="small text-center"><?php echo $outbound_det['out_expiry']; ?></td>
										</tr>
	 								<?php } ?>
	 							</tbody>
	 						</table>
	 					</div>
	 			<?php } ?>
	 		</div>
	 	</div>

	 </div>
	<?php } ?>


<?php
	include("layout/footer.php"); 
?>
<script>
	$(document).ready(function() {
	    $('#stock_sum_tbl').DataTable();
	    $('#stock_in_tbl').DataTable();
	    $('#stock_out_tbl').DataTable();
	} );
</script>