<?php
    session_start();
	include('inc/db/bd_connect.php'); // Db Connection
	include('inc/agl_ct.php'); // Constant
	include('inc/agl_fn.php'); // Functions
?>

<?php
 	include("layout/head.php");
 	include("layout/main_nav.php"); 
 	include("layout/sidebar.php");
?>

<?php   

    $query = "SELECT * FROM tb_receive ORDER BY receive_id DESC";
    $result = mysqli_query($conn,$query);

    $asar_stock_receive = array();

    while($db_rows = mysqli_fetch_assoc($result)){
        $asar_stock_receive[$db_rows['ref_num']] = $db_rows;
    }

?>
    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Misc</a></li>
            <li class="breadcrumb-item active">View Received Stock/s</li>
            </ul>
        </div>
    </div>

    <div class="container-fluid mt-3">
        <div class="card">
            <div class="card-header align-items-center">
                <h4>Stock Receive Transactions</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-md" id="tbl_stock_rec">
                        <thead>
                            <tr class="bg-primary">
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Reference No.</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">PTR No.</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Shipper</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Received Date</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Remarks</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Created By</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Date Created</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Action</th>
                                <th class="small text-center font-weight-bold text-light px-3 py-2">Barcode</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($asar_stock_receive as $stock_ref_num => $receive_details){ ?>
                            <tr>
                                <td class="small text-center font-weight-bold"><?php echo $stock_ref_num; ?></td>
                                <td class="small text-center font-weight-bold"><?php echo $receive_details['ptr_no']; ?></td>
                                <td class="small text-center"><?php echo $receive_details['shipper']; ?></td>
                                <td class="small text-center"><?php echo $receive_details['receive_date']; ?></td>
                                <td class="small text-center"><?php echo $receive_details['remarks']; ?></td>
                                <td class="small text-center"><?php echo $receive_details['created_by']; ?></td>
                                <td class="small text-center"><?php echo $receive_details['date_created']; ?></td>
                                <td class="small text-center"><a target="_blank" href="<?php echo "print_stock_rec.php?ref_no={$stock_ref_num}";?>" class="btn btn-primary">Print</a></td>
                                <td class="small text-center"><a target="_blank" class="btn btn-primary btn-primary2 btn-md" data-toggle="modal" data-target="#barcodeModal">Generate Barcode</a></td>
                            </tr>
                            <?php } ?> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="barcodeModal" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="barcodeModalLabel">New barcode
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;
              </span>
            </button>
          </div>
          <div class="modal-body">
          
  	            <form class="form-horizontal" method="post" action="barcode-master/barcode.php" target="_blank">
  	                <div class="form-group">
                         <label class="control-label col-sm-10" for="product">PTR No./ Transaction No.:</label>
                            <div class="col-sm-12">
                                <input autocomplete="OFF" type="text" class="form-control" id="product" name="product">
                            </div>
                     </div>
                    <div class="form-group">
                          <label class="control-label col-sm-4" for="product_id">Item Name:</label>
                            <div class="col-sm-12">
                                <input autocomplete="OFF" type="text" class="form-control" id="item_name" name="item_name">
                            </div>
                     </div>
                     <div class="form-group">
                          <label class="control-label col-sm-10" for="product_id">Item Code/Property No.:</label>
                            <div class="col-sm-12">
                                <input autocomplete="OFF" type="text" class="form-control" id="item_code" name="item_code">
                            </div>
                     </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4" for="rate">Expiry Date:</label>
                             <div class="col-sm-12">          
                                <input autocomplete="OFF" type="date" class="form-control" id="exp"  name="exp">
                            </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4" for="rate">Batch No.:</label>
                             <div class="col-sm-12">          
                                <input autocomplete="OFF" type="text" class="form-control" id="batch"  name="batch">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="control-label col-sm-4" for="print_qty">Barcode Quantity</label>
                             <div class="col-sm-12">          
                                <input autocomplete="OFF" type="print_qty" class="form-control" id="print_qty"  name="print_qty">
                             </div>
                    </div>

                    <div class="form-group">        
                        <div class="col-sm-offset-2 col-sm-10">
                           <button type="submit" class="btn btn-primary btn-sm">Generate</button>
                  </div>
             </form>
           </div>
          </div>
        </div>
      </div>
    </div>


<?php
    include("layout/footer.php"); 
?>

<script>
  	$(document).ready(function() {
	    $('#tbl_stock_rec').DataTable({
	    	"order": [[ 5, "desc" ]]
	    });
	} );
</script>