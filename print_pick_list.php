<?php 
    ob_start();
    session_start();
    include('inc/db/bd_connect.php'); // Db Connection
    include('inc/agl_ct.php'); // Constant
    include('inc/agl_fn.php'); // Functions
    include('fpdf/fpdf.php'); // fpdf
    include('fpdf/easytable/exfpdf.php'); // exfpdf
    include('fpdf/easytable/easyTable.php'); // easytable


    if(isset($_GET['ref_no'])){

        $query = "SELECT * FROM tb_order WHERE order_ref_num = '{$_GET['ref_no']}'";
		
		$result = mysqli_query($conn,$query);
		$num_rows = mysqli_num_rows($result); //Number of rows
		
		$asar_order = array();
		while($db_row = mysqli_fetch_assoc($result)){
			
			$asar_order[]= $db_row;
		}
		
		$total = 0;
		foreach($asar_order as $asar_order_key =>$asar_arr_val){
				$order_date = $asar_arr_val['order_date'];
				$order_created_by = $asar_arr_val['order_created_by'];
				$shipper = $asar_arr_val['order_shipper'];
				$consignee = $asar_arr_val['order_consignee'];
				$trans_type = $asar_arr_val['order_transfer_type'];
				$ptr_no = $asar_arr_val['order_ptr_num'];
				$pull_out_date = $asar_arr_val['order_release_date'];
				$remarks = $asar_arr_val['order_remarks'];
				$order_type = $asar_arr_val['order_type'];
				if ($order_type == "d") {
					$order_type = "Delivery";
				}else{
					$order_type = "Pick-Up";
				}
        }

        $pdf=new exFPDF('P','mm','A4');
		$pdf->AddPage(); 
        $pdf->SetFont('arial','',10);
        
        $header = new easyTable($pdf,2);
        $header->easyCell('','img:img/invoice-logo.png, w50; align:L;');
        $header->easyCell('WAREHOUSE PICK LIST','font-size:20; font-style:B; align:R; font-color:#424344;');
        $header->printRow();
        $header->endTable(4);

        $table = new easyTable($pdf, '{110,40,40}');

        $table->rowStyle('font-size:8');
        $table->easyCell('Warehouse A and B, New Bypass Road' . "\n" . ' Mamay Road,Buhangin' . "\n" . 'Davao City,Philippines', 'rowspan:4; valign:T');
        $table->easyCell('<b>Order Date:</b>', '');
        $table->easyCell($order_date, 'border:1; border-color:#afb5bf; align:R');
        $table->printRow();

        $table->rowStyle('font-size:8');
        $table->easyCell('<b>Order #:</b>', '');
        $table->easyCell($_GET['ref_no'], 'border:1; border-color:#afb5bf; align:R');
        $table->printRow();

        $table->rowStyle('font-size:8');
        $table->easyCell('<b>Order Created By:</b>', '');
        $table->easyCell($order_created_by, 'border:1; border-color:#afb5bf; align:R');
        $table->printRow();

        $table->rowStyle('font-size:8');
        $table->easyCell('<b>Order Type:</b>', '');
        $table->easyCell($order_type, 'border:1; border-color:#afb5bf; align:R');
        $table->printRow();
        $table->endTable(5);

        $tableB = new easyTable($pdf, '{130,60}');

        $tableB->rowStyle('font-size:11');
        $tableB->easyCell('ORDER DETAILS', 'bgcolor:#424344; border-color:#424344;border:1; align:L; font-color:#ffffff; font-style:B; colspan:2');
        $tableB->printRow();

        $tableB->rowStyle('font-size:9; border:LR; border-color:#afb5bf');
        $tableB->easyCell('<b>Shipper: </b>' . $shipper);
        $tableB->easyCell('<b>PTR #: </b>' . $ptr_no);
        $tableB->printRow();

        $tableB->rowStyle('font-size:9; border:LBR; border-color:#afb5bf');
        $tableB->easyCell('<b>Consignee: </b>' . $consignee);
        $tableB->easyCell('<b>' . $order_type . ' Date: </b>' . $pull_out_date);
        $tableB->printRow();

        $tableB->rowStyle('font-size:9; border:LBR; border-color:#424344; bgcolor:#424344');
        $tableB->easyCell('', 'colspan:2');
        $tableB->printRow();

        $tableB->rowStyle('font-size:9; border:LR; border-color:#afb5bf');
        $tableB->easyCell('<b>Transfer Type: </b>' . $trans_type, 'colspan:2');
        $tableB->printRow();

        $tableB->rowStyle('font-size:9; border:LBR; border-color:#afb5bf');
        $tableB->easyCell('<b>Remarks/Reason For Transfer: </b>' . $remarks, 'colspan:2');
        $tableB->printRow();
        $tableB->endTable(5);

        /**
         * Item Details Section
         */

        $tableC = new easyTable($pdf, '{20,60,20,20,20,10,25,15}');

        $tableC->rowStyle('border:1; border-color:#afb5bf; bgcolor:#424344');
        $tableC->easyCell('Property No.', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->easyCell('Item Name', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->easyCell('Batch No.', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->easyCell('Expiry Date', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->easyCell('Qty', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->easyCell('Unit', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->easyCell('Storage', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->easyCell('Picked', 'align:C; font-style:B; font-color:#ffffff; paddingY:3; font-size:7');
        $tableC->printRow();

        $maxRow = 10;
        $initRow = 0;
        $i = 0;
        if ($num_rows > $maxRow) {

            foreach ($asar_order as $asar_order_key => $asar_arr_val) {
                $bgcolor = '';
                if ($i % 2) {
                    $bgcolor = 'bgcolor:#afb5bf;';
                }

                $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
                $tableC->easyCell($asar_arr_val['order_property_num']);
                $tableC->easyCell($asar_arr_val['order_item_name']);
                $tableC->easyCell($asar_arr_val['order_batch_num']);
                $tableC->easyCell($asar_arr_val['order_expiry']);
                $tableC->easyCell($asar_arr_val['order_qty']);
                $tableC->easyCell($asar_arr_val['order_unit']);
                $tableC->easyCell($asar_arr_val['s_loc']);
                $tableC->easyCell('');
                $tableC->printRow();
                $i++;
                $maxRow--;
            }
        } else {


            foreach ($asar_order as $asar_order_key => $asar_arr_val) {
                $bgcolor = '';
                if ($i % 2) {
                    $bgcolor = 'bgcolor:#afb5bf;';
                }

                $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
                $tableC->easyCell($asar_arr_val['order_property_num']);
                $tableC->easyCell($asar_arr_val['order_item_name']);
                $tableC->easyCell($asar_arr_val['order_batch_num']);
                $tableC->easyCell($asar_arr_val['order_expiry']);
                $tableC->easyCell($asar_arr_val['order_qty']);
                $tableC->easyCell($asar_arr_val['order_unit']);
                $tableC->easyCell($asar_arr_val['sloc']);
                $tableC->easyCell('');
                $tableC->printRow();
                $i++;
                $maxRow--;
            }

            while ($maxRow != 0) {
                if ($i % 2) {
                    $bgcolor = 'bgcolor:#afb5bf;';
                }
                $tableC->rowStyle('border:1; border-color:#afb5bf;font-size:6; align:C{CCCCCCCC}; paddingY:3; split-row:true');
                $tableC->easyCell('');
                $tableC->easyCell('');
                $tableC->easyCell('');
                $tableC->easyCell('');
                $tableC->easyCell('');
                $tableC->easyCell('');
                $tableC->easyCell('');
                $tableC->easyCell('-');
                $tableC->printRow();
                $i++;
                $maxRow--;
            }
        }
		 
        $tableC->endTable(0);
        
        $tableD = new easyTable($pdf,'{20,60,20,20,10,10,25,5,20}','split-row:true');

        $tableD->rowStyle('font-size:7; split-row:false');
        $tableD->easyCell('','colspan:8; paddingY:2');
        $tableD->printRow();

        $tableD->rowStyle('font-size:7');
        $tableD->easyCell('SPECIAL NOTE(S) AND ACTION','font-style:B;colspan:9; bgcolor:#424344; border:1; border-color:#424344; font-color:#ffffff;');
        $tableD->printRow();

       
        $tableD->easyCell('','font-style:B;colspan:9; border:LRB; border-color:#afb5bf; rowspan:5');
        $tableD->easyCell('');
        $tableD->printRow();

        $tableD->rowStyle('paddingY:3');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->printRow();

        $tableD->rowStyle('paddingY:3');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->printRow();
        
        $tableD->rowStyle('paddingY:3');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->printRow();

        $tableD->rowStyle('paddingY:3');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->printRow();

        $tableD->rowStyle('paddingY:3');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->easyCell('');
        $tableD->printRow();
        $tableD->endTable(5);

        $table3 = new easyTable($pdf,'{25,30,15,20,30,20,20,10,20}','paddingY:1;valign:M;split-row:true');

        $table3->easyCell('','font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->easyCell('', 'font-size:6;align:C');
        $table3->printRow();
        $table3->endTable(5);

        $pdf->sign_field_pick_list($_SESSION['name']); // Sign Area

        $pdf->Output();


        
    }


?>