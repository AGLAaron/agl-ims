<?php 
	/**
	 * Function to Generate Property Number
	 */
	
	function generate_property_num(){
		$date_today = date('Ym');
		$random = mt_rand(1,rand(0,$date_today));
		return $random;
	}

	/**
	 * Function to Get All Items from DB [items] table
	 * @param  [type] $conn     [mysql connections]
	 * @param  [type] $tb_table [table name]
	 * @return [type]           [Associative Array]
	 */
	function get_all_items($conn,$tb_table){
			$asar_items = array();
			$query = "SELECT * FROM $tb_table";
			$result = mysqli_query($conn,$query);

			while($db_rows = mysqli_fetch_assoc($result)){
				if($db_rows['running_balance'] <= 0 || $db_rows['item_status'] == 0){
					continue;
				}
				$asar_items[$db_rows['property_num']] = $db_rows;
			}
			return $asar_items;
	}

	
	function get_all_items_report($conn,$tb_table){
			$asar_items = array();
			$query = "SELECT * FROM $tb_table";
			$result = mysqli_query($conn,$query);

			while($db_rows = mysqli_fetch_assoc($result)){
				$asar_items[$db_rows['property_num']] = $db_rows;
			}
			return $asar_items;
	}

	/**
	 * Function to get all All Items
	 *
	 * @param [type] $conn
	 * @param [type] $tb_table
	 * @return void
	 */

	 function get_available_items($conn){
		$asar_items = array();
		$query = "SELECT * FROM available_items";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_items[$db_rows['property_num']] = $db_rows;
		}

		return $asar_items;
	 }

	 /**
	 * Function to get all All Locked Items
	 *
	 * @param [type] $conn
	 * @param [type] $tb_table
	 * @return void
	 */

	 function get_locked_items($conn){
		$asar_items = array();
		$query = "SELECT * FROM lock_items";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_items[$db_rows['property_num']] = $db_rows;
		}

		return $asar_items;
	 }

	/**
	 * Function to get all batch number from DB [items] Table
	 * @param  [type] $conn     [DB connections]
	 * @param  [type] $db_table [table name]
	 * @return [type]           [Associative Array]
	 */
	function get_all_batch_no($conn,$db_table){
			$asar_num = array();
			$query = "SELECT * FROM $db_table";
			$result = mysqli_query($conn,$query);

			while($db_rows = mysqli_fetch_assoc($result)){
				$asar_num[$db_rows['item_batch_num']] = $db_rows['item_name'];  
			}

			return $asar_num;
	}

	/*
		Functions for Getting all the UNIT FROM DATABASE
	 */
	
	function get_all_unit($conn,$table_name){
		$table_exist = if_table_exist($table_name,$conn);
		if ($table_exist) {
			$query = "SELECT * FROM ".$table_name;		
			return get_unit_from_db($query,$conn);
		}else{
			//echo "Table does not exist";
			die("Table does not exist Please Contact your System Administrator.");
		}
	}

	function get_unit_from_db($query,$conn){
		$db_data = array();
		$result = mysqli_query($conn,$query);

		if (mysqli_num_rows($result) > 0) {
		    // output data of each row
		    while($row = mysqli_fetch_assoc($result)) {
		        $db_data[] = $row['unit_short_name'];
		    }
		} else {
		    echo "Table no data";
		}

		return $db_data;
	}

	function if_table_exist($tbl_name, $conn){

		$sql = "SHOW TABLES";
		$result = mysqli_query($conn,$sql);

		$tbl_name = strtolower($tbl_name);
		
		if (mysqli_num_rows($result) > 0) {
		    // output data of each row
		    foreach ($result as $value) {

		    	foreach($value as $table_name){
		    		$table_name = strtolower($table_name);

		    		if(strcmp($tbl_name,$table_name)==0){
		    			// echo "String Matched";
		    			// echo "<br/>";
		    			// echo "Table Name: ".$table_name;
		    			// echo "<br/>";
		    			return true;
		    		}
		    	}
			}
		} else {
		    echo "No Tables found inside the database";
		}
		
		return false;
		
	}



	/*
		Functions to Get Category From Database
	 */
	function get_all($table_name, $conn){
		$table_exist = if_table_exist($table_name,$conn);
		if ($table_exist) {
			$query = "SELECT * FROM ".$table_name;		
			return get_cat_from_db($query,$conn);
		}else{
			echo "Table does not exist";
		}
	}


	function get_cat_from_db($query,$conn){
		$db_data = array();
		$result = mysqli_query($conn,$query);

		if (mysqli_num_rows($result) > 0) {
		    // output data of each row
		    while($row = mysqli_fetch_assoc($result)) {
		        $db_data[] = $row['category_name'];
		    }
		} else {
		    echo "Table no data";
		}

		return $db_data;
	}





	/*--------------------------------------------------------------*/
	/* Function for Remove html characters
	/*--------------------------------------------------------------*/
	function remove_junk($str){
	  $str = nl2br($str);
	  $str = htmlspecialchars(strip_tags($str, ENT_QUOTES));
	  return $str;
	}

	/*
	 function to clean data before inserting to database
	 */
	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}


	/*
		Functions to Get Status From Database
	 */
	function get_all_stat($table_name, $conn){
		$table_exist = if_table_exist($table_name,$conn);
		if ($table_exist) {
			$query = "SELECT * FROM ".$table_name;		
			return get_stat_from_db($query,$conn);
		}else{
			echo "Table does not exist";
		}
	}


	function get_stat_from_db($query,$conn){
		$db_data = array();
		$result = mysqli_query($conn,$query);

		if (mysqli_num_rows($result) > 0) {
		    // output data of each row
		    while($row = mysqli_fetch_assoc($result)) {
		        $db_data[] = $row['status_name'];
		    }
		} else {
		    echo "Table no data";
		}

		return $db_data;
	}


	function print_r_html($o, $title = EmptyString)
	{
		if ($title) echobr($title . ":");
		?>
	    <pre><?php
		print_r($o);
		?></pre><?php
	}

	function echobr($s = STR_VOID, $a = STR_VOID, $b = STR_VOID, $c = STR_VOID, $d = STR_VOID, $e = STR_VOID, $f = STR_VOID, $g = STR_VOID, $h = STR_VOID)
	{
		echo '<br>', $s, $a, $b, $c, $d, $e, $f, $g, $h;
	}

	/*
		Function to Escape String for Use in SQL Statement
	 */
	function esc_str($conn,$str){
		$esc = mysqli_real_escape_string($conn,$str);
		return $esc;
	}

	/*
	Get the Total Amount 
	 */
	function get_amount($x,$y){
		$total = $x.$y;
		return $total;
	}

	/**
	 * Check if strings are equal
	 */
	function are_strings_equal($a, $b, $ignore_case = true)
	{
	//if(is_array($a)) {print_r_html($a,'this array arrived to '.debug_backtrace2string());}

		$a = '(' . $a;
		$b = '(' . $b;
		//why do this?
		//1 to have native strings, this is important to later be able to use ===
		//2 to solve issue when comparing strings with ==== that seem numbers, this way no automatic conversion shoulf happen by PHP, issues happened with strings like '8000014E-1423190654', PHP thinks it is a number in cientific notation

		if ($ignore_case) {
			$a = strtolower($a);
			$b = strtolower($b);
		} else  //be sure we have 2 strings in a and b because if not then === will always fail
		{
		}
		return ($a === $b);   //We do not use == because if you send "1.2" and "1.200" the result is true because this is they way PHP works with numeric strings using ==
	}



	/**
	 * Get The Running Balance of Each Item
	 */
	
	function get_curr_bal($conn,$db_table){
		$asar = array();
		$query = "SELECT * FROM $db_table";
		$result = mysqli_query($conn,$query);
		
		while ($db_rows = mysqli_fetch_assoc($result)) {
			$asar[$db_rows['item_batch_num']] = $db_rows['running_balance'];
		}

		return $asar;
	}

	/**
	 * Get all the Outbound Destination
	 */
	function get_all_out_cons($conn,$db_table){
		$asar = array();
		$query = "SELECT * FROM $db_table";
		$result = mysqli_query($conn,$query);

		while ($db_rows = mysqli_fetch_assoc($result)) {
			$asar[$db_rows['dest_name']] = $db_rows['dest_cat'];
		}

		return $asar;
	}

	/**
	 * Function to Generate Reference Number
	 * @param  [type] $conn        [DB connections]
	 * @param  [type] $transaction [transaction type 1=order, 2=outbound, 3=inbound, 4=stock receive ,5=stock return]
	 * @return [type]              [string reference number]
	 */

	 function generate_ref_num($transaction,$strength = 15){

		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		if($transaction == 1){
			$input_length = strlen($permitted_chars);
			$random_string = 'ORD';
			for($i = 0; $i < $strength; $i++) {
				$random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
				$random_string .= $random_character;
			}
		
			return $random_string;
		}

		if($transaction == 2){
			$input_length = strlen($permitted_chars);
			$random_string = 'OUT';
			for($i = 0; $i < $strength; $i++) {
				$random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
				$random_string .= $random_character;
			}
		
			return $random_string;
		}

		if($transaction == 3){
			$input_length = strlen($permitted_chars);
			$random_string = 'INB';
			for($i = 0; $i < $strength; $i++) {
				$random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
				$random_string .= $random_character;
			}
		
			return $random_string;
		}

		if($transaction == 4){
			$input_length = strlen($permitted_chars);
			$random_string = 'STCKRCV';
			for($i = 0; $i < $strength; $i++) {
				$random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
				$random_string .= $random_character;
			}
		
			return $random_string;
		}

		if($transaction == 5){
			$input_length = strlen($permitted_chars);
			$random_string = 'RET';
			for($i = 0; $i < $strength; $i++) {
				$random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
				$random_string .= $random_character;
			}
		
			return $random_string;
		}
		
		
	 }

	/**
	 * Function to Generate Reference Number
	 * @param  [type] $conn        [DB connections]
	 * @param  [type] $transaction [transaction type 1=order, 2=outbound, 3=inbound, 4=stock receive ,5=stock return]
	 * @return [type]              [string reference number]
	 */
	function generate_ref_num_old($conn,$transaction){
		if ($transaction == 1) { // if start
			$ref_len = 6;

			$unique_ref_found = false;

			$possible_chars = "23456789BCDFGHJKMNPQRSTVWXYZ";

			$unique_ref = "ORD".date('dms')."DOH";  
		    $i = 0;  
		      
		    // Add random characters from $possible_chars to $unique_ref   
		    // until $unique_ref_length is reached  
		    while ($i < $ref_len) {  
		      
		        // Pick a random character from the $possible_chars list  
		        $char = substr($possible_chars, mt_rand(0, strlen($possible_chars)-1), 1);  
		          
		        $unique_ref .= $char;  
		          
		        $i++;  
		      
		    }  
		}// if End

		if ($transaction == 2) { // if start
			$ref_len = 6;

			$unique_ref_found = false;

			$possible_chars = "23456789BCDFGHJKMNPQRSTVWXYZ";

			$unique_ref = "OUT".date('dms')."DOH";  
		    $i = 0;  
		      
		    // Add random characters from $possible_chars to $unique_ref   
		    // until $unique_ref_length is reached  
		    while ($i < $ref_len) {  
		      
		        // Pick a random character from the $possible_chars list  
		        $char = substr($possible_chars, mt_rand(0, strlen($possible_chars)-1), 1);  
		          
		        $unique_ref .= $char;  
		          
		        $i++;  
		      
		    }  
		}// if End

		if ($transaction == 3) { // if start
			$ref_len = 6;

			$unique_ref_found = false;

			$possible_chars = "23456789BCDFGHJKMNPQRSTVWXYZ";

			$unique_ref = "IN".date('dms')."DOH";  
		    $i = 0;  
		      
		    // Add random characters from $possible_chars to $unique_ref   
		    // until $unique_ref_length is reached  
		    while ($i < $ref_len) {  
		      
		        // Pick a random character from the $possible_chars list  
		        $char = substr($possible_chars, mt_rand(0, strlen($possible_chars)-1), 1);  
		          
		        $unique_ref .= $char;  
		          
		        $i++;  
		      
		    }  
		}// if End

		if ($transaction == 4) { // if start
			$ref_len = 6;

			$unique_ref_found = false;

			$possible_chars = "23456789BCDFGHJKMNPQRSTVWXYZ";

			$unique_ref = "STKREC".date('dms')."DOH";  
		    $i = 0;  
		      
		    // Add random characters from $possible_chars to $unique_ref   
		    // until $unique_ref_length is reached  
		    while ($i < $ref_len) {  
		      
		        // Pick a random character from the $possible_chars list  
		        $char = substr($possible_chars, mt_rand(0, strlen($possible_chars)-1), 1);  
		          
		        $unique_ref .= $char;  
		          
		        $i++;  
		      
		    }  
		}// if End

		if ($transaction == 5) { // if start
			$ref_len = 6;

			$unique_ref_found = false;

			$possible_chars = "23456789BCDFGHJKMNPQRSTVWXYZ";

			$unique_ref = "RTRN".date('dms')."DOH";  
		    $i = 0;  
		      
		    // Add random characters from $possible_chars to $unique_ref   
		    // until $unique_ref_length is reached  
		    while ($i < $ref_len) {  
		      
		        // Pick a random character from the $possible_chars list  
		        $char = substr($possible_chars, mt_rand(0, strlen($possible_chars)-1), 1);  
		          
		        $unique_ref .= $char;  
		          
		        $i++;  
		      
		    }  
		}// if End
	

    return $unique_ref;
	}




	 /**
     * Reporting
     * 
     */
    
    function get_all_inbound($conn,$tb_name){
		$asar_inbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_inbound[$db_rows['in_ref_num']][$db_rows['id']]= $db_rows;
		}
		
		return $asar_inbound;
	}

	 function get_all_outbound($conn,$tb_name){
		$asar_outbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_outbound[$db_rows['out_ref_num']][$db_rows['id']]= $db_rows;
		}
		
		return $asar_outbound;
	}

	 function get_prev_inbound($conn,$tb_name,$start_date){
		$asar_inbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$query .=" WHERE in_date < \"{$start_date}\"";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_inbound[$db_rows['id']]= $db_rows;
		}
		
		return $asar_inbound;
	}

	function get_bet_inbound($conn,$tb_name,$start_date,$end_date){
		$asar_inbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$query .=" WHERE in_date BETWEEN \"{$start_date}\" AND \"{$end_date}\"";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_inbound[$db_rows['id']]= $db_rows;
		}
		
		return $asar_inbound;
	}

	function get_bet_outbound($conn,$tb_name,$start_date,$end_date){
		$asar_outbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$query .=" WHERE out_date BETWEEN \"{$start_date}\" AND \"{$end_date}\"";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_outbound[$db_rows['id']]= $db_rows;
		}
		
		return $asar_outbound;
	}

	function get_item_prev_inbound($conn,$tb_name,$start_date,$property_num){
		$asar_inbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$query .=" WHERE in_property_num = \"{$property_num}\" AND in_date < \"{$start_date}\"";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_inbound[$db_rows['id']]= $db_rows;
		}
		
		return $asar_inbound;
	}

	function get_item_bet_inbound($conn,$tb_name,$start_date,$end_date,$property_num){
		$asar_inbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$query .=" WHERE in_property_num = \"{$property_num}\" AND in_date BETWEEN \"{$start_date}\" AND \"{$end_date}\"";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_inbound[$db_rows['id']]= $db_rows;
		}
		
		return $asar_inbound;
	}

	function get_item_bet_outbound($conn,$tb_name,$start_date,$end_date,$property_num){
		$asar_outbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$query .=" WHERE out_property_num = \"{$property_num}\" AND out_date BETWEEN \"{$start_date}\" AND \"{$end_date}\"";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_outbound[$db_rows['id']]= $db_rows;
		}
		
		return $asar_outbound;
	}


	/**
	 * Get All Orders
	 */
	
	function get_all_order($order_status,$tb_name,$conn){
		/**
		 * 0 - No Status
		 * 1 - Allocated
		 * 2 - In Transit
		 * 3 - Return
		 */
		
		$asar_order = array();
		

		$query = "SELECT * FROM {$tb_name}";
		$query .= " WHERE order_status = '{$order_status}'";

		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			if(isset($asar_order[$db_rows['order_property_num']])){
			 	$asar_order[$db_rows['order_property_num']] = $asar_order[$db_rows['order_property_num']] + $db_rows['order_qty'];
			}else{
				$asar_order[$db_rows['order_property_num']] = $db_rows['order_qty'];
			}
		}
		
		return $asar_order;

	}

	function get_all_return($conn){
		$asar_return = array();
		$query = "SELECT * FROM return_items";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			if(isset($asar_return[$db_rows['property_num']])){
				$asar_return[$db_rows['property_num']] = $asar_return[$db_rows['property_num']] + $db_rows['return_qty'];
		   }else{
			   $asar_return[$db_rows['property_num']] = $db_rows['return_qty'];
		   }
		}

		return $asar_return;
	}
	
	/**
		Get total Amount of Order
	 */

	 function get_order_amount($conn,$ref_num){
		
		$query = "SELECT order_ref_num, SUM(order_amount) AS amount";
		$query .= " FROM tb_order";
		$query .= " WHERE order_ref_num = '{$ref_num}'";

		$result = mysqli_query($conn,$query);

		$db_res = mysqli_fetch_assoc($result);

		$amount  = $db_res['amount'];

		return $amount;
		
	 }

	 /**
	 Get all Storage Location
	  */

	function get_all_sloc($conn,$tb_name){
		$asar_sloc = array();
		$query = "SELECT * FROM $tb_name";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_sloc[$db_rows['sloc_id']] = $db_rows['sloc_name'];
		}
		
		return $asar_sloc;
	}
	
	/**
	Check for Array if it has Space or Empty String Input
	 */

	 function is_array_has_empty_input($arr_name){
		foreach($arr_name as $arr_key => $arr_val){
			if(are_strings_equal(trim($arr_val),EmptyString)){
				return true;
			}
		}
		return false;
	}


	function are_all_items_exist($input_arr, $all_items){
		foreach($input_arr as $arr_key => $inp_property_num){
			if(!isset($all_items[$inp_property_num])){
				return false;
			}
		}

		return true;
	}

	function get_db_order($conn){
		$asar_order = array();

		$query = "SELECT * FROM tb_order";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_order[$db_rows['order_ref_num']][$db_rows['id']] = $db_rows; 
		}

		return $asar_order;
	}

	function get_db_out($conn){
		$asar_order = array();

		$query = "SELECT * FROM tb_outbound";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_order[$db_rows['out_ref_num']][$db_rows['id']] = $db_rows; 
		}

		return $asar_order;
	}

	function is_order_exist($ref_num,$all_order){
		if(!isset($all_order[$ref_num])){
			return false;
		}
		return true;
	}

	function is_out_exist($ref_num,$all_order){
		if(!isset($all_order[$ref_num])){
			return false;
		}
		return true;
	}

	function get_db_return($conn){
		$return_count = 0;

		$query = "SELECT * FROM return_items";
		$result = mysqli_query($conn,$query);

		$return_count = mysqli_num_rows($result);

		return $return_count;
		
	}

	function get_db_lock_items($conn){
		$lock_count = 0;

		$query = "SELECT * FROM lock_items";
		$result = mysqli_query($conn,$query);

		$lock_count = mysqli_num_rows($result);

		return $lock_count;
		
	}

	function get_db_avail_items($conn){
		$avail_count = 0;

		$query = "SELECT * FROM available_items WHERE qty <> 0";
		$result = mysqli_query($conn,$query);

		$avail_count = mysqli_num_rows($result);

		return $avail_count;
		
	}


	function delete_db_entry($conn,$id,$db_table){

		if(are_strings_equal($db_table,"lock_items")){
			$query = "DELETE FROM $db_table";
			$query .= " WHERE lock_id = '{$id}'";

			if($conn->query($query) === TRUE){
				return true;
			}
			return false;
		}

		if(are_strings_equal($db_table,"available_items")){
			$query = "DELETE FROM $db_table";
			$query .= " WHERE available_item_id = '{$id}'";

			if($conn->query($query) === TRUE){
				return true;
			}
			return false;
		}

		
		
	}

	function is_balance_ok($asar_all_items, $asar_order_items){
		foreach($asar_order_items as $arr_key => $arr_val){
			$property_num = $arr_val['property_num'];
			//Check to all Items it this Exist
			if(isset($asar_all_items[$property_num])){
				//Check the Outbound Qty vs Current Balance
				if((int)$arr_val['qty']>(int)$asar_all_items[$property_num]['qty']){
					return false;
				}
			}

			return true;
		}
	}


	/**
	  * Get total Amount of Outbound
	  */

	  function get_out_amount($conn,$ref_num){
		
		$query = "SELECT out_ref_num, SUM(out_amount) AS amount";
		$query .= " FROM tb_outbound";
		$query .= " WHERE out_ref_num = '{$ref_num}'";

		$result = mysqli_query($conn,$query);

		$db_res = mysqli_fetch_assoc($result);

		$amount  = $db_res['amount'];

		return $amount;
	}

	/**
	  * Get total Amount of Outbound
	  */

	  function get_stock_rec_amount($conn,$ref_num){
		
		$query = "SELECT ref_num, SUM(amount) AS amount";
		$query .= " FROM tb_receive";
		$query .= " WHERE ref_num = '{$ref_num}'";

		$result = mysqli_query($conn,$query);

		$db_res = mysqli_fetch_assoc($result);

		$amount  = $db_res['amount'];

		return $amount;
	}

	function get_stock_rec_trans($conn,$tb_name,$start_date,$end_date){
		$asar_outbound  = array();
		$query = "SELECT * FROM {$tb_name}";
		$query .=" WHERE receive_date BETWEEN \"{$start_date}\" AND \"{$end_date}\"";
		$result = mysqli_query($conn,$query);

		while($db_rows = mysqli_fetch_assoc($result)){
			$asar_outbound[$db_rows['receive_id']]= $db_rows;
		}
		
		return $asar_outbound;
	}
	


 ?>