<?php 
	/*
	Function to Get All Items from DB
	 */
	function get_db_items($conn,$db_table){
		$asar = array();
		$query = "SELECT * FROM {$db_table}";
		$result = mysqli_query($conn,$query);

		/**
		 * Get number of rows then if # of row <= 0 we will set a response
		 * Response = "There are no recorded Items in the database"
		 */
		$num_rows = mysqli_num_rows($result);

		if($num_rows>0){
			foreach($result as $row_key => $row_val){
				$asar[$row_val['property_num']] = $row_val['item_name'];
			}
		}
		
		return $asar;


	}



 ?>